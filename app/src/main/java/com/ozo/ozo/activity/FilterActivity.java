package com.ozo.ozo.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import com.ozo.ozo.R;
import com.ozo.ozo.fragment.FragmentCustomerFilter;
import com.ozo.ozo.fragment.FragmentNewsFilter;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseFragment;

public class FilterActivity extends BaseActivity {

    String tag = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        if (getIntent() != null) {
            tag = getIntent().getStringExtra("tag");
        }
        switch (tag) {
            case "FragmentCustomerManager":
                pushFragment(FragmentCustomerFilter.newInstance(tag));
                break;
            default:
                pushFragment(FragmentNewsFilter.newInstance(tag));
                break;
        }

    }

    public void pushFragment(BaseFragment mFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_layout, mFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            finish();
        }
    }
}
