package com.ozo.ozo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.WindowManager;

import com.ozo.ozo.R;
import com.ozo.ozo.fragment.FragmentLogin;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseFragment;

public class RegisterActivity extends BaseActivity {

    BaseActivity mActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mActivity = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        if (mActivity.getIsLogin()) {
            Intent mIntent = new Intent(mActivity, MainActivity.class);
            startActivity(mIntent);
            finish();
        } else {
            if (getIntent() != null && getIntent().hasExtra("block")) {
                mActivity.showDialog(mActivity, getIntent().getStringExtra("block"));
            }
            pushFragment(new FragmentLogin());
        }
    }

    public void pushFragment(BaseFragment mFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_layout, mFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            finish();
        }
    }
}
