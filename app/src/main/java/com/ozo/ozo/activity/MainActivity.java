package com.ozo.ozo.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ozo.ozo.R;
import com.ozo.ozo.adapter.MyPagerAdapter;
import com.ozo.ozo.fragment.FragmentNewFeeds;
import com.ozo.ozo.fragment.FragmentNotification;
import com.ozo.ozo.fragment.FragmentSetting;
import com.ozo.ozo.utils.BaseActivity;

public class MainActivity extends BaseActivity {
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        setupViewPager(mViewPager);

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setCurrentItem(0);
        setupTabIcons();
        updateTablayout(0);
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                updateTablayout(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void updateTablayout(int position) {
        TextView tabNewFeed = (TextView) mTabLayout.getTabAt(0).getCustomView().findViewById(R.id.title_tab);
        TextView tabNotification = (TextView) mTabLayout.getTabAt(1).getCustomView().findViewById(R.id.title_tab);
        TextView tabSetting = (TextView) mTabLayout.getTabAt(2).getCustomView().findViewById(R.id.title_tab);

        ImageView icontabNewFeed = (ImageView) mTabLayout.getTabAt(0).getCustomView().findViewById(R.id.icon_tab);
        ImageView icontabNotification = (ImageView) mTabLayout.getTabAt(1).getCustomView().findViewById(R.id.icon_tab);
        ImageView icontabSetting = (ImageView) mTabLayout.getTabAt(2).getCustomView().findViewById(R.id.icon_tab);
        switch (position) {
            case 0:
                tabNewFeed.setTextColor(getResources().getColor(R.color.colorMain));
                tabNotification.setTextColor(getResources().getColor(R.color.colorTabNomal));
                tabSetting.setTextColor(getResources().getColor(R.color.colorTabNomal));

                icontabNewFeed.setBackgroundResource(R.drawable.icon_new_feed_selected);
                icontabNotification.setBackgroundResource(R.drawable.icon_notification_normal);
                icontabSetting.setBackgroundResource(R.drawable.icon_setting_normal);
                break;
            case 1:
                tabNotification.setTextColor(getResources().getColor(R.color.colorMain));
                tabNewFeed.setTextColor(getResources().getColor(R.color.colorTabNomal));
                tabSetting.setTextColor(getResources().getColor(R.color.colorTabNomal));

                icontabNotification.setBackgroundResource(R.drawable.icon_notification_selected);
                icontabNewFeed.setBackgroundResource(R.drawable.icon_new_feed_normal);
                icontabSetting.setBackgroundResource(R.drawable.icon_setting_normal);
                break;
            case 2:
                tabSetting.setTextColor(getResources().getColor(R.color.colorMain));
                tabNewFeed.setTextColor(getResources().getColor(R.color.colorTabNomal));
                tabNotification.setTextColor(getResources().getColor(R.color.colorTabNomal));

                icontabSetting.setBackgroundResource(R.drawable.icon_setting_selected);
                icontabNotification.setBackgroundResource(R.drawable.icon_notification_normal);
                icontabNewFeed.setBackgroundResource(R.drawable.icon_new_feed_normal);
                break;
        }
    }

    private void setupTabIcons() {

        View mViewCustomTabLayput = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView tabNewFeed = (TextView) mViewCustomTabLayput.findViewById(R.id.title_tab);
        ImageView icontabNewFeed = (ImageView) mViewCustomTabLayput.findViewById(R.id.icon_tab);
        tabNewFeed.setText(getResources().getString(R.string.text_title_new_feeds));
//        tabNewFeed.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_tab_favourite, 0, 0);
        icontabNewFeed.setBackgroundResource(R.drawable.icon_new_feed_normal);
        mTabLayout.getTabAt(0).setCustomView(mViewCustomTabLayput);

        mViewCustomTabLayput = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView tabNotification = (TextView) mViewCustomTabLayput.findViewById(R.id.title_tab);
        ImageView icontabNotification = (ImageView) mViewCustomTabLayput.findViewById(R.id.icon_tab);
        icontabNotification.setBackgroundResource(R.drawable.icon_notification_normal);
        tabNotification.setText(getResources().getString(R.string.text_title_notification));
//        tabNotification.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_tab_call, 0, 0);
        mTabLayout.getTabAt(1).setCustomView(mViewCustomTabLayput);

        mViewCustomTabLayput = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        TextView tabSetting = (TextView) mViewCustomTabLayput.findViewById(R.id.title_tab);
        ImageView icontabSetting = (ImageView) mViewCustomTabLayput.findViewById(R.id.icon_tab);
        icontabSetting.setBackgroundResource(R.drawable.icon_setting_normal);
        tabSetting.setText(getResources().getString(R.string.text_title_setting));
//        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_tab_contacts, 0, 0);
        mTabLayout.getTabAt(2).setCustomView(mViewCustomTabLayput);
    }

    private void setupViewPager(ViewPager viewPager) {
        MyPagerAdapter mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mPagerAdapter.addFrag(new FragmentNewFeeds(), getResources().getString(R.string.text_title_new_feeds));
        mPagerAdapter.addFrag(new FragmentNotification(), getResources().getString(R.string.text_title_notification));
        mPagerAdapter.addFrag(new FragmentSetting(), getResources().getString(R.string.text_title_setting));
        viewPager.setAdapter(mPagerAdapter);
    }
}
