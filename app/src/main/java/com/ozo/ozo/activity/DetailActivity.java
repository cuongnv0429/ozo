package com.ozo.ozo.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.ozo.ozo.R;
import com.ozo.ozo.fragment.FragmentNewsDetail;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseFragment;

public class DetailActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Log.e("DetailActivity", "onCreate");
        if (getIntent() != null) {
            int newsID = getIntent().getIntExtra("news_id", 0);
            int position = getIntent().getIntExtra("position", 0);
            boolean isReason = false;
            if (getIntent().hasExtra("is_reason")) {
                isReason = getIntent().getBooleanExtra("is_reason", false);
            }
            if (getIntent().hasExtra("news_status")) {
                int news_status = getIntent().getIntExtra("news_status", 0);
                pushFragment(FragmentNewsDetail.newInstance(newsID, position, news_status, isReason));
            } else {
                pushFragment(FragmentNewsDetail.newInstance(newsID, position, isReason));
            }

        }
    }

    public void sendResult(int key, int position) {
        Intent intent = getIntent();
        intent.putExtra("key", key);
        intent.putExtra("position", position);
        setResult(Activity.RESULT_OK, intent);
        DetailActivity.this.finish();
    }

    public void pushFragment(BaseFragment mFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.frame_layout_detail, mFragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Log.e("onBackPressed: ", fragmentManager.getBackStackEntryCount() + "__");
        if (fragmentManager.getBackStackEntryCount() > 1) {
            super.onBackPressed();
        } else {
            finish();
        }
    }
}
