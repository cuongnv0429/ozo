package com.ozo.ozo.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.ozo.ozo.R;
import com.ozo.ozo.fragment.FragmentCardChoose;
import com.ozo.ozo.fragment.FragmentCustomerManager;
import com.ozo.ozo.fragment.FragmentNewsHided;
import com.ozo.ozo.fragment.FragmentNewsSaved;
import com.ozo.ozo.fragment.FragmentPostNews;
import com.ozo.ozo.fragment.FragmentRecharge;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

public class SettingActivity extends BaseActivity {

    int id = -1;
    private BaseFragment baseFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        if (getIntent() != null) {
            id = getIntent().getIntExtra("id", -1);
        }
        switch (id) {
            case Constant.POST_NEWS:
                addFragment(baseFragment = new FragmentPostNews());
                break;
            case Constant.NEWS_HIDE:
                addFragment(baseFragment = new FragmentNewsHided());
                break;
            case Constant.NEWS_SAVED:
                addFragment(baseFragment = new FragmentNewsSaved());
                break;
            case Constant.CUSTOMER_MANAGER:
                addFragment(baseFragment = new FragmentCustomerManager());
                break;
            case Constant.RECHARGE:
                addFragment(baseFragment = new FragmentRecharge());
                break;
            case Constant.CARD_CHOOSE:
                addFragment(baseFragment = new FragmentCardChoose());
                break;
        }
    }

    public void reloadPage( int key, int position) {
        if (baseFragment instanceof FragmentNewsHided) {
            ((FragmentNewsHided) baseFragment).reloadPage( key, position );
        }

        if (baseFragment instanceof  FragmentNewsSaved) {
            ((FragmentNewsSaved) baseFragment).reloadPage( key, position );
        }
    }

    public void addFragment(BaseFragment mFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.frame_layout, mFragment)
                .addToBackStack(null)
                .commit();
    }

    public void pushFragment(BaseFragment mFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_layout, mFragment)
                .addToBackStack(null)
                .commit();
    }


    @Override
    public void onBackPressed() {
        Log.e("onBackPressed", "onBackPressed");
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 1) {
            fragmentManager.popBackStackImmediate();
        } else {
            finish();
        }
    }
}
