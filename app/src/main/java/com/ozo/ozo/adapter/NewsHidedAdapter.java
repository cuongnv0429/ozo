package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ozo.ozo.R;
import com.ozo.ozo.model.NewsFeed;
import com.ozo.ozo.model.callbacks.OnLoadMoreListener;
import com.ozo.ozo.utils.ShareHelper;

import java.util.List;

/**
 * Created by ITV01 on 7/2/17.
 */

public class NewsHidedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context mContex;
    private List<NewsFeed> newsFeeds;
    private final int TYPE_ITEM = 0;
    private final int TYPE_LOAD_MORE = 1;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isLoading = false;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;

    public NewsHidedAdapter(Context mContex, List<NewsFeed> newsFeeds, RecyclerView mRecycler) {
        this.mContex = mContex;
        this.newsFeeds = newsFeeds;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecycler.getLayoutManager();
        mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }

            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    public void setLoaded() {
        isLoading = false;
    }

    public boolean isLoading() {
        return isLoading;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View mView = LayoutInflater.from(mContex).inflate(R.layout.item_news_hided, parent, false);
            return new ViewHolder(mView);
        } else if (viewType == TYPE_LOAD_MORE){
            View mView = LayoutInflater.from(mContex).inflate(R.layout.item_loading, parent, false);
            return new LoadingHolder(mView);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            NewsFeed newsFeed = newsFeeds.get(position);
            viewHolder.txtDate.setText(ShareHelper.parseDateTime(newsFeed.getCreateOn()));
            viewHolder.txtTitle.setText(newsFeed.getTitle());
            viewHolder.txtContent.setText(newsFeed.getContents());
        } else if (holder instanceof LoadingHolder) {
            LoadingHolder loadingViewHolder = (LoadingHolder) holder;
            loadingViewHolder.mLoading.setIndeterminate(true);
        }

    }

    @Override
    public int getItemCount() {
        return newsFeeds == null ? 0 : newsFeeds.size();
    }

    @Override
    public int getItemViewType(int position) {
        return newsFeeds.get(position) == null ? TYPE_LOAD_MORE : TYPE_ITEM;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txtTitle, txtContent, txtDate;
        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txt_tile_news_hided);
            txtContent = (TextView) itemView.findViewById(R.id.txt_content_news_hided);
            txtDate = (TextView) itemView.findViewById(R.id.txt_date_news_hided);
        }
    }

    public class  LoadingHolder extends RecyclerView.ViewHolder{
        private ProgressBar mLoading;
        public LoadingHolder(View itemView) {
            super(itemView);
            mLoading = (ProgressBar) itemView.findViewById(R.id.progress_load_more);
        }
    }
}
