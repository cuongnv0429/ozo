package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.ozo.ozo.R;
import com.ozo.ozo.model.Image;

import java.util.List;

/**
 * Created by ITV01 on 5/28/17.
 */

public class ImagesNewsDetailAdapter extends RecyclerView.Adapter<ImagesNewsDetailAdapter.ViewHolder>{

    private Context mContext;
    private List<Image> mLinks;

    public ImagesNewsDetailAdapter(Context mContext, List<Image> mLinks) {
        this.mContext = mContext;
        this.mLinks = mLinks;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_images_news_detail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Image image = mLinks.get(position);
        Glide.with(mContext)
                .load(image.getImageUrl())
                .placeholder(R.drawable.icon_camera_default)
                .override(150, 150)
                .error(R.drawable.icon_camera_default)
                .centerCrop()
                .into(holder.img);
    }

    @Override
    public int getItemCount() {
        return mLinks.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView img;
        public ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.img_news_detail);
        }
    }
}
