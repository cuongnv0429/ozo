package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ozo.ozo.R;
import com.ozo.ozo.model.NewsSite;
import com.ozo.ozo.utils.ShareHelper;

import java.util.List;

/**
 * Created by ITV01 on 5/18/17.
 */

public class NewsSiteAdapter extends RecyclerView.Adapter<NewsSiteAdapter.MyViewHolder>{
    List<NewsSite> mNewsSites;
    Context mContext;
    IOnNewsSource mInteface;
    String tag;
    public NewsSiteAdapter(Context mContext, List<NewsSite> mNewsSites, IOnNewsSource iOnNewsSource, String tag) {
        this.mContext = mContext;
        this.mNewsSites = mNewsSites;
        this.mInteface = iOnNewsSource;
        this.tag = tag;
    }

    @Override
    public NewsSiteAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_filter_district, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final NewsSiteAdapter.MyViewHolder holder, int position) {
        final NewsSite mNewsSite = mNewsSites.get(position);
        holder.txtName.setText(mNewsSite.getName());
        holder.imgChoose.setVisibility(View.GONE);
        NewsSite oldNewsSite = new Gson().fromJson(ShareHelper.getNewsSite(mContext, tag), NewsSite.class);
        if (mNewsSite.getId() == oldNewsSite.getId()) {
            holder.imgChoose.setVisibility(View.VISIBLE);
        }
        holder.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareHelper.saveNewsSite(new Gson().toJson(mNewsSite), mContext, tag);
                holder.imgChoose.setVisibility(View.VISIBLE);
                notifyDataSetChanged();
                mInteface.onChoose(mNewsSite.getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mNewsSites.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView txtName;
        private ImageView imgChoose;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txt_district_name);
            imgChoose = (ImageView) itemView.findViewById(R.id.img_district_choose);
        }
    }

    public interface IOnNewsSource{
        void onChoose(String name);
    }
}
