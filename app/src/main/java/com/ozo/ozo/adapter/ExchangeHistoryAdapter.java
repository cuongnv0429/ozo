package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ozo.ozo.R;
import com.ozo.ozo.model.ExchangeHistory;

import java.util.List;

/**
 * Created by user on 10/10/17.
 */

public class ExchangeHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context mContext;
    private List<ExchangeHistory> exchangeHistoryList;

    public ExchangeHistoryAdapter(Context mContext, List<ExchangeHistory> exchangeHistoryList) {
        this.mContext = mContext;
        this.exchangeHistoryList = exchangeHistoryList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_exchange_history, parent, false);
        return new ExchangeHistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ExchangeHistoryHolder exchangeHistoryHolder = (ExchangeHistoryHolder) holder;
        ExchangeHistory exchangeHistory = exchangeHistoryList.get(position);
        exchangeHistoryHolder.txtDate.setText(exchangeHistory.getDateString());
        exchangeHistoryHolder.txtAmount.setText(exchangeHistory.getAmount());
        exchangeHistoryHolder.txtNotes.setText(exchangeHistory.getNotes());
        exchangeHistoryHolder.txtPaymentMethod.setText(exchangeHistory.getPaymentMethod());
    }

    @Override
    public int getItemCount() {
        return exchangeHistoryList == null ? 0 : exchangeHistoryList.size();
    }


    private class ExchangeHistoryHolder extends RecyclerView.ViewHolder{
        private TextView txtAmount, txtDate, txtNotes, txtPaymentMethod;
        public ExchangeHistoryHolder(View itemView) {
            super(itemView);
            txtAmount = (TextView) itemView.findViewById(R.id.txt_price_exchange_history);
            txtDate = (TextView) itemView.findViewById(R.id.txt_date_exchange_history);
            txtNotes = (TextView) itemView.findViewById(R.id.txt_title_exchange_history);
            txtPaymentMethod = (TextView) itemView.findViewById(R.id.txt_content_exchange_history);
        }
    }
}
