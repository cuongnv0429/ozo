package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ozo.ozo.R;
import com.ozo.ozo.model.NewsFeed;
import com.ozo.ozo.model.callbacks.OnSimilarListener;

import java.util.List;

/**
 * Created by ITV01 on 6/10/17.
 */

public class SimilarAdapter extends RecyclerView.Adapter<SimilarAdapter.SimilarHolder>{

    private List<NewsFeed> mList;
    private Context mContext;
    private OnSimilarListener mOnSimilarListener;

    public SimilarAdapter(List<NewsFeed> mList, Context mContext, OnSimilarListener onSimilarListener) {
        this.mList = mList;
        this.mContext = mContext;
        this.mOnSimilarListener = onSimilarListener;
    }

    @Override
    public SimilarHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(mContext).inflate(R.layout.item_similar, parent, false);
        return new SimilarHolder(mView);
    }

    @Override
    public void onBindViewHolder(SimilarHolder holder, int position) {
        final NewsFeed mNewsFeed = mList.get(position);
        holder.txt_title.setText(mNewsFeed.getTitle());
        holder.txt_link.setText(mNewsFeed.getSiteName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnSimilarListener.onCLickSimilar(mNewsFeed.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class SimilarHolder extends RecyclerView.ViewHolder{
        private TextView txt_title;
        private TextView txt_link;
        public SimilarHolder(View itemView) {
            super(itemView);
            txt_title = (TextView) itemView.findViewById(R.id.txt_title_similar);
            txt_link = (TextView) itemView.findViewById(R.id.txt_link_similar);
        }
    }
}
