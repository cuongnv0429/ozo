package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ozo.ozo.R;
import com.ozo.ozo.model.Card;

import java.util.List;

/**
 * Created by ITV01 on 7/14/17.
 */

public class CardChooseAdapter extends RecyclerView.Adapter<CardChooseAdapter.MyViewHolder>{
    List<Card> cardList;
    Context mContext;
    IOnCardChoose mInteface;
    Card mCardChoose;
    public CardChooseAdapter(Context mContext, List<Card> cardList, IOnCardChoose iOnCardChoose) {
        this.mContext = mContext;
        this.cardList = cardList;
        this.mInteface = iOnCardChoose;
    }

    @Override
    public CardChooseAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_filter_district, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CardChooseAdapter.MyViewHolder holder, int position) {
        final Card card = cardList.get(position);
        holder.txtName.setText(card.getName() + "(" + card.getAmount() + " vnđ)");
        holder.imgChoose.setVisibility(View.GONE);
        if (card.equals(mCardChoose)) {
            holder.imgChoose.setVisibility(View.VISIBLE);
        }
        holder.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCardChoose = card;
                holder.imgChoose.setVisibility(View.VISIBLE);
                notifyDataSetChanged();
                mInteface.onChooseCard(card.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView txtName;
        private ImageView imgChoose;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txt_district_name);
            imgChoose = (ImageView) itemView.findViewById(R.id.img_district_choose);
        }
    }

    public interface IOnCardChoose{
        void onChooseCard(int name);
    }
}
