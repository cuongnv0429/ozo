package com.ozo.ozo.adapter;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ozo.ozo.R;
import com.ozo.ozo.model.Notification;
import com.ozo.ozo.model.callbacks.OnLoadMoreListener;
import com.ozo.ozo.utils.ShareHelper;


import java.util.List;

/**
 * Created by ITV01 on 7/2/17.
 */

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context mContext;
    private List<com.ozo.ozo.model.Notification> notifications;
    private final int TYPE_ITEM = 0;
    private final int TYPE_LOAD_MORE = 1;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isLoading = false;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;

    public NotificationAdapter(Context mContext, List<Notification> notifications, RecyclerView mRecycler) {
        this.mContext = mContext;
        this.notifications = notifications;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecycler.getLayoutManager();
        mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }

            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    public void setLoaded() {
        isLoading = false;
    }

    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View mView = LayoutInflater.from(mContext).inflate(R.layout.item_notification, parent, false);
            return new NotificationAdapter.NotificationHolder(mView);
        } else if (viewType == TYPE_LOAD_MORE){
            View mView = LayoutInflater.from(mContext).inflate(R.layout.item_loading, parent, false);
            return new NotificationAdapter.LoadingHolder(mView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NotificationAdapter.NotificationHolder) {
            NotificationAdapter.NotificationHolder notificationHolder = (NotificationAdapter.NotificationHolder) holder;
            Notification notification = notifications.get(position);
            notificationHolder.txtName.setText(notification.getUserName());
            notificationHolder.txtDate.setText(ShareHelper.parseDateTime(notification.getDateSend()));
            notificationHolder.txtTitle.setText(notification.getTitle());
            notificationHolder.txtContent.setText(notification.getDescription());
        } else if (holder instanceof NotificationAdapter.LoadingHolder) {
            NotificationAdapter.LoadingHolder loadingViewHolder = (NotificationAdapter.LoadingHolder) holder;
            loadingViewHolder.mLoading.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return notifications == null ? 0 : notifications.size();
    }

    @Override
    public int getItemViewType(int position) {
        return notifications.get(position) == null ? TYPE_LOAD_MORE : TYPE_ITEM;
    }

    private class NotificationHolder extends RecyclerView.ViewHolder{
        private TextView txtName, txtDate, txtTitle, txtContent;
        public NotificationHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txt_name_notification);
            txtDate = (TextView) itemView.findViewById(R.id.txt_date_notification);
            txtTitle = (TextView) itemView.findViewById(R.id.txt_title_notificaiton);
            txtContent = (TextView) itemView.findViewById(R.id.txt_content_notification);

        }
    }

    public class  LoadingHolder extends RecyclerView.ViewHolder{
        private ProgressBar mLoading;
        public LoadingHolder(View itemView) {
            super(itemView);
            mLoading = (ProgressBar) itemView.findViewById(R.id.progress_load_more);
        }
    }
}
