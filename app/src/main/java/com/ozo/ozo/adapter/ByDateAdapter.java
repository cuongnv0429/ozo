package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ozo.ozo.R;
import com.ozo.ozo.model.ByDate;
import com.ozo.ozo.utils.ShareHelper;

import java.util.List;

/**
 * Created by ITV01 on 5/18/17.
 */

public class ByDateAdapter extends RecyclerView.Adapter<ByDateAdapter.MyViewHolder>{
    List<ByDate> mByDates;
    Context mContext;
    IOnByDate mInteface;
    private String tag;
    public ByDateAdapter(Context mContext, List<ByDate> mByDates, IOnByDate iOnByDate, String tag) {
        this.mContext = mContext;
        this.mByDates = mByDates;
        this.mInteface = iOnByDate;
        this.tag = tag;
    }

    @Override
    public ByDateAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_filter_district, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ByDateAdapter.MyViewHolder holder, int position) {
        final ByDate mByDate = mByDates.get(position);
        holder.txtName.setText(mByDate.getName());
        holder.imgChoose.setVisibility(View.GONE);
        ByDate oldByDate = new Gson().fromJson(ShareHelper.getByDate(mContext, tag), ByDate.class);
        if (mByDate.getId().equalsIgnoreCase(oldByDate.getId())) {
            holder.imgChoose.setVisibility(View.VISIBLE);
        }
        holder.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareHelper.saveByDate(new Gson().toJson(mByDate), mContext, tag);
                holder.imgChoose.setVisibility(View.VISIBLE);
                notifyDataSetChanged();
                mInteface.onChoose(mByDate.getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mByDates.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView txtName;
        private ImageView imgChoose;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txt_district_name);
            imgChoose = (ImageView) itemView.findViewById(R.id.img_district_choose);
        }
    }

    public interface IOnByDate{
        void onChoose(String name);
    }
}

