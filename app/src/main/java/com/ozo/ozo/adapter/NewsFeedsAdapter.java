package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ozo.ozo.R;
import com.ozo.ozo.customview.ClickItemNewsFeed;
import com.ozo.ozo.model.NewsFeed;
import com.ozo.ozo.model.User;
import com.ozo.ozo.model.callbacks.OnLoadMoreListener;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.Constant;
import com.ozo.ozo.utils.ShareHelper;

import java.util.List;

/**
 * Created by ITV01 on 4/19/17.
 */

public class NewsFeedsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    List<NewsFeed> mNewsFeeds;
    Context mContext;
    IDialogNewsFeed iDialogNewsFeed;
    ClickItemNewsFeed clickItemNewsFeed;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isLoading = false;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    User mUser;

    public NewsFeedsAdapter(RecyclerView mRecyclerView, List<NewsFeed> mNewsFeeds, Context mContext,
                            IDialogNewsFeed iDialogNewsFeed, ClickItemNewsFeed clickItemNewsFeed) {
        this.mNewsFeeds = mNewsFeeds;
        this.mContext = mContext;
        this.iDialogNewsFeed = iDialogNewsFeed;
        this.clickItemNewsFeed = clickItemNewsFeed;
        mUser = ((BaseActivity) mContext).getUserInfo();
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView.getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }

            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View mView = LayoutInflater.from(mContext).inflate(R.layout.item_news_feeds, parent, false);
            return new ViewHolder(mView);
        } else if (viewType == VIEW_TYPE_LOADING) {
                View mView = LayoutInflater.from(mContext).inflate(R.layout.item_loading, parent, false);
                return new LoadingViewHolder(mView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            final NewsFeed mNewsFeed = mNewsFeeds.get(position);
            viewHolder.txtTitle.setText(mNewsFeed.getTitle());

            viewHolder.txtPrice.setText(mNewsFeed.getPriceText());
            String date = ShareHelper.parseDateTime(mNewsFeed.getCreateOn());
            viewHolder.txtDate.setText(date);
//            viewHolder.txtDate.setText(date + " - " + mNewsFeed.getStatusName());

            viewHolder.imgNewsSave.setColorFilter(mContext.getResources().getColor(R.color.colorMain));
            viewHolder.imgNewsHide.setColorFilter(mContext.getResources().getColor(R.color.colorMain));
            viewHolder.imgForUser.setColorFilter(mContext.getResources().getColor(R.color.colorMain));

            if (mNewsFeed.getContents().length() > Constant.SEE_MORE && mNewsFeed.getContents().length() < Constant.READ_MORE) {
                if (!mNewsFeed.isShow()) {
                    viewHolder.txtContent.setText(Html.fromHtml(mNewsFeed.getContents().substring(0, Constant.SEE_MORE) + "..." + "<font size=\"16\" color=\"#B8B8B8\">Xem thêm</font>"));
                } else {
                    viewHolder.txtContent.setText(Html.fromHtml(mNewsFeed.getContents()));
                }
            } else if (mNewsFeed.getContents().length() > Constant.READ_MORE) {
                viewHolder.txtContent.setText(Html.fromHtml(mNewsFeed.getContents().substring(0, Constant.READ_MORE) + "..." + "<font size=\"16\" color=\"#B8B8B8\">Đọc thêm</font>"));
            } else {
                viewHolder.txtContent.setText(Html.fromHtml(mNewsFeed.getContents()));
            }

            if (mNewsFeed.isCC()) {
                viewHolder.txtVerifier.setVisibility(View.VISIBLE);
                viewHolder.txtForUser.setText(mContext.getResources().getString(R.string.text_remove_for_use));
            } else {
                viewHolder.txtVerifier.setVisibility(View.GONE);
                viewHolder.txtForUser.setText(mContext.getResources().getString(R.string.text_for_user));
            }

            if (mUser != null && mUser.isValid()) {
                if (mUser.getIsUser()) {
                    viewHolder.linearCustomer.setVisibility(View.VISIBLE);
                    viewHolder.linearStaff.setVisibility(View.GONE);
                } else {
                    viewHolder.linearCustomer.setVisibility(View.GONE);
                    viewHolder.linearStaff.setVisibility(View.VISIBLE);
                }
            }

            viewHolder.txtContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mNewsFeed.getContents().length() > Constant.SEE_MORE && mNewsFeed.getContents().length() < Constant.READ_MORE) {
                        if (mNewsFeed.isShow()) {
                            mNewsFeed.setShow(false);
                        } else {
                            mNewsFeed.setShow(true);
                        }
                        notifyItemChanged(position);
                    } else if (mNewsFeed.getContents().length() > Constant.READ_MORE) {
                        //
                        clickItemNewsFeed.onClickItemNewsFeed(Constant.DETAIL_NEWSFEED, position);
                    }
                }
            });
            viewHolder.txtTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickItemNewsFeed.onClickItemNewsFeed(Constant.DETAIL_NEWSFEED, position);
                }
            });
            viewHolder.imgDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iDialogNewsFeed.showDialog(position, mNewsFeed.isCC());
                }
            });

            viewHolder.txtNewsSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickItemNewsFeed.onClickItemNewsFeed(Constant.NEWS_SAVE, position);
                }
            });

            viewHolder.txtNewsHide.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickItemNewsFeed.onClickItemNewsFeed(Constant.NEWS_HIDE, position);
                }
            });

            viewHolder.txtContactStaff.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickItemNewsFeed.onClickItemNewsFeed(Constant.CONTACT, position);
                }
            });

            viewHolder.txtNewsReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickItemNewsFeed.onClickItemNewsFeed(Constant.NEWS_REJECT, position);
                }
            });

            viewHolder.txtForUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mNewsFeed.isCC()) {
                        clickItemNewsFeed.onClickItemNewsFeed(Constant.FOR_USER_EDIT, position);
                    } else {
                        clickItemNewsFeed.onClickItemNewsFeed(Constant.NEWS_FOR_USER, position);
                    }

                }
            });

            viewHolder.txtContactCustomer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickItemNewsFeed.onClickItemNewsFeed(Constant.CONTACT, position);
                }
            });



        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }

    public void setLoaded() {
        isLoading = false;
    }

    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public int getItemCount() {
        return mNewsFeeds == null ? 0 : mNewsFeeds.size();
    }
    @Override
    public int getItemViewType(int position) {
        return mNewsFeeds.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle, txtContent, txtPrice, txtVerifier, txtDate;
        private ImageView imgDialog, imgNewsSave, imgNewsHide, imgForUser;
        private TextView txtNewsSave, txtNewsHide, txtContactCustomer, txtNewsReject, txtForUser, txtContactStaff;
        private LinearLayout linearCustomer, linearStaff;
        public ViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txt_title);
            txtContent = (TextView) itemView.findViewById(R.id.txt_content);
            txtPrice = (TextView) itemView.findViewById(R.id.txt_price);
            txtVerifier = (TextView) itemView.findViewById(R.id.txt_verifer);
            txtDate = (TextView) itemView.findViewById(R.id.txt_date_type_news);
            imgDialog = (ImageView) itemView.findViewById(R.id.img_dialog);
            txtNewsSave = (TextView) itemView.findViewById(R.id.txt_news_save_customer);
            txtNewsHide = (TextView) itemView.findViewById(R.id.txt_news_hide_customer);
            txtContactCustomer = (TextView) itemView.findViewById(R.id.txt_contact_customer);
            txtNewsReject = (TextView) itemView.findViewById(R.id.txt_reject_staff);
            txtForUser = (TextView) itemView.findViewById(R.id.txt_for_user_staft);
            txtContactStaff = (TextView) itemView.findViewById(R.id.txt_contact_staft);
            linearCustomer = (LinearLayout) itemView.findViewById(R.id.linear_customer);
            linearStaff = (LinearLayout) itemView.findViewById(R.id.linear_staff);
            imgNewsSave = (ImageView) itemView.findViewById(R.id.img_news_save);
            imgNewsHide = (ImageView) itemView.findViewById(R.id.img_news_hide);
            imgForUser = (ImageView) itemView.findViewById(R.id.img_for_user);
        }
    }

    class LoadingViewHolder extends RecyclerView.ViewHolder{
        private ProgressBar progressBar;
        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_load_more);
        }
    }

    public interface IDialogNewsFeed{
        void showDialog(int position, boolean isCC);
    }
}
