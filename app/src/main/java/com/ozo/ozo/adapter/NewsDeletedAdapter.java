package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ozo.ozo.R;
import com.ozo.ozo.model.News;

import java.util.List;

/**
 * Created by ITV01 on 4/13/17.
 */

public class NewsDeletedAdapter extends RecyclerView.Adapter<NewsDeletedAdapter.MyViewHolder>{

    private List<News> mNewsList;
    private Context mContext;

    public NewsDeletedAdapter(Context mContext, List<News> mList) {
        this.mContext = mContext;
        this.mNewsList = mList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_deleted, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        News mNews = mNewsList.get(position);
        holder.txtName.setText(mNews.getmName());
        holder.txtContent.setText(mNews.getmContent());
        holder.txtDate.setText(mNews.getmDate());
    }

    @Override
    public int getItemCount() {
        return mNewsList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgAvartar;
        private TextView txtName;
        private TextView txtContent;
        private TextView txtDate;
        public MyViewHolder(View itemView) {
            super(itemView);
            imgAvartar = (ImageView) itemView.findViewById(R.id.img_avartar);
            txtName = (TextView) itemView.findViewById(R.id.txt_name);
            txtContent = (TextView) itemView.findViewById(R.id.txt_content);
            txtDate = (TextView) itemView.findViewById(R.id.txt_date);
        }
    }
}
