package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ozo.ozo.R;
import com.ozo.ozo.model.Category;
import com.ozo.ozo.utils.ShareHelper;

import java.util.List;

/**
 * Created by ITV01 on 5/18/17.
 */

public class CategoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context mContext;
    private List<Category> mCategories;
    private IOnCategory iOnCategory;
    private String TAG = "";

    public CategoryAdapter(Context mContext, List<Category> mCategories, IOnCategory iOnCategory, String TAG) {
        this.mContext = mContext;
        this.mCategories = mCategories;
        this.iOnCategory = iOnCategory;
        this.TAG = TAG;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_section_category, parent, false);
            return new SectionViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
            return new CategoryViewHolder(v);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Category category = mCategories.get(position);
        if (category.getParentCategoryId() != 0 ) {
            final CategoryViewHolder categoryViewHolder = (CategoryViewHolder) holder;
            categoryViewHolder.txtContent.setText(category.getName());
            categoryViewHolder.imgChoose.setVisibility(View.GONE);
            Category oldCategory = new Gson().fromJson(ShareHelper.getCategory(mContext, TAG), Category.class);
            if (category.getId() == oldCategory.getId()) {
                categoryViewHolder.imgChoose.setVisibility(View.VISIBLE);
            }
            categoryViewHolder.txtContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    categoryViewHolder.imgChoose.setVisibility(View.VISIBLE);
                    ShareHelper.saveCategory(new Gson().toJson(category), mContext, TAG);
                    iOnCategory.onChoose(category.getName());
                }
            });
        } else {
            final SectionViewHolder sectionViewHolder = (SectionViewHolder) holder;
            sectionViewHolder.txtSectionContent.setText(category.getName());
            sectionViewHolder.imgSectionChoose.setVisibility(View.GONE);
            Category oldCategory = new Gson().fromJson(ShareHelper.getCategory(mContext, TAG), Category.class);
            if (category.getId() == oldCategory.getId()) {
                sectionViewHolder.imgSectionChoose.setVisibility(View.VISIBLE);
            }

            sectionViewHolder.txtSectionContent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sectionViewHolder.imgSectionChoose.setVisibility(View.VISIBLE);
                    ShareHelper.saveCategory(new Gson().toJson(category), mContext, TAG);
                    iOnCategory.onChoose(category.getName());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mCategories.get(position).getParentCategoryId() == 0) {
            return 0;
        } else {
            return 1;
        }
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        private TextView txtContent;
        private ImageView imgChoose;
        public CategoryViewHolder(View itemView) {
            super(itemView);
            txtContent = (TextView) itemView.findViewById(R.id.txt_category_content);
            imgChoose = (ImageView) itemView.findViewById(R.id.img_category_content);
        }
    }

    public static class SectionViewHolder extends RecyclerView.ViewHolder {
        private TextView txtSectionContent;
        private ImageView imgSectionChoose;
        public SectionViewHolder(View itemView) {
            super(itemView);
            txtSectionContent = (TextView) itemView.findViewById(R.id.txt_section_category_content);
            imgSectionChoose = (ImageView) itemView.findViewById(R.id.img_section_category_content);
        }
    }
    public interface IOnCategory{
        void onChoose(String name);
    }
}
