package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ozo.ozo.R;
import com.ozo.ozo.model.Customer;
import com.ozo.ozo.model.callbacks.OnLoadMoreListener;
import com.ozo.ozo.utils.ShareHelper;

import java.util.List;

/**
 * Created by ITV01 on 6/3/17.
 */

public class CustomerManagerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context mContex;
    private List<Customer> mCustomers;
    private final int TYPE_ITEM = 0;
    private final int TYPE_LOAD_MORE = 1;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isLoading = false;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;

    public CustomerManagerAdapter(Context mContex, List<Customer> mCustomers, RecyclerView mRecycler) {
        this.mContex = mContex;
        this.mCustomers = mCustomers;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecycler.getLayoutManager();
        mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            isLoading = true;
                            onLoadMoreListener.onLoadMore();
                        }
                    }
                }

            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    public void setLoaded() {
        isLoading = false;
    }

    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View mView = LayoutInflater.from(mContex).inflate(R.layout.item_customer_manager, parent, false);
            return new CustomerHolder(mView);
        } else if (viewType == TYPE_LOAD_MORE){
            View mView = LayoutInflater.from(mContex).inflate(R.layout.item_loading, parent, false);
            return new LoadingHolder(mView);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CustomerHolder) {
            CustomerHolder customerHolder = (CustomerHolder) holder;
            Customer mCustomer = mCustomers.get(position);
            customerHolder.viewIsOnline.setBackgroundResource(mCustomer.isOnline() ? R.drawable.bg_status_online : R.drawable.bg_status_offline);
            customerHolder.txtCustomerFullname.setText(mCustomer.getFullName());
            customerHolder.txtCustomerPhone.setText(mCustomer.getPhone());
            if (mCustomer.getTimeEnd() != null) {
                customerHolder.txtCustomerEndTimePayment.setText(ShareHelper.parseDateTime(mCustomer.getTimeEnd()));
            } else {
                customerHolder.txtCustomerEndTimePayment.setText(mCustomer.getTimeEndStr());
            }

        } else if (holder instanceof LoadingHolder) {
            LoadingHolder loadingViewHolder = (LoadingHolder) holder;
            loadingViewHolder.mLoading.setIndeterminate(true);
        }

    }

//    public void setList(List<Customer> customers) {
//        this.mCustomers.addAll(customers);
//        notifyDataSetChanged();
//    }

    @Override
    public int getItemCount() {
        return mCustomers == null ? 0 : mCustomers.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mCustomers.get(position) == null ? TYPE_LOAD_MORE : TYPE_ITEM;
    }

    public class CustomerHolder extends RecyclerView.ViewHolder{
        private View viewIsOnline;
        private TextView txtCustomerFullname, txtCustomerEndTimePayment, txtCustomerPhone;
        public CustomerHolder(View itemView) {
            super(itemView);
            viewIsOnline = (View) itemView.findViewById(R.id.view_is_online);
            txtCustomerFullname = (TextView) itemView.findViewById(R.id.txt_customer_full_name);
            txtCustomerEndTimePayment = (TextView) itemView.findViewById(R.id.txt_customer_end_time_payment);
            txtCustomerPhone = (TextView) itemView.findViewById(R.id.txt_customer_phone);
        }
    }

    public class  LoadingHolder extends RecyclerView.ViewHolder{
        private ProgressBar mLoading;
        public LoadingHolder(View itemView) {
            super(itemView);
            mLoading = (ProgressBar) itemView.findViewById(R.id.progress_load_more);
        }
    }
}
