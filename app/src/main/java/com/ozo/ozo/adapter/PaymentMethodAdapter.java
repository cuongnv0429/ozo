package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ozo.ozo.R;
import com.ozo.ozo.model.Payment;
import com.ozo.ozo.utils.ShareHelper;

import java.util.List;

/**
 * Created by ITV01 on 7/15/17.
 */

public class PaymentMethodAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context mContext;
    private List<Payment> paymentList;
    private String tag;
    private OnClickPaymentMethod onClickPaymentMethod;

    public PaymentMethodAdapter(Context mContext, List<Payment> paymentList, String tag, OnClickPaymentMethod onClickPaymentMethod) {
        this.mContext = mContext;
        this.paymentList = paymentList;
        this.tag = tag;
        this.onClickPaymentMethod = onClickPaymentMethod;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_payment_method, parent, false);
        return new PaymentHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PaymentHolder paymentHolder = (PaymentHolder) holder;
        final Payment payment = paymentList.get(position);

        paymentHolder.txtContent.setText(payment.getText());

        paymentHolder.txtContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareHelper.savePaymentMethod(new Gson().toJson(payment), mContext, tag);
                onClickPaymentMethod.onClickPaymentMethod(payment.getText());
            }
        });
    }

    @Override
    public int getItemCount() {
        return paymentList == null ? 0 : paymentList.size();
    }

    private class PaymentHolder extends RecyclerView.ViewHolder{
        private TextView txtContent;
        public PaymentHolder(View itemView) {
            super(itemView);
            txtContent = (TextView) itemView.findViewById(R.id.txt_payment_method_content);
        }
    }

    public interface OnClickPaymentMethod{
        void onClickPaymentMethod(String name);
    }
}
