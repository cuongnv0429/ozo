package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ozo.ozo.R;
import com.ozo.ozo.model.Account;
import com.ozo.ozo.model.Staff;
import com.ozo.ozo.utils.ShareHelper;

import java.util.List;

/**
 * Created by tug-on(_SM_) on 7/6/2017.
 */

public class AccountTypeFilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context mContext;
    private List<Account> accountList;
    private IOnAccount iOnAccount;
    private String tag = "";

    public AccountTypeFilterAdapter(Context mContext, List<Account> accounts, IOnAccount iOnAccount, String tag) {
        this.mContext = mContext;
        this.accountList = accounts;
        this.iOnAccount = iOnAccount;
        this.tag = tag;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_by_manage_filter, parent, false);
        return new AccountTypeFilterAdapter.AccountViewHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Account account = accountList.get(position);
        final AccountViewHolder accountViewHolder = (AccountViewHolder) holder;
        accountViewHolder.txtName.setText(account.getText());
        accountViewHolder.imgChoose.setVisibility(View.GONE);
        Account oldAccount = new Gson().fromJson(ShareHelper.getAccountType(mContext, tag), Account.class);
        if (account.getValue().equalsIgnoreCase(oldAccount.getValue())) {
            accountViewHolder.imgChoose.setVisibility(View.VISIBLE);
        }
        accountViewHolder.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accountViewHolder.imgChoose.setVisibility(View.VISIBLE);
                ShareHelper.saveAccountType(new Gson().toJson(account), mContext, tag);
                iOnAccount.onChoose(account.getText());
            }
        });
    }

    @Override
    public int getItemCount() {
        return accountList.size();
    }

    public static class AccountViewHolder extends RecyclerView.ViewHolder {
        private TextView txtName;
        private ImageView imgChoose;
        public AccountViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txt_by_manager_name);
            imgChoose = (ImageView) itemView.findViewById(R.id.img_by_manager);
        }
    }

    public interface IOnAccount{
        void onChoose(String name);
    }
}
