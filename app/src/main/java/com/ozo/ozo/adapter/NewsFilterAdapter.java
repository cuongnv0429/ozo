package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ozo.ozo.R;
import com.ozo.ozo.model.NewsFilter;

import java.util.List;

/**
 * Created by ITV01 on 4/13/17.
 */

public class NewsFilterAdapter extends RecyclerView.Adapter<NewsFilterAdapter.MyViewHolder>{
    List<NewsFilter> mNewsFilters;
    Context mContext;
    public NewsFilterAdapter(Context mContext, List<NewsFilter> mList) {
        this.mContext = mContext;
        this.mNewsFilters = mList;
    }

    @Override
    public NewsFilterAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_news_filter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NewsFilterAdapter.MyViewHolder holder, int position) {
        NewsFilter mNewsFilter = mNewsFilters.get(position);
        holder.txtTitle.setText(mNewsFilter.getTitle());
    }

    @Override
    public int getItemCount() {
        return mNewsFilters.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView txtTitle;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.txt_title);
        }
    }
}
