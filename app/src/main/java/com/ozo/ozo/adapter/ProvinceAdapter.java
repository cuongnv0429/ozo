package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ozo.ozo.R;
import com.ozo.ozo.model.District;
import com.ozo.ozo.utils.ShareHelper;

import java.util.List;

/**
 * Created by user on 1/6/18.
 */

public class ProvinceAdapter extends RecyclerView.Adapter<ProvinceAdapter.MyViewHolder>{
    List<District> mDistricts;
    Context mContext;
    ProvinceAdapter.IOnDistrict mInteface;
    String tag;
    public ProvinceAdapter(Context mContext, List<District> mDistricts, ProvinceAdapter.IOnDistrict iOnDistrict, String tag) {
        this.mContext = mContext;
        this.mDistricts = mDistricts;
        this.mInteface = iOnDistrict;
        this.tag = tag;
    }

    @Override
    public ProvinceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_filter_district, parent, false);
        return new ProvinceAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ProvinceAdapter.MyViewHolder holder, int position) {
        final District mDistrict = mDistricts.get(position);
        holder.txtName.setText(mDistrict.getName());
        holder.imgChoose.setVisibility(View.GONE);
        District oldDistrict = new Gson().fromJson(ShareHelper.getProvince(mContext, tag), District.class);
        if (mDistrict.getId() == oldDistrict.getId()) {
            holder.imgChoose.setVisibility(View.VISIBLE);
        }
        holder.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareHelper.saveProvince(new Gson().toJson(mDistrict), mContext, tag);
                holder.imgChoose.setVisibility(View.VISIBLE);
                notifyDataSetChanged();
                mInteface.onChooseDistrict(mDistrict.getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDistricts.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView txtName;
        private ImageView imgChoose;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txt_district_name);
            imgChoose = (ImageView) itemView.findViewById(R.id.img_district_choose);
        }
    }

    public interface IOnDistrict{
        void onChooseDistrict(String name);
    }
}
