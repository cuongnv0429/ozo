package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ozo.ozo.R;
import com.ozo.ozo.model.Category;
import com.ozo.ozo.model.Staff;
import com.ozo.ozo.model.callbacks.OnLoadMoreListener;
import com.ozo.ozo.utils.ShareHelper;

import java.util.List;

import static android.R.attr.category;

/**
 * Created by tug-on(_SM_) on 7/6/2017.
 */

public class ByMangerFilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private Context mContext;
    private List<Staff> staffList;
    private ByMangerFilterAdapter.IOnByManager IOnByManager;
    private String tag = "";
    private final int TYPE_ITEM = 0;
    private final int TYPE_LOAD_MORE = 1;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean isLoading = false;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;

    public ByMangerFilterAdapter(Context mContext, List<Staff> staffs, ByMangerFilterAdapter.IOnByManager iOnByManager,
                                 String tag, RecyclerView mRecycler) {
        this.mContext = mContext;
        this.staffList = staffs;
        this.IOnByManager = iOnByManager;
        this.tag = tag;
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecycler.getLayoutManager();
        mRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }

            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_by_manage_filter, parent, false);
            return new ByMangerViewHolder(v);
        } else if (viewType == TYPE_LOAD_MORE){
            View mView = LayoutInflater.from(mContext).inflate(R.layout.item_loading, parent, false);
            return new ByMangerFilterAdapter.LoadingHolder(mView);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ByMangerViewHolder) {
            final Staff staff = staffList.get(position);
            final ByMangerViewHolder byMangerViewHolder = (ByMangerViewHolder) holder;
            byMangerViewHolder.txtName.setText(staff.getText());
            byMangerViewHolder.imgChoose.setVisibility(View.GONE);
            Staff oldStaff = new Gson().fromJson(ShareHelper.getByManager(mContext, tag), Staff.class);
            if (staff.getValue().equalsIgnoreCase(oldStaff.getValue())) {
                byMangerViewHolder.imgChoose.setVisibility(View.VISIBLE);
            }
            byMangerViewHolder.txtName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    byMangerViewHolder.imgChoose.setVisibility(View.VISIBLE);
                    ShareHelper.saveByManager(new Gson().toJson(staff), mContext, tag);
                    IOnByManager.onChoose(staff.getText());
                }
            });
        } else if (holder instanceof ByMangerFilterAdapter.LoadingHolder) {
            ByMangerFilterAdapter.LoadingHolder loadingViewHolder = (ByMangerFilterAdapter.LoadingHolder) holder;
            loadingViewHolder.mLoading.setIndeterminate(true);
        }

    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    public void setLoaded() {
        isLoading = false;
    }

    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public int getItemViewType(int position) {
        return staffList.get(position) == null ? TYPE_LOAD_MORE : TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return staffList == null ? 0 : staffList.size();
    }

    public static class ByMangerViewHolder extends RecyclerView.ViewHolder {
        private TextView txtName;
        private ImageView imgChoose;
        public ByMangerViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txt_by_manager_name);
            imgChoose = (ImageView) itemView.findViewById(R.id.img_by_manager);
        }
    }

    public class  LoadingHolder extends RecyclerView.ViewHolder{
        private ProgressBar mLoading;
        public LoadingHolder(View itemView) {
            super(itemView);
            mLoading = (ProgressBar) itemView.findViewById(R.id.progress_load_more);
        }
    }

    public interface IOnByManager{
        void onChoose(String name);
    }
}
