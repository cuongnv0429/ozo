package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ozo.ozo.R;
import com.ozo.ozo.model.ByPrice;
import com.ozo.ozo.utils.ShareHelper;

import java.util.List;

/**
 * Created by ITV01 on 5/18/17.
 */

public class ByPriceAdapter extends RecyclerView.Adapter<ByPriceAdapter.MyViewHolder>{
    List<ByPrice> mByPrices;
    Context mContext;
    IOnByPrice mInteface;
    String tag;
    public ByPriceAdapter(Context mContext, List<ByPrice> mPrices, IOnByPrice iOnByDate, String tag) {
        this.mContext = mContext;
        this.mByPrices = mPrices;
        this.mInteface = iOnByDate;
        this.tag = tag;
    }

    @Override
    public ByPriceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_filter_district, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ByPriceAdapter.MyViewHolder holder, int position) {
        final ByPrice mByPrice = mByPrices.get(position);
        holder.txtName.setText(mByPrice.getName());
        holder.imgChoose.setVisibility(View.GONE);
        ByPrice oldByPrice = new Gson().fromJson(ShareHelper.getByPrice(mContext, tag), ByPrice.class);
        if (mByPrice.getId().equalsIgnoreCase(oldByPrice.getId())) {
            holder.imgChoose.setVisibility(View.VISIBLE);
        }
        holder.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareHelper.saveByPrice(new Gson().toJson(mByPrice), mContext, tag);
                holder.imgChoose.setVisibility(View.VISIBLE);
                notifyDataSetChanged();
                mInteface.onChoose(mByPrice.getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mByPrices.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView txtName;
        private ImageView imgChoose;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txt_district_name);
            imgChoose = (ImageView) itemView.findViewById(R.id.img_district_choose);
        }
    }

    public interface IOnByPrice{
        void onChoose(String name);
    }
}
