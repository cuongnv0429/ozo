package com.ozo.ozo.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ozo.ozo.R;
import com.ozo.ozo.model.NewsStatus;
import com.ozo.ozo.utils.ShareHelper;

import java.util.List;

/**
 * Created by ITV01 on 5/18/17.
 */

public class NewsStatusAdapter extends RecyclerView.Adapter<NewsStatusAdapter.MyViewHolder>{
    List<NewsStatus> mNewsStatuses;
    Context mContext;
    IOnNewsType mInteface;
    String tag = "";
    public NewsStatusAdapter(Context mContext, List<NewsStatus> mNewsStatuses, IOnNewsType iOnNewsType, String tag) {
        this.mContext = mContext;
        this.mNewsStatuses = mNewsStatuses;
        this.mInteface = iOnNewsType;
        this.tag = tag;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_filter_district, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final NewsStatus mNewsStatus = mNewsStatuses.get(position);
        holder.txtName.setText(mNewsStatus.getName());
        holder.imgChoose.setVisibility(View.GONE);
        NewsStatus oldNewsStatus = new Gson().fromJson(ShareHelper.getNewsStatus(mContext, tag), NewsStatus.class);
        if (mNewsStatus.getId() == oldNewsStatus.getId()) {
            holder.imgChoose.setVisibility(View.VISIBLE);
        }
        holder.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareHelper.saveNewsStatus(new Gson().toJson(mNewsStatus), mContext, tag);
                holder.imgChoose.setVisibility(View.VISIBLE);
                notifyDataSetChanged();
                mInteface.onChoose(mNewsStatus.getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mNewsStatuses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView txtName;
        private ImageView imgChoose;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txt_district_name);
            imgChoose = (ImageView) itemView.findViewById(R.id.img_district_choose);
        }
    }

    public interface IOnNewsType{
        void onChoose(String name);
    }
}

