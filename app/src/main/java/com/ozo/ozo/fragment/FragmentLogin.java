package com.ozo.ozo.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.ozo.ozo.R;
import com.ozo.ozo.activity.MainActivity;
import com.ozo.ozo.activity.RegisterActivity;
import com.ozo.ozo.databinding.FragmentLoginBinding;
import com.ozo.ozo.model.User;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 4/9/17.
 */

public class FragmentLogin extends BaseFragment implements View.OnClickListener{
    private static final String TAG = "FragmentLogin";
    BaseActivity mActivity;
    FragmentLoginBinding mLoginBinding;
    BaseApplication mApplication;
    OzoServices mServices;
    Subscription mSubscription;
    ProgressDialog mProgress;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mLoginBinding == null) {
            mLoginBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
            initView();
        }
        return mLoginBinding.getRoot();
    }

    private void initView() {
        mApplication = BaseApplication.getInstance(mActivity);
        mServices = mApplication.getApiServices();
        mLoginBinding.btnLogin.setOnClickListener(this);
        mLoginBinding.btnRegister.setOnClickListener(this);
        mLoginBinding.txtHotline.setOnClickListener(this);
    }

    private boolean validate(String username, String password) {
        if (username.trim().equalsIgnoreCase("") || password.trim().equalsIgnoreCase("")) {
            Toast.makeText(mActivity, getResources().getString(R.string.validate_empty_infor), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void login(String username, String password, String inforDevices) {
        String data = "username=" + username + "&password=" + password + "&infologin=" + inforDevices;
        String sign = mActivity.hmacSha(Constant.KEY_SIGN, data.toLowerCase());
        mProgress =  mActivity.showLoading(mActivity);
        mSubscription = mServices.login(username, password, inforDevices, sign)
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        mSubscription.unsubscribe();
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            if (!jsonObject.get("data").isJsonNull()) {
                                mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        RealmResults<User> result = realm.where(User.class).findAll();
                                        result.clear();
                                        realm.createObjectFromJson(User.class, jsonObject.getAsJsonObject("data").toString());
                                        if (mProgress != null) {
                                            mActivity.hideLoading(mProgress);
                                        }
                                        mActivity.saveIsLogin(true);
                                        Intent mIntent = new Intent(mActivity, MainActivity.class);
                                        startActivity(mIntent);
                                        mActivity.finish();
                                    }
                                });

                            } else {
                                if (mProgress != null) {
                                    mActivity.hideLoading(mProgress);
                                }
                                mActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(mActivity, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        } else {
                            if (mProgress != null) {
                                mActivity.hideLoading(mProgress);
                            }
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(mActivity, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        if (mProgress != null) {
                            mActivity.hideLoading(mProgress);
                        }
                    }
                });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                if (validate(mLoginBinding.edtUsername.getText().toString().trim(), mLoginBinding.edtPassword.getText().toString().trim())) {
                    login(mLoginBinding.edtUsername.getText().toString().trim(),
                            mLoginBinding.edtPassword.getText().toString().trim(), mActivity.getInfoLogin());
                }
                break;
            case R.id.btn_register:
                ((RegisterActivity) mActivity).pushFragment(new FragmentRegister());
                break;
            case R.id.txt_hotline:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mActivity.askPermission(Manifest.permission.CALL_PHONE, Constant.REQUEST_CALL)) {
                        mActivity.call(mActivity, getResources().getString(R.string.text_phone_help));
                    }
                } else {
                    mActivity.call(mActivity, getResources().getString(R.string.text_phone_help));
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(mActivity, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case Constant.REQUEST_CALL:
                    mActivity.call(mActivity, getResources().getString(R.string.text_phone_help));
                    break;
            }
        }
    }
}
