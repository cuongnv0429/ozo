package com.ozo.ozo.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ozo.ozo.R;
import com.ozo.ozo.activity.DetailActivity;
import com.ozo.ozo.activity.FilterActivity;
import com.ozo.ozo.adapter.NewsFeedsAdapter;
import com.ozo.ozo.customview.ClickItemNewsFeed;
import com.ozo.ozo.customview.ContactListener;
import com.ozo.ozo.customview.DialogConfirmNewsListerner;
import com.ozo.ozo.databinding.FragmentNewFeedsBinding;
import com.ozo.ozo.fragment.dialog.DialogNewsFeed;
import com.ozo.ozo.model.BaseNewsFeed;
import com.ozo.ozo.model.ByDate;
import com.ozo.ozo.model.ByPrice;
import com.ozo.ozo.model.Category;
import com.ozo.ozo.model.DialogNewsReport;
import com.ozo.ozo.model.District;
import com.ozo.ozo.model.NewsFeed;
import com.ozo.ozo.model.NewsSite;
import com.ozo.ozo.model.NewsStatus;
import com.ozo.ozo.model.User;
import com.ozo.ozo.model.callbacks.OnLoadMoreListener;
import com.ozo.ozo.model.request.NewsInHomRequest;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;
import com.ozo.ozo.utils.ShareHelper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 4/10/17.
 */

public class FragmentNewFeeds extends BaseFragment implements View.OnClickListener, NewsFeedsAdapter.IDialogNewsFeed,
        TextWatcher, ClickItemNewsFeed, ContactListener, DialogConfirmNewsListerner{
    private String TAG = FragmentNewFeeds.class.getName();
    BaseActivity mActivity;
    FragmentNewFeedsBinding mNewFeedsBinding;
    NewsFeedsAdapter mAdapter;
    List<NewsFeed> mNewsFeeds;
    private BaseNewsFeed mBaseNewsFeed;
    BaseApplication mApplication;
    OzoServices mServices;
    Subscription mSubscription;
    NewsFeedsAdapter.IDialogNewsFeed iDialogNewsFeed = this;
    private static final int PAGE_LIMIT = 10;
    private int pageIndex = 1;
    CountDownTimer mCountDownTimerSearchChange;
    private boolean isSearch = false;
    ProgressDialog progressDialog;
    ClickItemNewsFeed clickItemNewsFeed = this;
    ContactListener contactListener = this;
    private DialogConfirmNewsListerner dialogConfirmNewsListerner = this;
    private String phone = "";
    private Double minPrice = -1.0;
    private Double maxPrice = -1.0;
    private int mByDate = -1;
    User mUser;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mNewFeedsBinding == null) {
            mNewFeedsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_feeds, container, false);
            mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
            initView();
        }
        return mNewFeedsBinding.getRoot();
    }

    private void initView() {
        mNewFeedsBinding.mainToolbar.txtFilter.setOnClickListener(this);
        mNewFeedsBinding.mainToolbar.txtFilter.setVisibility(View.VISIBLE);
        mNewFeedsBinding.mainToolbar.edtSearch.addTextChangedListener(this);
        mNewFeedsBinding.mainToolbar.imgExitSearch.setOnClickListener(this);

        mNewFeedsBinding.txtEmptyInfor.setVisibility(View.GONE);

        mApplication = BaseApplication.getInstance(mActivity);
        mServices = mApplication.getApiServices();

        mNewsFeeds = new ArrayList<>();
        mBaseNewsFeed = new BaseNewsFeed();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mNewFeedsBinding.recyclerNewsFeed.setLayoutManager(mLayoutManager);
        mNewFeedsBinding.recyclerNewsFeed.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new NewsFeedsAdapter(mNewFeedsBinding.recyclerNewsFeed, mNewsFeeds, mActivity, iDialogNewsFeed, clickItemNewsFeed);
        mNewFeedsBinding.recyclerNewsFeed.setAdapter(mAdapter);

        mNewFeedsBinding.swiperefreshNewsFeed.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mUser != null && mUser.isValid()) {
                    if (mUser.getIsPayment()) {
                        mNewFeedsBinding.txtIsPayment.setVisibility(View.GONE);
                        pageIndex = 1;
                        mAdapter.setLoaded();
                        progressDialog = mActivity.showLoading(mActivity);
                        getListNewFeed(pageIndex);
                    } else {
                        mNewFeedsBinding.swiperefreshNewsFeed.setRefreshing(false);
                        mNewFeedsBinding.txtIsPayment.setVisibility(View.VISIBLE);
                        mNewFeedsBinding.progressbarNewsFeed.setVisibility(View.GONE);
                    }
                }

            }
        });

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mNewsFeeds.add(null);
                mAdapter.notifyItemInserted(mNewsFeeds.size() - 1);
                pageIndex += 1;
                getListNewFeed(pageIndex);
            }
        });
        mUser = mActivity.getUserInfo();
        if (mUser != null && mUser.isValid()) {
            if (mUser.getIsPayment()) {
                mNewFeedsBinding.txtIsPayment.setVisibility(View.GONE);
                pageIndex = 1;
                getListNewFeed(pageIndex);
            } else {
                mNewFeedsBinding.txtIsPayment.setVisibility(View.VISIBLE);
                mNewFeedsBinding.progressbarNewsFeed.setVisibility(View.GONE);
            }

        }

        //listener event Rxbus from FragmentDetail
        RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof DialogNewsReport) {
                    try {
                        int position = ((DialogNewsReport) o).getPosition();
                        boolean isReason = (boolean) ((DialogNewsReport) o).getContent();
                        Log.e(TAG, position + "__" + isReason);
                        if (position >= 0) {
                            mNewsFeeds.get(position).setReason(isReason);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }


                }
            }
        });
    }

    private void defineMinMaxPrice(ByPrice byPrice) {
        switch (byPrice.getId()) {
            case "1":
                minPrice = -1.0;
                maxPrice= -1.0;
                break;
            case "2":
                minPrice = 0.0;
                maxPrice= 1000000.0;
                break;
            case "3":
                minPrice = 1000000.0;
                maxPrice= 5000000.0;
                break;
            case "4":
                minPrice = Double.parseDouble(String.valueOf(5000000));
                maxPrice= Double.parseDouble(String.valueOf(10000000));
                break;
            case "5":
                minPrice = 10000000.0;
                maxPrice= 50000000.0;
                break;
            case "6":
                minPrice = 50000000.0;
                maxPrice= 100000000.0;
                break;
            case "7":
                minPrice = 100000000.0;
                maxPrice= 500000000.0;
                break;
            case "8":
                minPrice = 500000000.0;
                maxPrice= 1000000000.0;
                break;
            case "9":
                minPrice = 1000000000.0;
                maxPrice= 5000000000.0;
                break;
            case "10":
                minPrice = 5000000000.0;
                maxPrice= 10000000000.0;
                break;
            case "11":
                minPrice = 10000000000.0;
                maxPrice= -1.0;
                break;
            default:
                minPrice = -1.0;
                maxPrice= -1.0;
                break;
        }
    }


    private void getListNewFeed(final int pageIndex) {
        mNewFeedsBinding.txtIsPayment.setVisibility(View.GONE);
        if (mAdapter.getItemCount() < 1 || isSearch) {
            mNewFeedsBinding.progressbarNewsFeed.setVisibility(View.VISIBLE);
        } else {
            mNewFeedsBinding.progressbarNewsFeed.setVisibility(View.GONE);
        }
        NewsInHomRequest newsInHomRequest = new NewsInHomRequest();

        Category mCategory = new Gson().fromJson(ShareHelper.getCategory(mActivity, TAG), Category.class);
        District mDistrict = new Gson().fromJson(ShareHelper.getDistrict(mActivity, TAG), District.class);
        District mProvince = new Gson().fromJson(ShareHelper.getProvince(mActivity, TAG), District.class);
        NewsStatus mNewsStatus = new Gson().fromJson(ShareHelper.getNewsStatus(mActivity, TAG), NewsStatus.class);
        final NewsSite mNewsSite = new Gson().fromJson(ShareHelper.getNewsSite(mActivity, TAG), NewsSite.class);
        ByPrice byPrice = new Gson().fromJson(ShareHelper.getByPrice(mActivity, TAG), ByPrice.class);
        ByDate byDate = new Gson().fromJson(ShareHelper.getByDate(mActivity, TAG), ByDate.class);
        defineMinMaxPrice(byPrice);
        mByDate = mActivity.defineByDate(byDate);
        Log.e(TAG, mByDate + "");
        String fromDate = ShareHelper.getFromDate(mActivity, TAG);
        String toDate = ShareHelper.getToDate(mActivity, TAG);

        newsInHomRequest.setUserId(mUser.getId());
        newsInHomRequest.setCateId(mCategory.getId());
        newsInHomRequest.setProvinceId(mProvince.getId());
        newsInHomRequest.setDistrictId(mDistrict.getId());
        newsInHomRequest.setStatusId(mNewsStatus.getId());
        newsInHomRequest.setSiteId(mNewsSite.getId());
        newsInHomRequest.setBackDate(mByDate);
        newsInHomRequest.setFrom(fromDate);
        newsInHomRequest.setTo(toDate);
        newsInHomRequest.setMinPrice(minPrice);
        newsInHomRequest.setMaxPrice(maxPrice);
        newsInHomRequest.setPageIndex(pageIndex);
        newsInHomRequest.setPageSize(PAGE_LIMIT);
        newsInHomRequest.setRepeat(false);
        newsInHomRequest.setKey(mNewFeedsBinding.mainToolbar.edtSearch.getText().toString().trim());
        newsInHomRequest.setNameOrder("");
        newsInHomRequest.setDescending(false);
        String sign = "userid=" + newsInHomRequest.getUserId() + "&cateid=" + newsInHomRequest.getCateId() +
                "&provinceid=" + newsInHomRequest.getProvinceId() + "&districid=" + newsInHomRequest.getDistrictId() +
                "&statusid=" + newsInHomRequest.getStatusId() + "&siteid=" + newsInHomRequest.getSiteId() + "&backdate=" +
                newsInHomRequest.getBackDate() + "&from=" + newsInHomRequest.getFrom() + "&to=" + newsInHomRequest.getTo() +
                "&minprice=" + (long) newsInHomRequest.getMinPrice() + "&maxprice=" + (long) newsInHomRequest.getMaxPrice() + "&pageindex=" +
                newsInHomRequest.getPageIndex() + "&pagesize=" + newsInHomRequest.getPageSize() + "&isrepeat=" + newsInHomRequest.isRepeat() +
                "&key=" + newsInHomRequest.getKey() + "&nameorder=" + newsInHomRequest.getNameOrder() + "&descending=" + newsInHomRequest.isDescending();
        newsInHomRequest.setSign(mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase()));

        mSubscription = mServices.getListNewsInHome(newsInHomRequest.getUserId(), newsInHomRequest.getCateId(),
                newsInHomRequest.getProvinceId(), newsInHomRequest.getDistrictId(), newsInHomRequest.getStatusId(), newsInHomRequest.getSiteId(),
                newsInHomRequest.getBackDate(), newsInHomRequest.getFrom(), newsInHomRequest.getTo(), newsInHomRequest.getMinPrice(),
                newsInHomRequest.getMaxPrice(), newsInHomRequest.getPageIndex(), newsInHomRequest.getPageSize(), newsInHomRequest.isRepeat(),
                newsInHomRequest.getKey(), newsInHomRequest.getNameOrder(), newsInHomRequest.isDescending(),
                newsInHomRequest.getSign(), mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        if (mNewFeedsBinding.swiperefreshNewsFeed.isRefreshing()) {
                            mActivity.hideLoading(progressDialog);
                            mNewFeedsBinding.swiperefreshNewsFeed.setRefreshing(false);
                            mNewsFeeds.clear();
                            mAdapter.notifyDataSetChanged();
                        }
                        mNewFeedsBinding.progressbarNewsFeed.setVisibility(View.GONE);
                        mNewFeedsBinding.txtEmptyInfor.setVisibility(View.GONE);
                        if (mAdapter.isLoading()) {
                            mNewsFeeds.remove(mNewsFeeds.size() - 1);
                            mAdapter.notifyDataSetChanged();
                        }

                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            if (mAdapter.isLoading()) {
                                mAdapter.setLoaded();
                            }
                            if (!jsonObject.getAsJsonObject("data").isJsonNull()) {
                                Type collectionType = new TypeToken<BaseNewsFeed>(){}.getType();
                                mBaseNewsFeed = new Gson().fromJson(jsonObject.getAsJsonObject("data"), collectionType);
                                if (mBaseNewsFeed.getNewsFeedList().size() > 0 || isSearch) {
                                    if (isSearch) {
                                        isSearch = false;
                                        if (mBaseNewsFeed.getNewsFeedList().size() < 1) {
                                            if (pageIndex == 1) {
                                                mNewFeedsBinding.txtEmptyInfor.setVisibility(View.VISIBLE);
                                            }

                                        }
                                    }
                                    int positin = mNewsFeeds.size();
                                    mNewsFeeds.addAll(mNewsFeeds.size(), mBaseNewsFeed.getNewsFeedList());
                                    mAdapter.notifyItemInserted(positin);

                                } else {
                                    if (mNewsFeeds.size() < 1) {
                                        mNewFeedsBinding.txtEmptyInfor.setVisibility(View.VISIBLE);
                                    }
                                }
                            } else if (pageIndex == 1) {
                                mNewFeedsBinding.txtEmptyInfor.setVisibility(View.VISIBLE);
                            }
                        } else if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                            if (mActivity.getIsLogin()) {
                                mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        RealmResults<User> result = realm.where(User.class).findAll();
                                        result.clear();
                                        mActivity.saveIsLogin(false);
                                        mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                    }
                                });
                            }
                        }


                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mNewFeedsBinding.progressbarNewsFeed.setVisibility(View.GONE);
                        if (mNewFeedsBinding.swiperefreshNewsFeed.isRefreshing()) {
                            mActivity.hideLoading(progressDialog);
                            mNewFeedsBinding.swiperefreshNewsFeed.setRefreshing(false);
                        }
                        if (mNewsFeeds != null &&  mNewsFeeds.size() < 1) {
                            mNewFeedsBinding.txtEmptyInfor.setVisibility(View.VISIBLE);
                        }
                        if (mAdapter.isLoading()) {
                            mNewsFeeds.remove(mNewsFeeds.size() - 1);
                            mAdapter.notifyDataSetChanged();
                            mAdapter.setLoaded();
                        }
                    }
                });

    }

    private void initNewsFeedList() {
        if (mUser.getIsPayment()) {
            isSearch = false;
            pageIndex = 1;
            mAdapter.setLoaded();
            mNewsFeeds.clear();
            mAdapter.notifyDataSetChanged();
            mNewFeedsBinding.txtEmptyInfor.setVisibility(View.GONE);
            getListNewFeed(pageIndex);
        }
    }

    private void search(final String keySearch) {
        if (mCountDownTimerSearchChange != null) {
            mCountDownTimerSearchChange.cancel();
        }
        mCountDownTimerSearchChange = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                isSearch = true;
                pageIndex = 1;
                mAdapter.setLoaded();
                mNewsFeeds.clear();
                mAdapter.notifyDataSetChanged();
                mNewFeedsBinding.txtEmptyInfor.setVisibility(View.GONE);
                getListNewFeed(pageIndex);
            }
        };
        mCountDownTimerSearchChange.start();
    }

    private void saveNews(int []idNews, int idUser, final int position) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        Log.e(TAG, sign);
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        Log.e(TAG, sign);
        mSubscription = mServices.userSaveNews(idNews, idUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, new Gson().toJson(jsonObject));
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mNewsFeeds.remove(position);
                            mAdapter.notifyDataSetChanged();
                            mActivity.hideLoading(progressDialog);
                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    private void hideNews(int []idNews, int idUser, final int position) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        Log.e(TAG, sign);
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        Log.e(TAG, sign);
        mSubscription = mServices.userHideNews(idNews, idUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, new Gson().toJson(jsonObject));
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mNewsFeeds.remove(position);
                            mAdapter.notifyItemChanged(position);
                            mActivity.hideLoading(progressDialog);
                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(progressDialog);
                    }
                });
    }

    private void rejectNews(int []idNews, int idUser, final int position) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.newsSpam(idNews, idUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, new Gson().toJson(jsonObject));
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mNewsFeeds.remove(position);
                            mAdapter.notifyItemChanged(position);
                            mActivity.hideLoading(progressDialog);
                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(progressDialog);
                    }
                });
    }

    private void forUserNews(int []idNews, int idUser, final int position) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.newsForUser(idNews, idUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, new Gson().toJson(jsonObject));
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mNewsFeeds.get(position).setCC(true);
                            mAdapter.notifyItemChanged(position);
                            mActivity.hideLoading(progressDialog);
                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(progressDialog);
                    }
                });
    }

    private void removeForUserNews(int []idNews, int idUser, final int position) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.removeNewsForUser(idNews, idUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, new Gson().toJson(jsonObject));
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mNewsFeeds.get(position).setCC(false);
                            mAdapter.notifyItemChanged(position);
                            mActivity.hideLoading(progressDialog);
                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(progressDialog);
                    }
                });
    }

    private void agencyReport(int []idNews, int idUser, final int position) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.agencyNews(idNews, idUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            //cho param bao moi gioi
                            mActivity.hideLoading(progressDialog);
                            mNewsFeeds.remove(position);
                            mAdapter.notifyDataSetChanged();
                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(progressDialog);
                    }
                });
    }

    private void newsDelete(int []idNews, int idUser, final int position) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.newsDelete(idNews, idUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mNewsFeeds.remove(position);
                            mAdapter.notifyItemChanged(position);
                            mActivity.hideLoading(progressDialog);
                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(progressDialog);
                    }
                });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_filter:
                Intent intent = new Intent(mActivity, FilterActivity.class);
                intent.putExtra("tag", TAG);
                startActivityForResult(intent, Constant.REQUEST_FILTER_NEWS_FEED);
                break;
            case R.id.img_exit_search:
                mActivity.hideKeyboard();
                mNewFeedsBinding.mainToolbar.edtSearch.setText("");
                break;
        }
    }

    @Override
    public void showDialog(int position, boolean isCC) {
        DialogNewsFeed dialogNewsFeed = new DialogNewsFeed();
        boolean isStaff = !mActivity.getUserInfo().getIsUser();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isStaff", isStaff);
        bundle.putBoolean("isCC", isCC);
        bundle.putInt("position", position);
        dialogNewsFeed.setArguments(bundle);
        dialogNewsFeed.setTargetFragment(this, DialogNewsFeed.REQUEST_CODE);
        dialogNewsFeed.show(mActivity.getSupportFragmentManager(), TAG);
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (!s.toString().equalsIgnoreCase("") || (count == 0 && before != 0)) {
            if (count != 0 && before != 0) {
                mNewFeedsBinding.mainToolbar.imgExitSearch.setVisibility(View.VISIBLE);
            } else {
                mNewFeedsBinding.mainToolbar.imgExitSearch.setVisibility(View.GONE);
            }
            if (mUser != null && mUser.isValid()) {
                if (mUser.getIsPayment()) {
                    mNewFeedsBinding.txtIsPayment.setVisibility(View.GONE);
                    search(s.toString().trim());
                } else {
                    mNewFeedsBinding.txtIsPayment.setVisibility(View.VISIBLE);
                    mNewFeedsBinding.progressbarNewsFeed.setVisibility(View.GONE);
                }
            }

        } else {
            mNewFeedsBinding.mainToolbar.imgExitSearch.setVisibility(View.GONE);
        }

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onClickItemNewsFeed(int item, int position) {
        switch (item) {
            case Constant.NEWS_SAVE:
                if (mActivity.getUserInfo() != null) {
                    progressDialog = mActivity.showLoading(mActivity);
                    int idNews [] = {mNewsFeeds.get(position).getId()};
                    saveNews(idNews, mActivity.getUserInfo().getId(), position);
                }
                break;
            case Constant.NEWS_HIDE:
                if (mActivity.getUserInfo() != null) {
                    progressDialog = mActivity.showLoading(mActivity);
                    int idNews [] = {mNewsFeeds.get(position).getId()};
                    hideNews(idNews, mActivity.getUserInfo().getId(), position);
                }
                break;
            case Constant.NEWS_REJECT:
                mActivity.dialogConfirmNews(mActivity, getResources().getString(R.string.dialog_confirm_news_reject), dialogConfirmNewsListerner,
                        Constant.NEWS_REJECT, position);
                break;
            case Constant.NEWS_FOR_USER:
                if (mActivity.getUserInfo() != null) {
                    progressDialog = mActivity.showLoading(mActivity);
                    int idNews [] = {mNewsFeeds.get(position).getId()};
                    forUserNews(idNews, mActivity.getUserInfo().getId(), position);
                }
                break;
            case Constant.FOR_USER_EDIT:
                if (mActivity.getUserInfo() != null) {
                    progressDialog = mActivity.showLoading(mActivity);
                    int idNews [] = {mNewsFeeds.get(position).getId()};
                    removeForUserNews(idNews, mActivity.getUserInfo().getId(), position);
                }
                break;
            case Constant.CONTACT:
                phone = mNewsFeeds.get(position).getPhone();
                mActivity.contact(mActivity, this);
                break;
            case Constant.DETAIL_NEWSFEED:
                Intent intent = new Intent(mActivity, DetailActivity.class);
                intent.putExtra("news_id", mNewsFeeds.get(position).getId());
                intent.putExtra("position", position);
                intent.putExtra("is_reason", mNewsFeeds.get(position).isReason());
                startActivityForResult(intent, Constant.DETAIL_NEWSFEED);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(mActivity, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case Constant.REQUEST_CALL:
                    mActivity.call(mActivity, phone);
                    break;
                case Constant.REQUEST_SMS:
                    mActivity.sendMessage(phone);
                    break;
            }
        }
    }

    private void resultDialogNewsFeed(int value, int position) {
        switch (value) {
            case Constant.NEWS_FOR_USER:
                if (mActivity.getUserInfo() != null) {
                    progressDialog = mActivity.showLoading(mActivity);
                    int idNews [] = {mNewsFeeds.get(position).getId()};
                    forUserNews(idNews, mActivity.getUserInfo().getId(), position);
                }
                break;
            case Constant.FOR_USER_EDIT:
                if (mActivity.getUserInfo() != null) {
                    progressDialog = mActivity.showLoading(mActivity);
                    int idNews [] = {mNewsFeeds.get(position).getId()};
                    removeForUserNews(idNews, mActivity.getUserInfo().getId(), position);
                }
                break;
            case Constant.AGENCY_REPORT:
                if (mActivity.getUserInfo() != null) {
                    progressDialog = mActivity.showLoading(mActivity);
                    int idNews [] = {mNewsFeeds.get(position).getId()};
                    agencyReport(idNews, mActivity.getUserInfo().getId(), position);
                }
                break;
            case Constant.REPORT:
                break;
            case Constant.DELETE:
                mActivity.dialogConfirmNews(mActivity, getResources().getString(R.string.dialog_confirm_news_delete),
                        dialogConfirmNewsListerner, Constant.DELETE, position);
                break;
            case Constant.CANCEL:
                break;

        }
    }

    private void resultDetailNewsFeed(int value, int position) {
        switch (value) {
            case Constant.AGENCY_REPORT:
                mNewsFeeds.remove(position);
                mAdapter.notifyDataSetChanged();
                break;
            case Constant.NEWS_FOR_USER:
                mNewsFeeds.get(position).setCC(true);
                mAdapter.notifyItemChanged(position);
                break;
            case Constant.NEWS_REJECT:
                mNewsFeeds.remove(position);
                mAdapter.notifyDataSetChanged();
                break;
            case Constant.NEWS_SAVE:
                mNewsFeeds.remove(position);
                mAdapter.notifyDataSetChanged();
                break;
            case Constant.NEWS_HIDE:
                mNewsFeeds.remove(position);
                mAdapter.notifyDataSetChanged();
                break;
//            case Constant.REPORT:
//                mNewsFeeds.remove(position);
//                mAdapter.notifyItemRemoved(position);
////                initView();
//                break;
            case Constant.FOR_USER_EDIT:
                mNewsFeeds.get(position).setCC(false);
                mAdapter.notifyItemChanged(position);
                break;
            case Constant.DELETE:
                mNewsFeeds.remove(position);
                mAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case DialogNewsFeed.REQUEST_CODE:
                int value = data.getIntExtra(
                        DialogNewsFeed.KEY, 0);
                int position = data.getIntExtra(
                        "position", 0);
                resultDialogNewsFeed(value, position);
                break;
            case Constant.DETAIL_NEWSFEED:
                if (resultCode == Activity.RESULT_OK) {
                    int valueDetail = data.getIntExtra(
                            "key", 0);
                    int positionDetail = data.getIntExtra(
                            "position", 0);
                    resultDetailNewsFeed(valueDetail, positionDetail);
                }
                break;
            case Constant.REQUEST_FILTER_NEWS_FEED:
                if (resultCode == Activity.RESULT_OK) {
                    initNewsFeedList();
                }
                break;
        }
    }

    @Override
    public void contact(String action) {
        switch (action) {
            case "call":
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mActivity.askPermission(Manifest.permission.CALL_PHONE, Constant.REQUEST_CALL)) {
                        mActivity.call(mActivity, phone);
                    }
                } else {
                    mActivity.call(mActivity, phone);
                }
                break;
            case "message":
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mActivity.askPermission(Manifest.permission.SEND_SMS, Constant.REQUEST_SMS)) {
                        mActivity.sendMessage(phone);
                    }
                } else {
                    mActivity.sendMessage(phone);
                }
                break;
        }
    }

    @Override
    public void dialogOk(int type, int position) {
        switch (type) {
            case Constant.DELETE:
                if (mActivity.getUserInfo() != null) {
                    progressDialog = mActivity.showLoading(mActivity);
                    int idNews [] = {mNewsFeeds.get(position).getId()};
                    newsDelete(idNews, mActivity.getUserInfo().getId(), position);
                }
                break;
            case Constant.NEWS_REJECT:
                if (mActivity.getUserInfo() != null) {
                    progressDialog = mActivity.showLoading(mActivity);
                    int idNews [] = {mNewsFeeds.get(position).getId()};
                    rejectNews(idNews, mActivity.getUserInfo().getId(), position);
                }
                break;
        }
    }

    @Override
    public void dialogCancle(int type, int position) {

    }
}
