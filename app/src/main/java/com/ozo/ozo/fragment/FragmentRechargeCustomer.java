package com.ozo.ozo.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ozo.ozo.R;
import com.ozo.ozo.activity.SettingActivity;
import com.ozo.ozo.customview.DialogConfirmListerner;
import com.ozo.ozo.databinding.FragmentRechargeCustomerBinding;
import com.ozo.ozo.model.Customer;
import com.ozo.ozo.model.Event;
import com.ozo.ozo.model.Payment;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;
import com.ozo.ozo.utils.ShareHelper;

import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 7/14/17.
 */

public class FragmentRechargeCustomer extends BaseFragment implements View.OnClickListener, DialogConfirmListerner{

    private String TAG = "RechargeCustomer";
    BaseActivity mActivity;
    FragmentRechargeCustomerBinding fragmentRechargeCustomerBinding;
    Customer customer;
    private String tag = "";
    Subscription mSubscription;
    OzoServices mServices;
    DialogConfirmListerner dialogConfirm = this;
    ProgressDialog mProgressDialog;

    public static FragmentRechargeCustomer newInstance(Customer customer, String tag) {
        Bundle args = new Bundle();
        args.putSerializable("customer", customer);
        args.putString("tag", tag);
        FragmentRechargeCustomer fragment = new FragmentRechargeCustomer();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            customer = (Customer) getArguments().getSerializable("customer");
            tag = getArguments().getString("tag");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (fragmentRechargeCustomerBinding == null) {
            fragmentRechargeCustomerBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_recharge_customer, container, false);
            initView();
        }
        return fragmentRechargeCustomerBinding.getRoot();
    }

    private void initView() {
        fragmentRechargeCustomerBinding.toolbarRechargeCustomer.txtToolbarCancel.setOnClickListener(this);
        fragmentRechargeCustomerBinding.toolbarRechargeCustomer.txtToolbarPost.setOnClickListener(this);
        fragmentRechargeCustomerBinding.linearPayments.setOnClickListener(this);
        fragmentRechargeCustomerBinding.toolbarRechargeCustomer.txtToolbarPost.setText(getResources().getString(R.string.text_done));
        fragmentRechargeCustomerBinding.toolbarRechargeCustomer.txtTitleToolbar.setText(getResources().getString(R.string.text_recharge));
        Payment payment = new Gson().fromJson(ShareHelper.getPaymentMethod(mActivity, tag), Payment.class);
        fragmentRechargeCustomerBinding.txtPaymentMethod.setText(payment.getText());
        if (customer != null) {
            fragmentRechargeCustomerBinding.txtAccount.setText(customer.getUserName());
        }

        mServices = BaseApplication.getInstance(mActivity).getApiServices();

        RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof Event) {
                    switch (((Event) o).getType()) {
                        case Constant.PAYMENT_METHOD:
                            fragmentRechargeCustomerBinding.txtPaymentMethod.setText(((Event) o).getContent().toString());
                            break;
                    }

                }
            }
        });

    }

    private void rechargeByCustomer(int customerId, int paymentMethodId, String note, long amount, int userId) {
        String sign = "cusid=" + customerId + "&paymentmethodid=" + paymentMethodId + "&note=" + note +
                "&amount=" + amount;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.insertPayment(customerId, paymentMethodId, note, amount, sign, userId, mActivity.getInfoLogin())
                .subscribeOn(Schedulers.io())
                .observeOn(BaseApplication.getInstance(mActivity).subscribeScheduler())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject jsonObject) {
                        Log.e(TAG, jsonObject.toString());
                        mActivity.hideLoading(mProgressDialog);
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            Toast.makeText(mActivity, "Bạn đã nạp tiền thành công!", Toast.LENGTH_SHORT).show();
                            mActivity.onBackPressed();
                        } else {
                            Toast.makeText(mActivity, jsonObject.get("errorcode").getAsString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    private boolean validateForm() {
        Payment payment = new Gson().fromJson(ShareHelper.getPaymentMethod(mActivity, tag), Payment.class);
        if (payment.getValue().equalsIgnoreCase("0")) {
            Toast.makeText(mActivity, "Vui lòng chọn hình thức thanh toán!", Toast.LENGTH_SHORT).show();
            return true;
        }

        if (fragmentRechargeCustomerBinding.edtMoneys.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(mActivity, "Vui lòng nhập số tiền cần chuyển!", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_toolbar_cancel:
                mActivity.onBackPressed();
                break;
            case R.id.txt_toolbar_post:
                if (!validateForm()) {
                    mActivity.dialogConfirm(mActivity, "Bạn có chắc chắn hoàn tất thao tác!", dialogConfirm);
                }
                break;
            case R.id.linear_payments:
                ((SettingActivity) mActivity).pushFragment(FragmentPaymentMethod.newInstance(tag));
                break;
        }
    }

    @Override
    public void dialogOk() {
        if (mActivity.getUserInfo() != null) {
            mProgressDialog = mActivity.showLoading(mActivity);
            Payment payment = new Gson().fromJson(ShareHelper.getPaymentMethod(mActivity, tag), Payment.class);
            rechargeByCustomer(customer.getId(), Integer.parseInt(payment.getValue()), fragmentRechargeCustomerBinding.edtContent.getText().toString().trim(),
                    Long.parseLong(fragmentRechargeCustomerBinding.edtMoneys.getText().toString().trim()), mActivity.getUserInfo().getId());
        }
    }

    @Override
    public void dialogCancle() {

    }
}
