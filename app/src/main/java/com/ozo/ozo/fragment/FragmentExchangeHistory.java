package com.ozo.ozo.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ozo.ozo.R;
import com.ozo.ozo.adapter.ExchangeHistoryAdapter;
import com.ozo.ozo.databinding.FragmentExchangeHistoryBinding;
import com.ozo.ozo.model.ExchangeHistory;
import com.ozo.ozo.model.User;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by user on 10/10/17.
 */

public class FragmentExchangeHistory extends BaseFragment implements View.OnClickListener{
    private String TAG = "FragmentExchangeHistory";
    private BaseActivity mActivity;
    FragmentExchangeHistoryBinding exchangeHistoryBinding;
    private Subscription mSubscription;
    private OzoServices ozoServices;
    private User mUser;
    private int cusId;
    private ExchangeHistoryAdapter exchangeHistoryAdapter;
    private List<ExchangeHistory> exchangeHistoryList;

    public static FragmentExchangeHistory newInstance(int cusId) {

        Bundle args = new Bundle();
        args.putInt("cus_id", cusId);
        FragmentExchangeHistory fragment = new FragmentExchangeHistory();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            if (getArguments().containsKey("cus_id")) {
                cusId = getArguments().getInt("cus_id");
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (exchangeHistoryBinding == null) {
            exchangeHistoryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_exchange_history, container, false);
            initView();
        }
        return exchangeHistoryBinding.getRoot();
    }

    private void initView() {
        mUser = mActivity.getUserInfo();
        exchangeHistoryList = new ArrayList<>();
        ozoServices = BaseApplication.getInstance(mActivity).getApiServices();
        exchangeHistoryBinding.exchangeHistoryToolbar.txtTitleToolbar.setText(getResources().getString(R.string.title_exchange_history));
        exchangeHistoryBinding.exchangeHistoryToolbar.imgBack.setOnClickListener(this);

        exchangeHistoryBinding.recyclerExchangeHistory.setLayoutManager(new LinearLayoutManager(mActivity));
        exchangeHistoryBinding.recyclerExchangeHistory.setHasFixedSize(true);
        exchangeHistoryBinding.recyclerExchangeHistory.addItemDecoration(new DividerItemDecoration(mActivity, DividerItemDecoration.VERTICAL));
        exchangeHistoryAdapter = new ExchangeHistoryAdapter(mActivity, exchangeHistoryList);
        exchangeHistoryBinding.recyclerExchangeHistory.setAdapter(exchangeHistoryAdapter);

        exchangeHistoryBinding.swiperefreshExchangeHistory.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getListExchangeHistory(cusId, 0, mUser.getId());
            }
        });
        if (mUser != null) {
            getListExchangeHistory(cusId, 0, mUser.getId());
        }
    }

    private void getListExchangeHistory(int cusId, int page, int userId) {
        if (!exchangeHistoryBinding.swiperefreshExchangeHistory.isRefreshing()) {
            exchangeHistoryBinding.progressBar.setVisibility(View.VISIBLE);
        }

        String sign = "cusId=" + cusId + "&Page=" + page;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = ozoServices.getListExchangeHistory(cusId, page, sign, userId, mActivity.getInfoLogin())
                .observeOn(BaseApplication.getInstance(mActivity).subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, jsonObject.toString());
                        exchangeHistoryBinding.progressBar.setVisibility(View.GONE);
                        exchangeHistoryBinding.swiperefreshExchangeHistory.setRefreshing(false);
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            if (!jsonObject.getAsJsonObject("data").getAsJsonArray("PaymentHisList").isJsonNull()) {
                                Type mCollections = new TypeToken<List<ExchangeHistory>>(){}.getType();
                                exchangeHistoryList = new Gson().fromJson(jsonObject.getAsJsonObject("data").getAsJsonArray("PaymentHisList"),
                                        mCollections);
                                if (exchangeHistoryList.size() > 1) {
                                    exchangeHistoryBinding.recyclerExchangeHistory.setVisibility(View.VISIBLE);
                                    exchangeHistoryBinding.txtEmptyExchangeHistory.setVisibility(View.GONE);
                                    exchangeHistoryAdapter = new ExchangeHistoryAdapter(mActivity, exchangeHistoryList);
                                    exchangeHistoryBinding.recyclerExchangeHistory.setAdapter(exchangeHistoryAdapter);
                                    return;
                                }

                            }
                            exchangeHistoryBinding.recyclerExchangeHistory.setVisibility(View.GONE);
                            exchangeHistoryBinding.txtEmptyExchangeHistory.setVisibility(View.VISIBLE);
                        } else {
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        Log.e(TAG, "error");
                        exchangeHistoryBinding.swiperefreshExchangeHistory.setRefreshing(false);
                        exchangeHistoryBinding.progressBar.setVisibility(View.GONE);
                        exchangeHistoryBinding.recyclerExchangeHistory.setVisibility(View.GONE);
                        exchangeHistoryBinding.txtEmptyExchangeHistory.setVisibility(View.VISIBLE);

                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                mActivity.onBackPressed();
                break;
        }
    }
}
