package com.ozo.ozo.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ozo.ozo.R;
import com.ozo.ozo.adapter.CardChooseAdapter;
import com.ozo.ozo.databinding.FragmentCardChooseBinding;
import com.ozo.ozo.model.Card;
import com.ozo.ozo.model.User;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

import static com.ozo.ozo.fragment.FragmentPostNews.TAG;

/**
 * Created by ITV01 on 7/14/17.
 */

public class FragmentCardChoose extends BaseFragment implements View.OnClickListener, CardChooseAdapter.IOnCardChoose{
    BaseActivity mActivity;
    FragmentCardChooseBinding cardChooseBinding;
    OzoServices mServices;
    Subscription mSubcriptions;
    CardChooseAdapter mAdapter;
    List<Card> cardList;
    CardChooseAdapter.IOnCardChoose iOnCardChoose = this;
    private int id = -1;
    ProgressDialog progressDialog;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (cardChooseBinding == null) {
            cardChooseBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_card_choose, container, false);
            initView();
        }
        return cardChooseBinding.getRoot();
    }

    private void initView() {
        cardChooseBinding.toolbarCardChoose.txtTitleToolbar.setText(getResources().getString(R.string.text_card_choose));
        cardChooseBinding.toolbarCardChoose.txtToolbarPost.setText(getResources().getString(R.string.text_done));
        cardChooseBinding.toolbarCardChoose.txtToolbarCancel.setOnClickListener(this);
        cardChooseBinding.toolbarCardChoose.txtToolbarPost.setOnClickListener(this);

        mServices = BaseApplication.getInstance(mActivity).getApiServices();
        cardList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
        cardChooseBinding.recyclerCardChoose.setLayoutManager(linearLayoutManager);
        cardChooseBinding.recyclerCardChoose.setHasFixedSize(true);
        cardChooseBinding.recyclerCardChoose.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new CardChooseAdapter(mActivity, cardList, iOnCardChoose);
        cardChooseBinding.recyclerCardChoose.setAdapter(mAdapter);
        if (mActivity.getUserInfo() != null) {
            cardChooseBinding.progressCardChoose.setVisibility(View.VISIBLE);
            getPayment(mActivity.getUserInfo().getId());
        }
    }

    private void getPayment(int userId) {
        String sign = mActivity.hmacSha(Constant.KEY_SIGN, "");
        mSubcriptions = mServices.getPayment(sign, userId)
                .observeOn(BaseApplication.getInstance(mActivity).subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, jsonObject.toString());
                        cardChooseBinding.progressCardChoose.setVisibility(View.GONE);
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            Type collectionType = new TypeToken<List<Card>>(){}.getType();
                            cardList = (List<Card>) new Gson().fromJson(jsonObject.getAsJsonArray("data"), collectionType);
                            mAdapter = new CardChooseAdapter(mActivity, cardList, iOnCardChoose);
                            cardChooseBinding.recyclerCardChoose.setAdapter(mAdapter);
                        } else {
                            Toast.makeText(mActivity, jsonObject.get("errorcode").getAsString(), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    private void registerCard(int userId, int paymentId) {
        if (mSubcriptions != null) {
            mSubcriptions.unsubscribe();
        }
        String sign = "userid=" + userId + "&paymentid=" + paymentId;
        Log.e(TAG, sign);
        sign =  mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        Log.e(TAG, sign);
        mSubcriptions = mServices.registerPackage(userId, paymentId, sign, mActivity.getInfoLogin())
                .observeOn(BaseApplication.getInstance(mActivity).subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, jsonObject.toString());
                        mActivity.hideLoading(progressDialog);
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    User userSaved = realm.where(User.class).findFirst();
                                    userSaved.setIsPayment(true);
                                    mActivity.onBackPressed();
                                }
                            });
                            Toast.makeText(mActivity, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mActivity, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_toolbar_post:
                if (id == -1) {
                    Toast.makeText(mActivity, "Vui lòng chọn gói cước!", Toast.LENGTH_SHORT).show();
                } else {
                    if (mActivity.getUserInfo() != null && mActivity.getUserInfo().isValid()) {
                        progressDialog = mActivity.showLoading(mActivity);
                        registerCard(mActivity.getUserInfo().getId(), id);
                    }
                }
                break;
            case R.id.txt_toolbar_cancel:
                mActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onChooseCard(int name) {
        id = name;
    }
}
