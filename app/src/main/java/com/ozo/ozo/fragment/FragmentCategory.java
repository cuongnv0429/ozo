package com.ozo.ozo.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ozo.ozo.R;
import com.ozo.ozo.adapter.CategoryAdapter;
import com.ozo.ozo.databinding.FragmentCategoryBinding;
import com.ozo.ozo.model.Category;
import com.ozo.ozo.model.Event;
import com.ozo.ozo.model.User;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 5/18/17.
 */

public class FragmentCategory extends BaseFragment implements View.OnClickListener, CategoryAdapter.IOnCategory{
    private static final String TAG = "FragmentCategory";
    BaseActivity mActivity;
    FragmentCategoryBinding mFragmentCategoryBinding;
    BaseApplication mApplication;
    Subscription mSubcription;
    OzoServices mServices;
    List<Category> mCategories;
    CategoryAdapter mCategoryAdapter;
    CategoryAdapter.IOnCategory iOnCategory = this;
    private String tag = "";

    public static FragmentCategory newInstance(String tag) {
        Bundle args = new Bundle();
        args.putString("tag", tag);
        FragmentCategory fragment = new FragmentCategory();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tag = getArguments().getString("tag");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mFragmentCategoryBinding == null) {
            mFragmentCategoryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_category, container, false);
            initView();
        }
        return mFragmentCategoryBinding.getRoot();
    }

    private void initView() {
        mCategories = new ArrayList<>();
        mFragmentCategoryBinding.toolbarFilterDetail.imgBackFilterDetailToolbar.setOnClickListener(this);
        mFragmentCategoryBinding.toolbarFilterDetail.txtFilterDetailToolbar.setText(getResources().getString(R.string.text_category));
        mApplication = BaseApplication.getInstance(mActivity);
        mServices = mApplication.getApiServices();

        mFragmentCategoryBinding.recyclerCategory.setHasFixedSize(true);
        mFragmentCategoryBinding.recyclerCategory.setLayoutManager( new LinearLayoutManager(mActivity));
        mFragmentCategoryBinding.recyclerCategory.addItemDecoration(new DividerItemDecoration(mActivity,LinearLayoutManager.VERTICAL));
        if (mActivity.getUserInfo() != null) {
            getListCategory(mActivity.getUserInfo().getId());
        }

    }

    private void getListCategory(int userId) {
        mFragmentCategoryBinding.progressCategory.setVisibility(View.VISIBLE);
        String sign = mActivity.hmacSha(Constant.KEY_SIGN, "");
        mSubcription = mServices.getlistCategory(sign, userId,  mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, jsonObject.toString());
                        mSubcription.unsubscribe();
                        mFragmentCategoryBinding.progressCategory.setVisibility(View.GONE);
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            if (!jsonObject.get("data").isJsonNull()) {
                                Type collectionType = new TypeToken<List<Category>>(){}.getType();
                                mCategories = (List<Category>) new Gson().fromJson(jsonObject.getAsJsonArray("data"), collectionType);
                                mCategories.add(0, new Category(0, "Tất cả", 0));
                                mCategoryAdapter = new CategoryAdapter(mActivity, mCategories, iOnCategory, tag);
                                mFragmentCategoryBinding.recyclerCategory.setAdapter(mCategoryAdapter);
                            } else {
                                mCategories.clear();
                                mCategories.add(0, new Category(0, "Tất cả", 0));
                                mCategoryAdapter = new CategoryAdapter(mActivity, mCategories, iOnCategory, tag);
                                mFragmentCategoryBinding.recyclerCategory.setAdapter(mCategoryAdapter);
                                mFragmentCategoryBinding.progressCategory.setVisibility(View.GONE);
                            }
                        } else if (mActivity.getIsLogin()) {
                            mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    RealmResults<User> result = realm.where(User.class).findAll();
                                    result.clear();
                                    mActivity.saveIsLogin(false);
                                    mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                }
                            });
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mCategories.clear();
                        mCategories.add(0, new Category(0, "Tất cả", 0));
                        mCategoryAdapter = new CategoryAdapter(mActivity, mCategories, iOnCategory, tag);
                        mFragmentCategoryBinding.recyclerCategory.setAdapter(mCategoryAdapter);
                        mFragmentCategoryBinding.progressCategory.setVisibility(View.GONE);
                        throwable.printStackTrace();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_filter_detail_toolbar:
                mActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onChoose(String name) {
        Event event = new Event(Constant.CATEGORY_TYPE, name);
        RxBus.getInstance().post(event);
        mActivity.onBackPressed();
    }
}
