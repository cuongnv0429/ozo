package com.ozo.ozo.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ozo.ozo.R;
import com.ozo.ozo.adapter.AccountTypeFilterAdapter;
import com.ozo.ozo.adapter.ByMangerFilterAdapter;
import com.ozo.ozo.databinding.FragmentAccountFilterBinding;
import com.ozo.ozo.databinding.FragmentByManagerFilterBinding;
import com.ozo.ozo.model.Account;
import com.ozo.ozo.model.Event;
import com.ozo.ozo.model.Staff;
import com.ozo.ozo.model.User;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

import java.lang.reflect.Type;
import java.util.List;

import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by tug-on(_SM_) on 7/6/2017.
 */

public class FragmentAccountTypeFilter extends BaseFragment implements AccountTypeFilterAdapter.IOnAccount, View.OnClickListener{
    private final String TAG = "FragmentByManagerFilter";
    private BaseActivity mActivity;
    FragmentAccountFilterBinding accountFilterBinding;
    OzoServices mServices;
    Subscription mSubscription;
    User mUser;
    private List<Account> accountList;
    private AccountTypeFilterAdapter mAdapter;
    private String tag = "";
    private AccountTypeFilterAdapter.IOnAccount iOnAccount = this;

    public static FragmentAccountTypeFilter newInstance(String tag) {

        Bundle args = new Bundle();
        args.putString("tag", tag);
        FragmentAccountTypeFilter fragment = new FragmentAccountTypeFilter();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tag = getArguments().getString("tag");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (accountFilterBinding == null) {
            accountFilterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_account_filter, container, false);
            initView();
        }
        return accountFilterBinding.getRoot();
    }

    private void initView() {
        accountFilterBinding.toolbarFilterDetail.imgBackFilterDetailToolbar.setOnClickListener(this);
        accountFilterBinding.toolbarFilterDetail.txtFilterDetailToolbar.setText(getResources().getString(R.string.text_by_manager));
        mServices = BaseApplication.getInstance(mActivity).getApiServices();
        mUser = mActivity.getUserInfo();

        accountFilterBinding.recyclerAccount.setHasFixedSize(true);
        accountFilterBinding.recyclerAccount.setLayoutManager( new LinearLayoutManager(mActivity));
        accountFilterBinding.recyclerAccount.addItemDecoration(new DividerItemDecoration(mActivity,LinearLayoutManager.VERTICAL));

        if (mUser != null) {
            getAcountType(mUser.getId(), 1, mUser.getIsUser());
        }
    }

    private void getAcountType(int userId, final int pageIndex, boolean isUser) {
        String sign = "userid=" + userId + "&page=" + pageIndex + "&isuser=" + isUser;
        Log.e(TAG, sign);
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign);
        Log.e(TAG, sign);
        mSubscription = mServices.getPaymentStatus(userId, pageIndex, isUser, sign)
                .observeOn(BaseApplication.getInstance(mActivity).subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject jsonObject) {
                        Log.e(TAG, jsonObject.toString());
                        mSubscription.unsubscribe();

                        if (!jsonObject.get("data").isJsonNull()) {
                            Type collectionType = new TypeToken<List<Account>>(){}.getType();
                            accountList = (List<Account>) new Gson().fromJson(jsonObject.getAsJsonArray("data"), collectionType);
                            mAdapter = new AccountTypeFilterAdapter(mActivity, accountList, iOnAccount, tag);
                            accountFilterBinding.progressBar.setVisibility(View.GONE);
                            accountFilterBinding.recyclerAccount.setAdapter(mAdapter);
                        } else {
                            accountList.clear();
                            mAdapter = new AccountTypeFilterAdapter(mActivity, accountList, iOnAccount, tag);
                            accountFilterBinding.recyclerAccount.setAdapter(mAdapter);
                            accountFilterBinding.progressBar.setVisibility(View.GONE);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        accountList.clear();
                        mAdapter = new AccountTypeFilterAdapter(mActivity, accountList, iOnAccount, tag);
                        accountFilterBinding.recyclerAccount.setAdapter(mAdapter);
                        accountFilterBinding.progressBar.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void onChoose(String name) {
        Event event = new Event(Constant.ACCOUNT_TYPE, name);
        RxBus.getInstance().post(event);
        mActivity.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_filter_detail_toolbar:
                mActivity.onBackPressed();
                break;
        }
    }
}
