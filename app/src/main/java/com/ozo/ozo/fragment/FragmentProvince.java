package com.ozo.ozo.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ozo.ozo.R;
import com.ozo.ozo.adapter.ProvinceAdapter;
import com.ozo.ozo.databinding.FragmentDistrictBinding;
import com.ozo.ozo.model.District;
import com.ozo.ozo.model.Event;
import com.ozo.ozo.model.User;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by user on 1/6/18.
 */

public class FragmentProvince extends BaseFragment implements View.OnClickListener, ProvinceAdapter.IOnDistrict{
    private static final String TAG = "FragmentDistrict";
    BaseActivity mActivity;
    FragmentDistrictBinding mFragmentDistrictBinding;
    ProvinceAdapter mProvincecAdapter;
    OzoServices mServices;
    List<District> mProvinces;
    Subscription mSubscription;
    BaseApplication mApplication;
    ProvinceAdapter.IOnDistrict mIOnDistrict = this;
    private String tag = "";

    public static FragmentProvince newInstance(String tag) {
        Bundle args = new Bundle();
        args.putString("tag", tag);
        FragmentProvince fragment = new FragmentProvince();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tag = getArguments().getString("tag");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mFragmentDistrictBinding == null) {
            mFragmentDistrictBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_district, container, false);
            initView();
        }
        return mFragmentDistrictBinding.getRoot();
    }

    private void initView() {
        mFragmentDistrictBinding.toolbarFilterDetail.imgBackFilterDetailToolbar.setOnClickListener(this);
        mFragmentDistrictBinding.toolbarFilterDetail.txtFilterDetailToolbar.setText(getResources().getString(R.string.text_district));
        mApplication = BaseApplication.getInstance(mActivity);
        mServices = mApplication.getApiServices();
        mProvinces = new ArrayList<>();
        mProvincecAdapter = new ProvinceAdapter(mActivity, mProvinces, mIOnDistrict, tag);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mFragmentDistrictBinding.recyclerDistrict.setLayoutManager(mLayoutManager);
        mFragmentDistrictBinding.recyclerDistrict.setItemAnimator(new DefaultItemAnimator());
        mFragmentDistrictBinding.recyclerDistrict.setAdapter(mProvincecAdapter);
        if (mActivity.getUserInfo() != null) {
            getListProvince(mActivity.getUserInfo().getId());
        }

    }

    private void getListProvince(int userId) {
        mFragmentDistrictBinding.progressDistrict.setVisibility(View.VISIBLE);
        String sign = mActivity.hmacSha(Constant.KEY_SIGN, "");
        mSubscription = mServices.getListProvince(sign, userId, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, jsonObject.toString());
                        mSubscription.unsubscribe();
                        mFragmentDistrictBinding.progressDistrict.setVisibility(View.GONE);
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            if (!jsonObject.get("data").isJsonNull()) {
                                Type collectionType = new TypeToken<List<District>>(){}.getType();
                                District district = new District();
                                district.setId(0);
                                district.setName(mActivity.getResources().getString(R.string.text_all));
                                mProvinces = (List<District>) new Gson().fromJson(jsonObject.getAsJsonArray("data"), collectionType);
                                mProvinces.add(0, district);
                                mProvincecAdapter = new ProvinceAdapter(mActivity, mProvinces, mIOnDistrict, tag);
                                mFragmentDistrictBinding.recyclerDistrict.setAdapter(mProvincecAdapter);
                            } else {
                                mFragmentDistrictBinding.progressDistrict.setVisibility(View.GONE);
                            }
                        } else if (mActivity.getIsLogin()) {
                            mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    RealmResults<User> result = realm.where(User.class).findAll();
                                    result.clear();
                                    mActivity.saveIsLogin(false);
                                    mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                }
                            });
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mFragmentDistrictBinding.progressDistrict.setVisibility(View.GONE);
                        throwable.printStackTrace();
                    }
                });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_filter_detail_toolbar:
                mActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onChooseDistrict(String name) {
        Event event = new Event(Constant.PROVINCE_TYPE, name);
        RxBus.getInstance().post(event);
        mActivity.onBackPressed();
    }
}
