package com.ozo.ozo.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.ozo.ozo.R;
import com.ozo.ozo.activity.SettingActivity;
import com.ozo.ozo.databinding.FragmentPostNewsBinding;
import com.ozo.ozo.model.Category;
import com.ozo.ozo.model.District;
import com.ozo.ozo.model.Event;
import com.ozo.ozo.model.User;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;
import com.ozo.ozo.utils.ShareHelper;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 4/13/17.
 */

public class FragmentPostNews extends BaseFragment implements View.OnClickListener{
    BaseActivity mActivity;
    FragmentPostNewsBinding mPostNewsBinding;
    public static final String TAG = "FragmentPostNews";
    Category oldCategory;
    District oldDistrict;
    BaseApplication mApplication;
    OzoServices mServices;
    Subscription mSubscription;
    ProgressDialog mProgressDialog;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getView() != null) {
            mActivity.hideKeyboard();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mPostNewsBinding == null) {
            mPostNewsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_post_news, container, false);
            initView();
        }
        mActivity.hideKeyboard();
        return mPostNewsBinding.getRoot();
    }

    private void initView() {

        mApplication = BaseApplication.getInstance(mActivity);
        mServices = mApplication.getApiServices();

        mPostNewsBinding.toolbarPostNews.txtToolbarCancel.setOnClickListener(this);
        mPostNewsBinding.toolbarPostNews.txtToolbarPost.setOnClickListener(this);
        mPostNewsBinding.linearCategoryPostNews.setOnClickListener(this);
        mPostNewsBinding.linearDistrictPostNews.setOnClickListener(this);
        mPostNewsBinding.toolbarPostNews.txtTitleToolbar.setText(getResources().getString(R.string.text_post_news));

        oldCategory = new Gson().fromJson(ShareHelper.getCategory(mActivity, TAG), Category.class);
        mPostNewsBinding.txtCategoryPostNews.setText(oldCategory.getName() );

        oldDistrict = new Gson().fromJson(ShareHelper.getDistrict(mActivity, TAG), District.class);
        mPostNewsBinding.txtDistrictPostNews.setText(oldDistrict.getName() );


        RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof Event) {
                    switch (((Event) o).getType()) {
                        case Constant.CATEGORY_TYPE:
                            mPostNewsBinding.txtCategoryPostNews.setText(((Event) o).getContent().toString());
                            break;
                        case Constant.DISTRICT_TYPE:
                            mPostNewsBinding.txtDistrictPostNews.setText(((Event) o).getContent().toString());
                            break;
                    }
                }
            }
        });
    }

    private void createNews(String title, int cateId, int districtId, String phone, double price, String content,
                          int userId, boolean isUser) {
        String sign = "title=" + title + "&cateid=" + cateId + "&districtid=" + districtId + "&phone=" + phone +
                "&price=" + (int)price + "&content=" + content + "&userid=" + userId + "&isuser=" + isUser;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.createNews(title, cateId, districtId, phone, price, content, userId,
                isUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, jsonObject.toString());
                        mActivity.hideLoading(mProgressDialog);
                        if (!jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            } else {
                                Toast.makeText(mActivity, getResources().getString(R.string.alert_post_news_error), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            mActivity.onBackPressed();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(mProgressDialog);
                        Toast.makeText(mActivity, getResources().getString(R.string.alert_post_news_error), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void postNews() {
        mActivity.hideKeyboard();

        oldCategory = new Gson().fromJson(ShareHelper.getCategory(mActivity, TAG), Category.class);
        oldDistrict = new Gson().fromJson(ShareHelper.getDistrict(mActivity, TAG), District.class);
        String title = mPostNewsBinding.edtTitlePostNews.getText().toString().trim();
        int cateId = oldCategory.getId();
        int districtId = oldDistrict.getId();
        String phone = mPostNewsBinding.edtPhonePostNews.getText().toString().trim();
        double price = 0.0;
        try{
            price= Double.parseDouble(mPostNewsBinding.edtPricePostNews.getText().toString().trim());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        String content = mPostNewsBinding.edtContentPostNews.getText().toString().trim();
        int userId = mActivity.getUserInfo().getId();
        boolean isUser = mActivity.getUserInfo().getIsUser();
        if (!validate()) {
            mProgressDialog = mActivity.showLoading(mActivity);
            createNews(title, cateId, districtId, phone, price, content, userId, isUser);
        }
    }

    private boolean validate() {
        if (mPostNewsBinding.edtPhonePostNews.getText().toString().equalsIgnoreCase("")
                || mPostNewsBinding.edtContentPostNews.getText().toString().equalsIgnoreCase("")
                || mPostNewsBinding.edtPricePostNews.getText().toString().equalsIgnoreCase("")
                || mPostNewsBinding.edtTitlePostNews.getText().toString().equalsIgnoreCase("")
                || mPostNewsBinding.edtTitlePostNews.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(mActivity, getResources().getString(R.string.validate_empty_infor), Toast.LENGTH_SHORT).show();
            return true;
        }

        if (mActivity.validatePhone(mPostNewsBinding.edtPhonePostNews.getText().toString().trim())) {
            Toast.makeText(mActivity, getResources().getString(R.string.validate_phone), Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_toolbar_cancel:
                mActivity.onBackPressed();
                break;
            case R.id.linear_category_post_news:
                ((SettingActivity) mActivity).pushFragment(FragmentCategory.newInstance(TAG));
                break;
            case R.id.linear_district_post_news:
                ((SettingActivity) mActivity).pushFragment(FragmentDistrict.newInstance(TAG));
                break;
            case R.id.txt_toolbar_post:
                postNews();
                break;
        }
    }
}
