package com.ozo.ozo.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ozo.ozo.R;
import com.ozo.ozo.adapter.ByDateAdapter;
import com.ozo.ozo.databinding.FragmentByDateBinding;
import com.ozo.ozo.model.ByDate;
import com.ozo.ozo.model.Event;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ITV01 on 5/18/17.
 */

public class FragmentByDate extends BaseFragment implements View.OnClickListener, ByDateAdapter.IOnByDate{
    private static final String TAG = "FragmentByDate";
    BaseActivity mActivity;
    FragmentByDateBinding mFragmentByDateBinding;
    ByDateAdapter mByDateAdapter;
    List<ByDate> mByDates;
    ByDateAdapter.IOnByDate mIOnByDate = this;
    private String tag = "";

    public static FragmentByDate newInstance(String tag) {

        Bundle args = new Bundle();
        args.putString("tag", tag);
        FragmentByDate fragment = new FragmentByDate();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tag = getArguments().getString("tag");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mFragmentByDateBinding == null) {
            mFragmentByDateBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_by_date, container, false);
            initView();
        }
        return mFragmentByDateBinding.getRoot();
    }

    private void initView() {

        mFragmentByDateBinding.toolbarFilterDetail.imgBackFilterDetailToolbar.setOnClickListener(this);
        mFragmentByDateBinding.toolbarFilterDetail.txtFilterDetailToolbar.setText(getResources().getString(R.string.text_filter_by_date));
        mByDates = new ArrayList<>();
        String[] by_date_array = getResources().getStringArray(R.array.by_date);
        for (int i = 0; i < by_date_array.length; i++ ) {
            mByDates.add(new ByDate(((i + 1) + ""), by_date_array[i]));
        }
        mByDateAdapter = new ByDateAdapter(mActivity, mByDates, mIOnByDate, tag);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mFragmentByDateBinding.recyclerByDate.setLayoutManager(mLayoutManager);
        mFragmentByDateBinding.recyclerByDate.setItemAnimator(new DefaultItemAnimator());
        mFragmentByDateBinding.recyclerByDate.setAdapter(mByDateAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_filter_detail_toolbar:
                mActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onChoose(String name) {
        Event event = new Event(Constant.BY_DATE_TYPE, name);
        RxBus.getInstance().post(event);
        mActivity.onBackPressed();

    }
}
