package com.ozo.ozo.fragment;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ozo.ozo.R;
import com.ozo.ozo.activity.DetailActivity;
import com.ozo.ozo.activity.SettingActivity;
import com.ozo.ozo.adapter.ImagesNewsDetailAdapter;
import com.ozo.ozo.adapter.SimilarAdapter;
import com.ozo.ozo.customview.ContactListener;
import com.ozo.ozo.customview.DialogConfirmNewsListerner;
import com.ozo.ozo.databinding.FragmentNewsDetailBinding;
import com.ozo.ozo.fragment.dialog.DialogNewsDetail;
import com.ozo.ozo.model.DialogNewsReport;
import com.ozo.ozo.model.Image;
import com.ozo.ozo.model.NewsFeed;
import com.ozo.ozo.model.User;
import com.ozo.ozo.model.callbacks.OnSimilarListener;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 5/28/17.
 */

public class FragmentNewsDetail extends BaseFragment implements OnSimilarListener, View.OnClickListener, ContactListener, DialogConfirmNewsListerner{

    private static final String TAG = "FragmentNewsDetail";
    BaseActivity mActivity;
    FragmentNewsDetailBinding mFragmentNewsDetailBinding;
    List<Image> mImages;
    List<NewsFeed> mSimilars;
    ImagesNewsDetailAdapter mAdapter;
    SimilarAdapter mSimilarAdapter;
    BaseApplication mApplication;
    OzoServices mServices;
    Subscription mSubscription;
    User mUser;
    NewsFeed mNewsFeed;
    int newsID = 0;
    int position = 0;
    private OnSimilarListener mOnSimilarListener = this;
    private ContactListener contactListener = this;
    private DialogConfirmNewsListerner dialogConfirmNewsListerner = this;
    private ProgressDialog progressDialog;
    private boolean iNewsSaved = false;
    private boolean iNewsHided = false;
    private boolean isReason = false;

    public static FragmentNewsDetail newInstance(int newsId, int position, boolean isReason) {
        Bundle args = new Bundle();
        args.putInt("news_id", newsId);
        args.putInt("position", position);
        args.putBoolean("is_reason", isReason);
        FragmentNewsDetail fragment = new FragmentNewsDetail();
        fragment.setArguments(args);
        return fragment;
    }

    public static FragmentNewsDetail newInstance(int newsId, int position, int isNewsStatus, boolean isReason) {
        Bundle args = new Bundle();
        args.putInt("news_id", newsId);
        args.putInt("position", position);
        args.putInt("news_status", isNewsStatus);
        args.putBoolean("is_reason", isReason);
        FragmentNewsDetail fragment = new FragmentNewsDetail();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            newsID = getArguments().getInt("news_id");
            position = getArguments().getInt("position");
            if (getArguments().containsKey("news_status")) {
                if (getArguments().getInt("news_status") == 1) {
                    iNewsSaved = true;
                }
                if (getArguments().getInt("news_status") == 3) {
                    iNewsHided = true;
                }
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mFragmentNewsDetailBinding == null) {
            mFragmentNewsDetailBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_news_detail, container, false);
            initView();
        }
        return mFragmentNewsDetailBinding.getRoot();
    }

    private void initView() {
        mFragmentNewsDetailBinding.toolbarNewsDetail.imgBackNewsDetailToolbar.setOnClickListener(this);
        mFragmentNewsDetailBinding.toolbarNewsDetail.imgRightNewsDetailToolbar.setOnClickListener(this);
        mFragmentNewsDetailBinding.txtNewsSaveCustomerDetail.setOnClickListener(this);
        mFragmentNewsDetailBinding.txtContactCustomerDetail.setOnClickListener(this);
        mFragmentNewsDetailBinding.txtNewsHideCustomerDetail.setOnClickListener(this);
        mFragmentNewsDetailBinding.txtNewsRejectStaffDetail.setOnClickListener(this);
        mFragmentNewsDetailBinding.txtContactStaftDetail.setOnClickListener(this);
        mFragmentNewsDetailBinding.txtForUserStaftDetail.setOnClickListener(this);

        setColorIcon();

        mUser = mActivity.getUserInfo();
        mNewsFeed = new NewsFeed();
        mApplication = BaseApplication.getInstance(mActivity);
        mServices = mApplication.getApiServices();

        //list images
        mImages = new ArrayList<>();
        mAdapter = new ImagesNewsDetailAdapter(mActivity, mImages);
        mFragmentNewsDetailBinding.recyclerDetail.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, true));
        mFragmentNewsDetailBinding.recyclerDetail.setItemAnimator(new DefaultItemAnimator());
        mFragmentNewsDetailBinding.recyclerDetail.setHasFixedSize(true);
        mFragmentNewsDetailBinding.recyclerDetail.setAdapter(mAdapter);

        //similar
        mSimilars = new ArrayList<>();
        mSimilarAdapter = new SimilarAdapter(mSimilars, mActivity, mOnSimilarListener);
        mFragmentNewsDetailBinding.recyclerSimilarDetail.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, true));
        mFragmentNewsDetailBinding.recyclerSimilarDetail.setItemAnimator(new DefaultItemAnimator());
        mFragmentNewsDetailBinding.recyclerSimilarDetail.setHasFixedSize(true);
        mFragmentNewsDetailBinding.recyclerSimilarDetail.setAdapter(mSimilarAdapter);
        if (mUser != null) {
            //true is customer
            if (mUser.getIsUser()) {
                mFragmentNewsDetailBinding.linearDetaillCustomer.setVisibility(View.VISIBLE);
                mFragmentNewsDetailBinding.linearDetaillStaff.setVisibility(View.GONE);
                if (iNewsSaved) {
                    mFragmentNewsDetailBinding.txtNewsSaveCustomerDetail.setText(getResources().getString(R.string.text_remove_news_save));
                } else {
                    mFragmentNewsDetailBinding.txtNewsSaveCustomerDetail.setText(getResources().getString(R.string.text_news_save));
                }

                if (iNewsHided) {
                    mFragmentNewsDetailBinding.txtNewsHideCustomerDetail.setText(getResources().getString(R.string.text_remove_news_hide));
                } else {
                    mFragmentNewsDetailBinding.txtNewsHideCustomerDetail.setText(getResources().getString(R.string.text_news_hide));
                }

            } else {
                mFragmentNewsDetailBinding.linearDetaillCustomer.setVisibility(View.GONE);
                mFragmentNewsDetailBinding.linearDetaillStaff.setVisibility(View.VISIBLE);
            }
            getNewsDetail(newsID, mUser.getId());
        }



    }

    private void setColorIcon() {
//        mFragmentNewsDetailBinding.imgNewsDetailForUse.setColorFilter(mActivity.getResources().getColor(R.color.colorRed), PorterDuff.Mode.MULTIPLY);
//        mFragmentNewsDetailBinding.imgNewsDetailHide.setColorFilter(mActivity.getResources().getColor(R.color.colorRed), PorterDuff.Mode.MULTIPLY);
//        mFragmentNewsDetailBinding.imgNewsDetailSave.setColorFilter(mActivity.getResources().getColor(R.color.colorRed), PorterDuff.Mode.MULTIPLY);
    }

    private void getNewsDetail(int newsId, int userId) {
        String sign = "id=" + newsId + "&userid=" + userId;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.getNewsDetail(newsId, userId, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            if (!jsonObject.getAsJsonObject("data").isJsonNull()) {
                                Type collectionType = new TypeToken<NewsFeed>(){}.getType();
                                mNewsFeed = new Gson().fromJson(jsonObject.getAsJsonObject("data"), collectionType);
                                if (mNewsFeed != null) {
                                    applyData(mNewsFeed);
                                }
                            }
                        } else if (mActivity.getIsLogin()) {
                            mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    RealmResults<User> result = realm.where(User.class).findAll();
                                    result.clear();
                                    mActivity.saveIsLogin(false);
                                    mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                }
                            });
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    private void applyData(NewsFeed mNewsFeed) {
        String date = mNewsFeed.getCreateOn().replace("/Date(", "");
        date = date.replace(")/", "");
        Timestamp timestamp = new Timestamp(Long.parseLong(date));
        Date mCreateDate = new Date(timestamp.getTime());
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        date = df.format(mCreateDate);
        isReason = mNewsFeed.isReason();
        try {
            //check is list image not yet ?
            if (mNewsFeed.getListImage() == null || mNewsFeed.getListImage().size() < 1) {
                mFragmentNewsDetailBinding.linearImages.setVisibility(View.GONE);
            } else {
                mFragmentNewsDetailBinding.linearImages.setVisibility(View.VISIBLE);
                mImages = mNewsFeed.getListImage();
                mAdapter = new ImagesNewsDetailAdapter(mActivity, mImages);
                mFragmentNewsDetailBinding.recyclerDetail.setAdapter(mAdapter);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            mFragmentNewsDetailBinding.linearImages.setVisibility(View.GONE);
        }

        try {
            //check is list samenews not yet ?
            if (mNewsFeed.getSameNews().size() < 1) {
                mFragmentNewsDetailBinding.linearSimilar.setVisibility(View.GONE);
            } else {
                mFragmentNewsDetailBinding.linearSimilar.setVisibility(View.VISIBLE);
                mSimilars = mNewsFeed.getSameNews();
                mSimilarAdapter = new SimilarAdapter(mSimilars, mActivity, mOnSimilarListener);
                mFragmentNewsDetailBinding.recyclerSimilarDetail.setAdapter(mSimilarAdapter);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            mFragmentNewsDetailBinding.linearSimilar.setVisibility(View.GONE);
        }

        mFragmentNewsDetailBinding.txtHeaderNewsDetail.setText(mNewsFeed.getTitle().trim());
        mFragmentNewsDetailBinding.txtContentNewsDetail.setText(mNewsFeed.getContents().trim());
        mFragmentNewsDetailBinding.txtPriceNewsDetail.setText(mNewsFeed.getPriceText());
        mFragmentNewsDetailBinding.txtContactNewsDetail.setText(mNewsFeed.getPhone());
        mFragmentNewsDetailBinding.txtPostDateNewsDetail.setText(date);
        mFragmentNewsDetailBinding.txtNewsTypeNewsDetail.setText(mNewsFeed.getStatusName());
        mFragmentNewsDetailBinding.txtCategoryNewsDetail.setText(mNewsFeed.getCateName());
        mFragmentNewsDetailBinding.txtLocationNewsDetail.setText(mNewsFeed.getDistrictName());
        mFragmentNewsDetailBinding.txtSiteNewsDetail.setText(mNewsFeed.getSiteName());
        mFragmentNewsDetailBinding.txtVertifierNewsDetail.setText(mNewsFeed.getPersonCheck());
        mFragmentNewsDetailBinding.txtReportNewsDetail.setText(mNewsFeed.getPersionalReport());
        mFragmentNewsDetailBinding.progressNewsDetail.setVisibility(View.GONE);
        mFragmentNewsDetailBinding.scrollNewsDetail.setVisibility(View.VISIBLE);

        if (mNewsFeed.isCC()) {
            mFragmentNewsDetailBinding.txtForUserStaftDetail.setText(getResources().getString(R.string.text_remove_for_use));
        } else {
            mFragmentNewsDetailBinding.txtForUserStaftDetail.setText(getResources().getString(R.string.text_for_user));
        }

//        if (mNewsFeed.IsReason)
    }

    private void saveNews(int []idNews, int idUser) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.userSaveNews(idNews, idUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, new Gson().toJson(jsonObject));
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mActivity.hideLoading(progressDialog);
                            sendResult(Constant.NEWS_SAVE, position);
                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    private void removeSaveNews(int []idNews, int idUser) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.removeNewsSave(idNews, idUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, new Gson().toJson(jsonObject));
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            //
                            mActivity.hideLoading(progressDialog);
                            sendResult(Constant.REMOVE_NEWS_SAVE, position);
                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(progressDialog);
                    }
                });
    }

    private void removeHideNews(int []idNews, int idUser) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        Log.e(TAG, sign);
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        Log.e(TAG, sign);
        mSubscription = mServices.removeNewsHide(idNews, idUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, new Gson().toJson(jsonObject));
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mActivity.hideLoading(progressDialog);
                            sendResult(Constant.REMOVE_NEWS_HIDE, position);
                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    private void hideNews(int []idNews, int idUser) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        Log.e("hideNews", sign);
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        Log.e(TAG, sign);
        mSubscription = mServices.userHideNews(idNews, idUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, new Gson().toJson(jsonObject));
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mActivity.hideLoading(progressDialog);
                            sendResult(Constant.NEWS_HIDE, position);
                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(progressDialog);
                    }
                });
    }

    private void removeForUserNews(int []idNews, int idUser) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.removeNewsForUser(idNews, idUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mNewsFeed.setCC(false);
                            mActivity.hideLoading(progressDialog);
                            sendResult(Constant.FOR_USER_EDIT, position);
                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(progressDialog);
                    }
                });
    }

    private void deleteNews(int []idNews, int idUser) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.newsDelete(idNews, idUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, new Gson().toJson(jsonObject));
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mActivity.hideLoading(progressDialog);
                            sendResult(Constant.DELETE, position);
                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(progressDialog);
                    }
                });
    }

    private void rejectNews(int []idNews, int idUser) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.newsSpam(idNews, idUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, new Gson().toJson(jsonObject));
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mActivity.hideLoading(progressDialog);
                            sendResult(Constant.NEWS_REJECT, position);
                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(progressDialog);
                    }
                });
    }

    private void forUserNews(int []idNews, int idUser) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.newsForUser(idNews, idUser, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mNewsFeed.setCC(true);
                            mActivity.hideLoading(progressDialog);
                            sendResult(Constant.NEWS_FOR_USER, position);

                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(progressDialog);
                    }
                });
    }

    private void cancleReport( int idUser, int newsID) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "userid=" + idUser + "&newsid=" + newsID;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.cancleNewsWarning(idUser, newsID,sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mActivity.hideLoading(progressDialog);
                            isReason = !isReason;
                            RxBus.getInstance().post(new DialogNewsReport(position, new Boolean(isReason)));
                            initView();

                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            } else {
                                Toast.makeText(mActivity, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(progressDialog);
                    }
                });
    }

    private void newsWarning(int idNews, int idUser, String reason) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "userid=" + idUser + "&newsid=" + idNews + "&reason=" + reason;

        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.newsWarning(idUser, idNews, reason, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mActivity.hideLoading(progressDialog);
                            isReason = !isReason;
                            RxBus.getInstance().post(new DialogNewsReport(position, new Boolean(isReason)));
                            initView();

                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(progressDialog);
                    }
                });
    }


    private void agencyNews(int []idNews, int idUser) {
        if (!mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        String sign = "listnewsid=" + new Gson().toJson(idNews) + "&userid=" + idUser;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.agencyNews(idNews, idUser,sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mActivity.hideLoading(progressDialog);
                            sendResult(Constant.AGENCY_REPORT, position);

                        } else {
                            mActivity.hideLoading(progressDialog);
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(progressDialog);
                    }
                });
    }


    private void call(String phone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mActivity.askPermission(Manifest.permission.CALL_PHONE, Constant.REQUEST_CALL)) {
                mActivity.call(mActivity, phone);
            }
        } else {
            mActivity.call(mActivity, phone);
        }
    }

    private void message(String phone) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (mActivity.askPermission(Manifest.permission.SEND_SMS, Constant.REQUEST_SMS)) {
                mActivity.sendMessage(phone);
            }
        } else {
            mActivity.sendMessage(phone);
        }
    }

    private void sendResult(int key, int position) {
        if (mActivity instanceof DetailActivity) {
            ((DetailActivity) mActivity).sendResult(key, position);
        }

        if (mActivity instanceof SettingActivity) {
            ((SettingActivity) mActivity).reloadPage(key, position);
            mActivity.onBackPressed();
        }
    }

    public void dialogReport(Context mContext) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_news_report);
        dialog.setCanceledOnTouchOutside(false);
        TextView btnOk = (TextView) dialog.findViewById(R.id.txt_dialog_report_report);
        TextView btnCancle = (TextView) dialog.findViewById(R.id.txt_dialog__report_cancle);
        final EditText edtContent = (EditText) dialog.findViewById(R.id.edt_report_content);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (mUser != null) {
                    progressDialog = mActivity.showLoading(mActivity);
                    int idNews  = mNewsFeed.getId();
                    newsWarning(idNews, mActivity.getUserInfo().getId(), edtContent.getText().toString().trim());
                }
            }
        });

        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_news_detail_toolbar:
                mActivity.onBackPressed();
                break;
            case R.id.img_right_news_detail_toolbar:
                DialogNewsDetail dialogNewsDetail = new DialogNewsDetail();
                Bundle bundle = new Bundle();
                bundle.putBoolean("isCC", mNewsFeed.isCC());
                bundle.putBoolean("isStaff", !mUser.getIsUser());
                bundle.putBoolean("is_reason", isReason);
                dialogNewsDetail.setArguments(bundle);
                dialogNewsDetail.setCancelable(true);
                dialogNewsDetail.setTargetFragment(this, DialogNewsDetail.REQUEST_CODE);
                dialogNewsDetail.show(mActivity.getSupportFragmentManager(), TAG);
                break;
            case R.id.txt_news_reject_staff_detail:
                mActivity.dialogConfirmNews(mActivity, getResources().getString(R.string.dialog_confirm_news_reject),
                        dialogConfirmNewsListerner, Constant.NEWS_REJECT, 0);
                break;
            case R.id.txt_contact_staft_detail:
                mActivity.contact(mActivity, this);
                break;
            case R.id.txt_for_user_staft_detail:
                if (mUser != null) {
                    progressDialog = mActivity.showLoading(mActivity);
                    int idNews [] = {mNewsFeed.getId()};
                    if (mNewsFeed.isCC()) {
                        removeForUserNews(idNews, mActivity.getUserInfo().getId());
                    } else {
                        forUserNews(idNews, mActivity.getUserInfo().getId());
                    }
                }
                break;
            case R.id.txt_news_save_customer_detail:
                if (mUser != null) {
                    progressDialog = mActivity.showLoading(mActivity);
                    int idNews [] = {mNewsFeed.getId()};
                    if (iNewsSaved) {
                        removeSaveNews(idNews, mActivity.getUserInfo().getId());
                    } else {
                        saveNews(idNews, mActivity.getUserInfo().getId());
                    }
                }
                break;
            case R.id.txt_news_hide_customer_detail:
                if (mUser != null) {
                    progressDialog = mActivity.showLoading(mActivity);
                    int idNews [] = {mNewsFeed.getId()};
                    if (iNewsHided) {
                        removeHideNews(idNews, mActivity.getUserInfo().getId());
                    } else {
                        hideNews(idNews, mActivity.getUserInfo().getId());
                    }
                }
                break;
            case R.id.txt_contact_customer_detail:
                mActivity.contact(mActivity, this);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(mActivity, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case Constant.REQUEST_CALL:
                    mActivity.call(mActivity, mNewsFeed.getPhone());
                    break;
                case Constant.REQUEST_SMS:
                    mActivity.sendMessage(mNewsFeed.getPhone());
                    break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DialogNewsDetail.REQUEST_CODE) {
            int value = data.getIntExtra(
                    DialogNewsDetail.KEY, 0);
            switch (value) {
                case Constant.AGENCY_REPORT:
                    if (mUser != null) {
                        progressDialog = mActivity.showLoading(mActivity);
                        int idNews [] = {mNewsFeed.getId()};
                        agencyNews(idNews, mActivity.getUserInfo().getId());
                    }
                    break;
                case Constant.NEWS_FOR_USER:
                    if (mUser != null) {
                        progressDialog = mActivity.showLoading(mActivity);
                        int idNews [] = {mNewsFeed.getId()};
                        forUserNews(idNews, mActivity.getUserInfo().getId());
                    }
                    break;
                case Constant.FOR_USER_EDIT:
                    if (mUser != null) {
                        progressDialog = mActivity.showLoading(mActivity);
                        int idNews [] = {mNewsFeed.getId()};
                        removeForUserNews(idNews, mActivity.getUserInfo().getId());
                    }
                    break;
                case Constant.REPORT:
                    dialogReport(mActivity);
                    break;
                case Constant.CANCEL_REPORT:
                    if (mUser != null) {
                        progressDialog = mActivity.showLoading(mActivity);
                        int idNews = mNewsFeed.getId();
                        cancleReport(mActivity.getUserInfo().getId(), idNews);
                    }
                    break;
                case Constant.DELETE:
                    mActivity.dialogConfirmNews(mActivity, getResources().getString(R.string.dialog_confirm_news_delete),
                            dialogConfirmNewsListerner, Constant.DELETE, 0);
                    break;
                case Constant.CANCEL:
                    break;
            }
        }
    }

    @Override
    public void contact(String action) {
        switch (action) {
            case "call":
                call(mNewsFeed.getPhone());
                break;
            case "message":
                message(mNewsFeed.getPhone());
                break;
        }
    }

    @Override
    public void onCLickSimilar(int newsId) {
        mFragmentNewsDetailBinding.scrollNewsDetail.setVisibility(View.GONE);
        mFragmentNewsDetailBinding.progressNewsDetail.setVisibility(View.VISIBLE);
        this.newsID = newsId;
        initView();
    }

    @Override
    public void dialogOk(int type, int position) {
        switch (type) {
            case Constant.DELETE:
                if (mUser != null) {
                    progressDialog = mActivity.showLoading(mActivity);
                    int idNews [] = {mNewsFeed.getId()};
                    deleteNews(idNews, mActivity.getUserInfo().getId());
                }
                break;
            case Constant.NEWS_REJECT:
                if (mUser != null) {
                    progressDialog = mActivity.showLoading(mActivity);
                    int idNews [] = {mNewsFeed.getId()};
                    rejectNews(idNews, mActivity.getUserInfo().getId());
                }
                break;
        }
    }

    @Override
    public void dialogCancle(int type, int position) {

    }
}
