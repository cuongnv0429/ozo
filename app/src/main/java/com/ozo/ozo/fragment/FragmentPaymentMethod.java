package com.ozo.ozo.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ozo.ozo.R;
import com.ozo.ozo.adapter.PaymentMethodAdapter;
import com.ozo.ozo.databinding.FragmentPaymentMethodBinding;
import com.ozo.ozo.model.Event;
import com.ozo.ozo.model.Payment;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 7/15/17.
 */

public class FragmentPaymentMethod extends BaseFragment implements View.OnClickListener, PaymentMethodAdapter.OnClickPaymentMethod{

    private String TAG = "FragmentPaymentMethod";
    BaseActivity mActivity;
    FragmentPaymentMethodBinding fragmentPaymentMethodBinding;
    OzoServices mServices;
    Subscription mSubcription;
    private List<Payment> paymentList;
    private PaymentMethodAdapter paymentMethodAdapter;
    private String tag = "";
    private PaymentMethodAdapter.OnClickPaymentMethod onClickPaymentMethod = this;

    public static FragmentPaymentMethod newInstance(String tag) {

        Bundle args = new Bundle();
        args.putString("tag", tag);
        FragmentPaymentMethod fragment = new FragmentPaymentMethod();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tag = getArguments().getString("tag");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (fragmentPaymentMethodBinding == null) {
            fragmentPaymentMethodBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment_method, container, false);
            initView();
        }
        return fragmentPaymentMethodBinding.getRoot();
    }

    private void initView() {
        fragmentPaymentMethodBinding.toolbarPaymenMethod.txtFilterDetailToolbar.setText(getResources().getString(R.string.text_payment_method_choose));
        fragmentPaymentMethodBinding.toolbarPaymenMethod.imgBackFilterDetailToolbar.setOnClickListener(this);

        mServices = BaseApplication.getInstance(mActivity).getApiServices();

        paymentList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity);
        fragmentPaymentMethodBinding.recyclerPaymentMethod.setLayoutManager(linearLayoutManager);
        fragmentPaymentMethodBinding.recyclerPaymentMethod.setHasFixedSize(true);
        paymentMethodAdapter = new PaymentMethodAdapter(mActivity, paymentList, tag, onClickPaymentMethod);
        fragmentPaymentMethodBinding.recyclerPaymentMethod.setAdapter(paymentMethodAdapter);
        if (mActivity.getUserInfo() != null) {
            fragmentPaymentMethodBinding.progressPaymentMethod.setVisibility(View.VISIBLE);
            fragmentPaymentMethodBinding.recyclerPaymentMethod.setVisibility(View.GONE);
            getPaymentMethod(mActivity.getUserInfo().getId());
        }
    }

    private void getPaymentMethod(int userId) {
        String sign = "";
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign);

        mSubcription = mServices.getPaymentMethod(sign, userId, mActivity.getInfoLogin())
                .observeOn(BaseApplication.getInstance(mActivity).subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject jsonObject) {
                        fragmentPaymentMethodBinding.progressPaymentMethod.setVisibility(View.GONE);
                        fragmentPaymentMethodBinding.recyclerPaymentMethod.setVisibility(View.VISIBLE);
                        Type collectionType = new TypeToken<List<Payment>>(){}.getType();
                        paymentList = new Gson().fromJson(jsonObject.getAsJsonArray("data"), collectionType);
                        paymentMethodAdapter = new PaymentMethodAdapter(mActivity, paymentList, tag, onClickPaymentMethod);
                        fragmentPaymentMethodBinding.recyclerPaymentMethod.setAdapter(paymentMethodAdapter);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back_filter_detail_toolbar:
                mActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onClickPaymentMethod(String name) {
        Event event = new Event(Constant.PAYMENT_METHOD, name);
        RxBus.getInstance().post(event);
        mActivity.onBackPressed();
    }
}
