package com.ozo.ozo.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ozo.ozo.R;
import com.ozo.ozo.activity.FilterActivity;
import com.ozo.ozo.activity.SettingActivity;
import com.ozo.ozo.adapter.NewsHidedAdapter;
import com.ozo.ozo.customview.RecyclerTouchListener;
import com.ozo.ozo.databinding.FragmentNewsHidedBinding;
import com.ozo.ozo.fragment.dialog.DialogNewsFeed;
import com.ozo.ozo.model.BaseNewsFeed;
import com.ozo.ozo.model.ByDate;
import com.ozo.ozo.model.ByPrice;
import com.ozo.ozo.model.Category;
import com.ozo.ozo.model.DialogNewsReport;
import com.ozo.ozo.model.District;
import com.ozo.ozo.model.NewsFeed;
import com.ozo.ozo.model.NewsSite;
import com.ozo.ozo.model.NewsStatus;
import com.ozo.ozo.model.User;
import com.ozo.ozo.model.callbacks.OnLoadMoreListener;
import com.ozo.ozo.model.request.NewsInHomRequest;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;
import com.ozo.ozo.utils.ShareHelper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 7/2/17.
 */

public class FragmentNewsHided extends BaseFragment implements TextWatcher, View.OnClickListener{
    private static final String TAG = "FragmentNewsHided";
    private static final int PAGE_LIMIT = 10;
    BaseActivity mActivity;
    FragmentNewsHidedBinding mNewsHidedBinding;
    OzoServices mServices;
    Subscription mSubcription;
    private BaseNewsFeed mBaseNewsFeed;
    private List<NewsFeed> mNewsFeeds;
    private NewsHidedAdapter mAdapter;
    private ProgressDialog progressDialog;
    private boolean isSearch = false;
    private int pageIndex = 1;
    private CountDownTimer mCountDownTimerSearchChange;
    private double minPrice = -1.0;
    private double maxPrice = -1.0;
    User mUser;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mNewsHidedBinding == null) {
            mNewsHidedBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_news_hided, container, false);
            initView();
            //listener event Rxbus from FragmentDetail
            RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
                @Override
                public void call(Object o) {
                    if (o instanceof DialogNewsReport) {
                        try {
                            int position = ((DialogNewsReport) o).getPosition();
                            boolean isReason = (boolean) ((DialogNewsReport) o).getContent();
                            Log.e(TAG, position + "__" + isReason);
                            if (position >= 0) {
                                mNewsFeeds.get(position).setReason(isReason);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }


                    }
                }
            });
        }
        return mNewsHidedBinding.getRoot();
    }

    private void initView() {
        mServices = BaseApplication.getInstance(mActivity).getApiServices();

        mNewsHidedBinding.toolbarNewsHided.edtSearch.addTextChangedListener(this);
        mNewsHidedBinding.toolbarNewsHided.txtFilter.setOnClickListener(this);
        mNewsHidedBinding.toolbarNewsHided.imgExitSearch.setOnClickListener(this);
        mNewsHidedBinding.toolbarNewsHided.edtSearch.setHint(getResources().getString(R.string.hint_search_news_hiedd));

        mNewsFeeds = new ArrayList<>();
        mBaseNewsFeed = new BaseNewsFeed();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mNewsHidedBinding.recycler.setLayoutManager(mLayoutManager);
        mNewsHidedBinding.recycler.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new NewsHidedAdapter(mActivity, mNewsFeeds, mNewsHidedBinding.recycler);
        mNewsHidedBinding.recycler.setAdapter(mAdapter);

        mNewsHidedBinding.swiperefreshNewsHided.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mUser != null && mUser.isValid()) {
                    if (mUser.getIsPayment()) {
                        mNewsHidedBinding.txtIsPayment.setVisibility(View.GONE);
                        pageIndex = 1;
                        mAdapter.setLoaded();
                        progressDialog = mActivity.showLoading(mActivity);
                        getListNewsHided(pageIndex, 3);
                    } else {
                        mNewsHidedBinding.txtIsPayment.setVisibility(View.VISIBLE);
                        mNewsHidedBinding.txtEmptyInfor.setVisibility(View.GONE);
                        mNewsHidedBinding.progressBar.setVisibility(View.GONE);
                        mNewsHidedBinding.swiperefreshNewsHided.setRefreshing(false);
                    }
                }

            }
        });

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mNewsFeeds.add(null);
                mAdapter.notifyItemInserted(mNewsFeeds.size() - 1);
                pageIndex += 1;
                getListNewsHided(pageIndex, 3);
            }
        });

        mNewsHidedBinding.recycler.addOnItemTouchListener(
                new RecyclerTouchListener(mActivity, mNewsHidedBinding.recycler, new RecyclerTouchListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (mActivity instanceof SettingActivity) {
                            ((SettingActivity) mActivity).pushFragment(FragmentNewsDetail.newInstance(mNewsFeeds.get(position).getId(),
                                    position,  Constant.STATUS_HIDED, mNewsFeeds.get(position).isReason()));
                        }
//
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));
        mUser = mActivity.getUserInfo();
        if (mUser != null && mUser.isValid()) {
            if (mUser.getIsPayment()) {
                pageIndex = 1;
                getListNewsHided(pageIndex, 3);
                mNewsHidedBinding.txtIsPayment.setVisibility(View.GONE);
            } else {
                mNewsHidedBinding.txtIsPayment.setVisibility(View.VISIBLE);
                mNewsHidedBinding.txtEmptyInfor.setVisibility(View.GONE);
                mNewsHidedBinding.progressBar.setVisibility(View.GONE);
            }
        }

    }

    private void getListNewsHided(final int pageIndex, int newsStatus) {
        mNewsHidedBinding.recycler.setVisibility(View.VISIBLE);
        mNewsHidedBinding.txtEmptyInfor.setVisibility(View.GONE);
        if (mAdapter.getItemCount() < 1 || isSearch) {
            mNewsHidedBinding.progressBar.setVisibility(View.VISIBLE);
        } else {
            mNewsHidedBinding.progressBar.setVisibility(View.GONE);
        }
        NewsInHomRequest newsInHomRequest = new NewsInHomRequest();

        Category mCategory = new Gson().fromJson(ShareHelper.getCategory(mActivity, TAG), Category.class);
        District mDistrict = new Gson().fromJson(ShareHelper.getDistrict(mActivity, TAG), District.class);
        NewsStatus mNewsStatus = new Gson().fromJson(ShareHelper.getNewsStatus(mActivity, TAG), NewsStatus.class);
        final NewsSite mNewsSite = new Gson().fromJson(ShareHelper.getNewsSite(mActivity, TAG), NewsSite.class);
        String fromDate = ShareHelper.getFromDate(mActivity, TAG);
        String toDate = ShareHelper.getToDate(mActivity, TAG);
        ByDate byDate = new Gson().fromJson(ShareHelper.getByDate(mActivity, TAG), ByDate.class);
        ByPrice byPrice = new Gson().fromJson(ShareHelper.getByPrice(mActivity, TAG), ByPrice.class);
        defineMinMaxPrice(byPrice);

        newsInHomRequest.setUserId(mUser.getId());
        newsInHomRequest.setCateId(mCategory.getId());
        newsInHomRequest.setDistrictId(mDistrict.getId());
        newsInHomRequest.setStatusId(mNewsStatus.getId());
        newsInHomRequest.setSiteId(mNewsSite.getId());
        newsInHomRequest.setBackDate(mActivity.defineByDate(byDate));
        newsInHomRequest.setFrom(fromDate);
        newsInHomRequest.setTo(toDate);
        newsInHomRequest.setMinPrice(minPrice);
        newsInHomRequest.setMaxPrice(maxPrice);
        newsInHomRequest.setPageIndex(pageIndex);
        newsInHomRequest.setPageSize(PAGE_LIMIT);
        newsInHomRequest.setRepeat(false);
        newsInHomRequest.setKey(mNewsHidedBinding.toolbarNewsHided.edtSearch.getText().toString().trim());
        newsInHomRequest.setNameOrder("");
        newsInHomRequest.setDescending(false);
        String sign = "userid=" + newsInHomRequest.getUserId() + "&cateid=" + newsInHomRequest.getCateId() + "&districid=" + newsInHomRequest.getDistrictId() +
                "&statusid=" + newsInHomRequest.getStatusId() + "&siteid=" + newsInHomRequest.getSiteId() + "&backdate=" +
                newsInHomRequest.getBackDate() + "&from=" + newsInHomRequest.getFrom() + "&to=" + newsInHomRequest.getTo() +
                "&minprice=" + (long)newsInHomRequest.getMinPrice() + "&maxprice=" + (long)newsInHomRequest.getMaxPrice() + "&pageindex=" +
                newsInHomRequest.getPageIndex() + "&pagesize=" + newsInHomRequest.getPageSize() + "&newsstatus=" + newsStatus + "&isrepeat=" + newsInHomRequest.isRepeat() +
                "&key=" + newsInHomRequest.getKey() + "&nameorder=" + newsInHomRequest.getNameOrder() + "&descending=" + newsInHomRequest.isDescending();
        newsInHomRequest.setSign(mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase()));

        mSubcription = mServices.getListNewStatus(newsInHomRequest.getUserId(), newsInHomRequest.getCateId(),
                newsInHomRequest.getDistrictId(), newsInHomRequest.getStatusId(), newsInHomRequest.getSiteId(),
                newsInHomRequest.getBackDate(), newsInHomRequest.getFrom(), newsInHomRequest.getTo(), newsInHomRequest.getMinPrice(),
                newsInHomRequest.getMaxPrice(), newsInHomRequest.getPageIndex(), newsInHomRequest.getPageSize(), newsStatus, newsInHomRequest.isRepeat(),
                newsInHomRequest.getKey(), newsInHomRequest.getNameOrder(), newsInHomRequest.isDescending(),
                newsInHomRequest.getSign(), mActivity.getInfoLogin())
                .observeOn(BaseApplication.getInstance(mActivity).subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        if (mNewsHidedBinding.swiperefreshNewsHided.isRefreshing()) {
                            mActivity.hideLoading(progressDialog);
                            mNewsHidedBinding.swiperefreshNewsHided.setRefreshing(false);
                            mNewsFeeds.clear();
                            mAdapter.notifyDataSetChanged();
                        }
                        mNewsHidedBinding.progressBar.setVisibility(View.GONE);
                        if (mAdapter.isLoading()) {
                            mNewsFeeds.remove(mNewsFeeds.size() - 1);
                            mAdapter.notifyItemRemoved(mNewsFeeds.size());
                        }
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            if (!jsonObject.getAsJsonObject("data").isJsonNull()) {
                                Type collectionType = new TypeToken<BaseNewsFeed>(){}.getType();
                                mBaseNewsFeed = new Gson().fromJson(jsonObject.getAsJsonObject("data"), collectionType);
                                if (mBaseNewsFeed.getNewsFeedList().size() > 0 || isSearch) {
                                    if (isSearch) {
                                        isSearch = false;
                                        if (mBaseNewsFeed.getNewsFeedList().size() < 1) {
                                            if (pageIndex == 1) {
                                                mNewsHidedBinding.txtEmptyInfor.setVisibility(View.VISIBLE);
                                            }
                                        }
                                    }
                                    int positin = mNewsFeeds.size();
                                    mNewsFeeds.addAll(mNewsFeeds.size(), mBaseNewsFeed.getNewsFeedList());
                                    mAdapter.notifyItemInserted(positin);
                                    mAdapter.setLoaded();
                                } else {
                                    if (mBaseNewsFeed.getNewsFeedList().size() < 1) {
                                        if (pageIndex == 1) {
                                            mNewsHidedBinding.txtEmptyInfor.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }

                            }
                        } else {
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }


                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    private void defineMinMaxPrice(ByPrice byPrice) {
        switch (byPrice.getId()) {
            case "1":
                minPrice = -1.0;
                maxPrice= -1.0;
                break;
            case "2":
                minPrice = 0.0;
                maxPrice= 1000000.0;
                break;
            case "3":
                minPrice = 1000000.0;
                maxPrice= 5000000.0;
                break;
            case "4":
                minPrice = Double.parseDouble(String.valueOf(5000000));
                maxPrice= Double.parseDouble(String.valueOf(10000000));
                break;
            case "5":
                minPrice = 10000000.0;
                maxPrice= 50000000.0;
                break;
            case "6":
                minPrice = 50000000.0;
                maxPrice= 100000000.0;
                break;
            case "7":
                minPrice = 100000000.0;
                maxPrice= 500000000.0;
                break;
            case "8":
                minPrice = 500000000.0;
                maxPrice= 1000000000.0;
                break;
            case "9":
                minPrice = 1000000000.0;
                maxPrice= 5000000000.0;
                break;
            case "10":
                minPrice = 5000000000.0;
                maxPrice= 10000000000.0;
                break;
            case "11":
                minPrice = 10000000000.0;
                maxPrice= -1.0;
                break;
            default:
                minPrice = -1.0;
                maxPrice= -1.0;
                break;
        }
    }

    private void search(final String keySearch) {
        if (mCountDownTimerSearchChange != null) {
            mCountDownTimerSearchChange.cancel();
        }
        mCountDownTimerSearchChange = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                isSearch = true;
                pageIndex = 1;
                mAdapter.setLoaded();
                mNewsFeeds.clear();
                mAdapter.notifyDataSetChanged();
                getListNewsHided(pageIndex, 3);
            }
        };
        mCountDownTimerSearchChange.start();
    }

    public void reloadPage( int key, int position) {
        switch (key) {
            case Constant.AGENCY_REPORT:
                mNewsFeeds.remove(position);
                mAdapter.notifyDataSetChanged();
                break;
            case Constant.NEWS_FOR_USER:
                mNewsFeeds.get(position).setCC(true);
                mAdapter.notifyItemChanged(position);
                break;
            case Constant.NEWS_REJECT:
                mNewsFeeds.remove(position);
                mAdapter.notifyDataSetChanged();
                break;
            case Constant.NEWS_SAVE:
            case Constant.REMOVE_NEWS_SAVE:
                mNewsFeeds.remove(position);
                mAdapter.notifyDataSetChanged();
                break;
            case Constant.NEWS_HIDE:
            case Constant.REMOVE_NEWS_HIDE:
                mNewsFeeds.remove(position);
                mAdapter.notifyDataSetChanged();
                break;
            case Constant.FOR_USER_EDIT:
                mNewsFeeds.get(position).setCC(false);
                mAdapter.notifyItemChanged(position);
                break;
            case Constant.DELETE:
                mNewsFeeds.remove(position);
                mAdapter.notifyDataSetChanged();
                break;
        }
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Log.e(TAG, count + "__" + before);
        if (!s.toString().equalsIgnoreCase("") || (count == 0 && before != 0)) {
            if (count != 0 && before != 0) {
                mNewsHidedBinding.toolbarNewsHided.imgExitSearch.setVisibility(View.VISIBLE);
            } else {
                mNewsHidedBinding.toolbarNewsHided.imgExitSearch.setVisibility(View.GONE);
            }
            if (mUser != null && mUser.isValid()) {
                if (mUser.getIsPayment()) {
                    mNewsHidedBinding.txtIsPayment.setVisibility(View.GONE);
                    search(s.toString().trim());
                } else {
                    mNewsHidedBinding.txtIsPayment.setVisibility(View.VISIBLE);
                    mNewsHidedBinding.txtEmptyInfor.setVisibility(View.GONE);
                    mNewsHidedBinding.progressBar.setVisibility(View.GONE);

                }
            }

        } else {
            mNewsHidedBinding.toolbarNewsHided.imgExitSearch.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_exit_search:
                mNewsHidedBinding.toolbarNewsHided.edtSearch.setText("");
                break;
            case R.id.txt_filter:
                Intent intent = new Intent(mActivity, FilterActivity.class);
                intent.putExtra("tag", TAG);
                startActivityForResult(intent, Constant.REQUEST_FILTER_NEWS_HIDED);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case Constant.REQUEST_FILTER_NEWS_HIDED:
                    initView();
                    break;
            }
        }
    }
}
