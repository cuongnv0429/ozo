package com.ozo.ozo.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ozo.ozo.R;
import com.ozo.ozo.adapter.ByMangerFilterAdapter;
import com.ozo.ozo.databinding.FragmentByManagerFilterBinding;
import com.ozo.ozo.model.Event;
import com.ozo.ozo.model.Staff;
import com.ozo.ozo.model.User;
import com.ozo.ozo.model.callbacks.OnLoadMoreListener;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by tug-on(_SM_) on 7/6/2017.
 */

public class FragmentByManagerFilter extends BaseFragment implements ByMangerFilterAdapter.IOnByManager, View.OnClickListener{
    private final String TAG = "FragmentByManagerFilter";
    private BaseActivity mActivity;
    FragmentByManagerFilterBinding byManagerFilterBinding;
    OzoServices mServices;
    Subscription mSubscription;
    User mUser;
    private List<Staff> staffList;
    private ByMangerFilterAdapter mAdapter;
    private String tag = "";
    private ByMangerFilterAdapter.IOnByManager iOnByManager = this;
    private ProgressDialog progressDialog;
    private int pageIndex = 1;

    public static FragmentByManagerFilter newInstance(String tag) {

        Bundle args = new Bundle();
        args.putString("tag", tag);
        FragmentByManagerFilter fragment = new FragmentByManagerFilter();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tag = getArguments().getString("tag");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (byManagerFilterBinding == null) {
            byManagerFilterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_by_manager_filter, container, false);
            initView();
        }
        return byManagerFilterBinding.getRoot();
    }

    private void initView() {
        byManagerFilterBinding.toolbarFilterDetail.imgBackFilterDetailToolbar.setOnClickListener(this);
        byManagerFilterBinding.toolbarFilterDetail.txtFilterDetailToolbar.setText(getResources().getString(R.string.text_by_manager));
        mServices = BaseApplication.getInstance(mActivity).getApiServices();
        mUser = mActivity.getUserInfo();

        staffList = new ArrayList<>();
        byManagerFilterBinding.recyclerByManager.setHasFixedSize(true);
        byManagerFilterBinding.recyclerByManager.setLayoutManager( new LinearLayoutManager(mActivity));
        byManagerFilterBinding.recyclerByManager.addItemDecoration(new DividerItemDecoration(mActivity,LinearLayoutManager.VERTICAL));
        mAdapter = new ByMangerFilterAdapter(mActivity, staffList, iOnByManager, tag, byManagerFilterBinding.recyclerByManager);
        byManagerFilterBinding.recyclerByManager.setAdapter(mAdapter);
        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
//                staffList.add(null);
//                mAdapter.notifyItemInserted(staffList.size() - 1);
//                pageIndex++;
//                getListByManager(mUser.getId(), pageIndex, mUser.getIsUser());
            }
        });
        byManagerFilterBinding.swiperefreshByManager.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageIndex = 1;
                mAdapter.setLoaded();
                progressDialog = mActivity.showLoading(mActivity);
                getListByManager(mUser.getId(), pageIndex, mUser.getIsUser());
            }
        });

        if (mUser != null) {
            byManagerFilterBinding.progressBar.setVisibility(View.VISIBLE);
            getListByManager(mUser.getId(), pageIndex, mUser.getIsUser());
        }
    }

    private void getListByManager(int userId, final int pageIndex, boolean isUser) {
        if (pageIndex == 1 && !byManagerFilterBinding.swiperefreshByManager.isRefreshing()) {
            byManagerFilterBinding.progressBar.setVisibility(View.VISIBLE);
        } else {
            byManagerFilterBinding.progressBar.setVisibility(View.GONE);
        }
        String sign = "userid=" + userId + "&page=" + pageIndex + "&isuser=" + isUser;
        Log.e(TAG, sign);
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        Log.e(TAG, sign);
        mSubscription = mServices.getManagerList(userId, pageIndex, isUser, sign)
                .observeOn(BaseApplication.getInstance(mActivity).subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject jsonObject) {
                        Log.e(TAG, jsonObject.toString());
                        mSubscription.unsubscribe();
                        if (byManagerFilterBinding.swiperefreshByManager.isRefreshing()) {
                            mActivity.hideLoading(progressDialog);
                            byManagerFilterBinding.swiperefreshByManager.setRefreshing(false);
                            staffList.clear();
                            mAdapter.notifyDataSetChanged();
                        }
                        byManagerFilterBinding.progressBar.setVisibility(View.GONE);
                        if (mAdapter.isLoading()) {
                            staffList.remove(staffList.size() - 1);
                            mAdapter.notifyItemRemoved(staffList.size() - 1);
                        }
                        if (!jsonObject.getAsJsonArray("data").isJsonNull()) {
                            Type typeCollection = new TypeToken<List<Staff>>(){}.getType();
                            List<Staff> data = new Gson().fromJson(jsonObject.getAsJsonArray("data"), typeCollection);
                            if (data.size() > 0) {
                                int position = staffList.size();
                                staffList.addAll(staffList.size(), data);
                                mAdapter.notifyItemInserted(position);
                                mAdapter.setLoaded();
                            } else {
                                if (staffList.size() < 1) {
                                    //mNotificaitonBinding.txtEmptyNotification.setVisibility(View.VISIBLE);
                                }
                            }

                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    @Override
    public void onChoose(String name) {
        Event event = new Event(Constant.BY_MANAGER, name);
        RxBus.getInstance().post(event);
        mActivity.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_filter_detail_toolbar:
                mActivity.onBackPressed();
                break;
        }
    }
}
