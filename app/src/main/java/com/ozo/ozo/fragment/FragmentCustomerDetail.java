package com.ozo.ozo.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ozo.ozo.R;
import com.ozo.ozo.activity.SettingActivity;
import com.ozo.ozo.databinding.FragmentDetailCustomerBinding;
import com.ozo.ozo.model.Customer;
import com.ozo.ozo.model.User;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;
import com.ozo.ozo.utils.ShareHelper;

import java.lang.reflect.Type;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 6/17/17.
 */

public class FragmentCustomerDetail extends BaseFragment implements View.OnClickListener{
    private final String TAG = "FragmentCustomerDetail";
    BaseActivity mActivity;
    FragmentDetailCustomerBinding mFragmentDetailCustomerBinding;
    BaseApplication mApplication;
    OzoServices mServices;
    Subscription mSubscription;
    int id;
    Customer mCustomer;

    public static FragmentCustomerDetail newInstance(int id) {
        Bundle args = new Bundle();
        args.putInt("id", id);
        FragmentCustomerDetail fragment = new FragmentCustomerDetail();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getInt("id", 0);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mFragmentDetailCustomerBinding == null) {
            mFragmentDetailCustomerBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail_customer, container, false);
            initView();
        }
        return mFragmentDetailCustomerBinding.getRoot();
    }

    private void initView() {

        mApplication = BaseApplication.getInstance(mActivity);
        mServices = mApplication.getApiServices();
        mCustomer = new Customer();
        mFragmentDetailCustomerBinding.scrollView.setVisibility(View.GONE);
        if (mActivity.getUserInfo() != null) {
            getCustomerDetail(id, mActivity.getUserInfo().getId());
        }


        mFragmentDetailCustomerBinding.toolbarCustomerDetail.txtFilterDetailToolbar.setText(getResources().getString(R.string.title_customer_detail));
        mFragmentDetailCustomerBinding.toolbarCustomerDetail.imgBackFilterDetailToolbar.setOnClickListener(this);
        mFragmentDetailCustomerBinding.linearExchangeHistory.setOnClickListener(this);
        mFragmentDetailCustomerBinding.linearRechargeCustomer.setOnClickListener(this);
    }

    private void getCustomerDetail(int id, int userId) {
        mFragmentDetailCustomerBinding.progressDetailCustomer.setVisibility(View.VISIBLE);
        String sign = "id=" + id;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.customerDetail(id, sign, userId, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        mSubscription.unsubscribe();
                        mFragmentDetailCustomerBinding.progressDetailCustomer.setVisibility(View.GONE);
                        mFragmentDetailCustomerBinding.scrollView.setVisibility(View.VISIBLE);
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            if (!jsonObject.getAsJsonObject("data").isJsonNull()) {
                                Type mCollections = new TypeToken<Customer>(){}.getType();
                                mCustomer = new Gson().fromJson(jsonObject.getAsJsonObject("data"), mCollections);
                                updateData(mCustomer);
                            }
                        } else {
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    private void updateData(Customer customer) {
        mFragmentDetailCustomerBinding.txtFullName.setText(customer.getFullName());
        mFragmentDetailCustomerBinding.txtPhoneNumber.setText(customer.getUserName());
        mFragmentDetailCustomerBinding.txtByManager.setText(customer.getManageBy());
        mFragmentDetailCustomerBinding.txtEndingLogin.setText(ShareHelper.parseDateTime(customer.getLastLogin()));
        mFragmentDetailCustomerBinding.txtCreateDate.setText(ShareHelper.parseDateTime(customer.getCreateDate()));
        if (customer.getTimeEnd() != null) {
            mFragmentDetailCustomerBinding.txtExpired.setText(ShareHelper.parseDateTime(customer.getTimeEnd()));
        } else {
            mFragmentDetailCustomerBinding.txtExpired.setText(customer.getTimeEndStr());
        }

        mFragmentDetailCustomerBinding.txtSurplus.setText(customer.getAmount());
        mFragmentDetailCustomerBinding.txtCash.setText(customer.getCashPayment());
        mFragmentDetailCustomerBinding.txtCashCk.setText(customer.getCardPayment());
        mFragmentDetailCustomerBinding.txtNote.setText(customer.getNotes());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_filter_detail_toolbar:
                mActivity.onBackPressed();
                break;
            case R.id.linear_recharge_customer:
                ((SettingActivity) mActivity).pushFragment(FragmentRechargeCustomer.newInstance(mCustomer, TAG));
                break;
            case R.id.linear_exchange_history:
                ((SettingActivity) mActivity).pushFragment(FragmentExchangeHistory.newInstance(mCustomer.getId()));
                break;
        }
    }
}
