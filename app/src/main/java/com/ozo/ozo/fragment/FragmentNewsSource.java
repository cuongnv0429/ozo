package com.ozo.ozo.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ozo.ozo.R;
import com.ozo.ozo.adapter.NewsSiteAdapter;
import com.ozo.ozo.databinding.FragmentNewsSourceBinding;
import com.ozo.ozo.model.Event;
import com.ozo.ozo.model.NewsSite;
import com.ozo.ozo.model.User;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 5/18/17.
 */

public class FragmentNewsSource extends BaseFragment implements View.OnClickListener, NewsSiteAdapter.IOnNewsSource{
    private static final String TAG = "FragmentNewsSource";
    BaseActivity mActivity;
    FragmentNewsSourceBinding mFragmentNewsSourceBinding;
    NewsSiteAdapter mNewsSiteAdapter;
    OzoServices mServices;
    List<NewsSite> mNewsSites;
    Subscription mSubscription;
    BaseApplication mApplication;
    NewsSiteAdapter.IOnNewsSource mIOnNewsSource = this;
    private String tag = "";

    public static FragmentNewsSource newInstance(String tag) {

        Bundle args = new Bundle();
        args.putString("tag", tag);
        FragmentNewsSource fragment = new FragmentNewsSource();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tag = getArguments().getString("tag");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mFragmentNewsSourceBinding == null) {
            mFragmentNewsSourceBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_news_source, container, false);
            initView();
        }
        return mFragmentNewsSourceBinding.getRoot();
    }

    private void initView() {
        mFragmentNewsSourceBinding.toolbarFilterDetail.imgBackFilterDetailToolbar.setOnClickListener(this);
        mFragmentNewsSourceBinding.toolbarFilterDetail.txtFilterDetailToolbar.setText(getResources().getString(R.string.text_filter_new_source));
        mApplication = BaseApplication.getInstance(mActivity);
        mServices = mApplication.getApiServices();
        mNewsSites = new ArrayList<>();
        mNewsSiteAdapter = new NewsSiteAdapter(mActivity, mNewsSites, mIOnNewsSource, tag);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mFragmentNewsSourceBinding.recyclerNewsSource.setLayoutManager(mLayoutManager);
        mFragmentNewsSourceBinding.recyclerNewsSource.setItemAnimator(new DefaultItemAnimator());
        mFragmentNewsSourceBinding.recyclerNewsSource.setAdapter(mNewsSiteAdapter);
        if (mActivity.getUserInfo() != null) {
            getListSite(mActivity.getUserInfo().getId());
        }

    }

    private void getListSite(int userId) {
        mFragmentNewsSourceBinding.progressNewsSource.setVisibility(View.VISIBLE);
        String sign = mActivity.hmacSha(Constant.KEY_SIGN, "");
        mSubscription = mServices.getlistSite(sign, userId, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, jsonObject.toString());
                        mSubscription.unsubscribe();
                        mFragmentNewsSourceBinding.progressNewsSource.setVisibility(View.GONE);
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            if (!jsonObject.get("data").isJsonNull()) {
                                Type collectionType = new TypeToken<List<NewsSite>>(){}.getType();
                                mNewsSites = (List<NewsSite>) new Gson().fromJson(jsonObject.getAsJsonArray("data"), collectionType);
                                mNewsSiteAdapter = new NewsSiteAdapter(mActivity, mNewsSites, mIOnNewsSource, tag);
                                mFragmentNewsSourceBinding.recyclerNewsSource.setAdapter(mNewsSiteAdapter);
                            } else {
                                mFragmentNewsSourceBinding.progressNewsSource.setVisibility(View.GONE);
                            }
                        } else if (mActivity.getIsLogin()) {
                            mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    RealmResults<User> result = realm.where(User.class).findAll();
                                    result.clear();
                                    mActivity.saveIsLogin(false);
                                    mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                }
                            });
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mFragmentNewsSourceBinding.progressNewsSource.setVisibility(View.GONE);
                        throwable.printStackTrace();
                    }
                });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_filter_detail_toolbar:
                mActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onChoose(String name) {
        Event event = new Event(Constant.NEWS_SOURCE_TYPE, name);
        RxBus.getInstance().post(event);
        mActivity.onBackPressed();

    }
}

