package com.ozo.ozo.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.ozo.ozo.R;
import com.ozo.ozo.activity.FilterActivity;
import com.ozo.ozo.activity.SettingActivity;
import com.ozo.ozo.databinding.FragmentCustomerFilterBinding;
import com.ozo.ozo.model.Account;
import com.ozo.ozo.model.Event;
import com.ozo.ozo.model.Staff;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;
import com.ozo.ozo.utils.ShareHelper;

import rx.functions.Action1;

/**
 * Created by ITV01 on 6/3/17.
 */

public class FragmentCustomerFilter extends BaseFragment implements View.OnClickListener{
    private static final String TAG = "FragmentCustomerFilter";
    private BaseActivity mActivity;
    private FragmentCustomerFilterBinding mFragmentCustomerFilterBinding;
    private String tag = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    public static FragmentCustomerFilter newInstance(String tag) {

        Bundle args = new Bundle();
        args.putString("tag", tag);
        FragmentCustomerFilter fragment = new FragmentCustomerFilter();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tag = getArguments().getString("tag");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mFragmentCustomerFilterBinding == null) {
            mFragmentCustomerFilterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_customer_filter, container, false);
            initView();
        }
        return mFragmentCustomerFilterBinding.getRoot();
    }

    private void initView() {
        mFragmentCustomerFilterBinding.toolbarCustomerFilter.txtTitleToolbar.setText(getResources().getString(R.string.text_customer_filter));
        mFragmentCustomerFilterBinding.toolbarCustomerFilter.txtToolbarPost.setText(getResources().getString(R.string.text_done));

        mFragmentCustomerFilterBinding.toolbarCustomerFilter.txtToolbarCancel.setOnClickListener(this);
        mFragmentCustomerFilterBinding.toolbarCustomerFilter.txtToolbarPost.setOnClickListener(this);
        mFragmentCustomerFilterBinding.linearFilterByManager.setOnClickListener(this);
        mFragmentCustomerFilterBinding.linearFilterAccountStatus.setOnClickListener(this);
        mFragmentCustomerFilterBinding.fabResetCustomerFilter.setOnClickListener(this);
        Staff staff = new Gson().fromJson(ShareHelper.getByManager(mActivity, tag), Staff.class);
        Account account = new Gson().fromJson(ShareHelper.getAccountType(mActivity, tag), Account.class);
        mFragmentCustomerFilterBinding.txtFilterAccountStatus.setText(account.getText());
        mFragmentCustomerFilterBinding.txtFilterManagerBy.setText(staff.getText());

        RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof Event) {
                    switch (((Event) o).getType()) {
                        case Constant.BY_MANAGER:
                            mFragmentCustomerFilterBinding.txtFilterManagerBy.setText(((Event) o).getContent().toString());
                            break;
                        case Constant.ACCOUNT_TYPE:
                            mFragmentCustomerFilterBinding.txtFilterAccountStatus.setText(((Event) o).getContent().toString());
                            break;
                    }
                }
            }
        });

    }

    private void sendResult() {
        Intent intent = mActivity.getIntent();
        mActivity.setResult(Activity.RESULT_OK, intent);
        mActivity.finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_toolbar_cancel:
                mActivity.onBackPressed();
                break;
            case R.id.txt_toolbar_post:
                sendResult();
            case R.id.linear_filter_by_manager:
                if (mActivity instanceof FilterActivity) {
                    ((FilterActivity) mActivity).pushFragment(FragmentByManagerFilter.newInstance(tag));
                }
                break;
            case R.id.linear_filter_account_status:
                if (mActivity instanceof FilterActivity) {
                    ((FilterActivity) mActivity).pushFragment(FragmentAccountTypeFilter.newInstance(tag));
                }
                break;
            case R.id.fab_reset_customer_filter:
                mActivity.resetFilter(mActivity, tag);
                initView();
                break;
        }
    }
}
