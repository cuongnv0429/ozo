package com.ozo.ozo.fragment.dialog;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.ozo.ozo.R;
import com.ozo.ozo.databinding.DialogNewsDetailBinding;
import com.ozo.ozo.utils.Constant;

/**
 * Created by ITV01 on 6/10/17.
 */

public class DialogNewsDetail extends DialogFragment implements View.OnClickListener{
    DialogNewsDetailBinding mDialogNewsDetailBinding;
    public static final int REQUEST_CODE = 1002;
    public static final String KEY = "CHOOSE_NEWS_DETAIL";
    private boolean isCC = false;
    private boolean isStaff = false;
    private boolean isReason = false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mDialogNewsDetailBinding == null) {
            mDialogNewsDetailBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_news_detail, container, false);
            initView();
        }
        Window window = getDialog().getWindow();
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        return mDialogNewsDetailBinding.getRoot();
    }

    private void initView() {
        if (getArguments() != null) {
            isCC = getArguments().getBoolean("isCC");
            isStaff = getArguments().getBoolean("isStaff");
            isReason = getArguments().getBoolean("is_reason");
        }

        if (isStaff) {
            mDialogNewsDetailBinding.linearStaff.setVisibility(View.VISIBLE);
            mDialogNewsDetailBinding.linearCustomer.setVisibility(View.GONE);
        } else {
            mDialogNewsDetailBinding.linearStaff.setVisibility(View.GONE);
            mDialogNewsDetailBinding.linearCustomer.setVisibility(View.VISIBLE);
            if (isCC) {
                mDialogNewsDetailBinding.txtForUserDialogDetail.setText(getResources().getString(R.string.text_remove_for_use));
            } else {
                mDialogNewsDetailBinding.txtForUserDialogDetail.setText(getResources().getString(R.string.text_for_user));
            }
        }

        if (isReason) {
            mDialogNewsDetailBinding.txtReportDialogDetail.setText(getResources().getString(R.string.text_cancle_report));
        } else {
            mDialogNewsDetailBinding.txtReportDialogDetail.setText(getResources().getString(R.string.text_report));
        }
        mDialogNewsDetailBinding.txtCancleDialogDetail.setOnClickListener(this);
        mDialogNewsDetailBinding.txtDeleteDialogDetail.setOnClickListener(this);
        mDialogNewsDetailBinding.txtAgencyDialogDetail.setOnClickListener(this);
        mDialogNewsDetailBinding.txtForUserDialogDetail.setOnClickListener(this);
        mDialogNewsDetailBinding.txtReportDialogDetail.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_delete_dialog_detail:
                sendResult(Constant.DELETE);
                break;
            case R.id.txt_for_user_dialog_detail:
                if (isCC) {
                    sendResult(Constant.FOR_USER_EDIT);
                } else {
                    sendResult(Constant.NEWS_FOR_USER);
                }
                break;
            case R.id.txt_agency_dialog_detail:
                sendResult(Constant.AGENCY_REPORT);
                break;
            case R.id.txt_report_dialog_detail:
                if (isReason) {
                    sendResult(Constant.CANCEL_REPORT);
                } else {
                    sendResult(Constant.REPORT);
                }
                break;
            case R.id.txt_cancle_dialog_detail:
                sendResult(Constant.CANCEL);
                break;
        }
    }

    private void sendResult(int value) {
        Intent intent = new Intent();
        intent.putExtra(KEY, value);
        getTargetFragment().onActivityResult(getTargetRequestCode(), REQUEST_CODE, intent);
        this.dismiss();
    }
}
