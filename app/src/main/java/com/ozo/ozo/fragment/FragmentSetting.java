package com.ozo.ozo.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonObject;
import com.ozo.ozo.R;
import com.ozo.ozo.activity.RegisterActivity;
import com.ozo.ozo.activity.SettingActivity;
import com.ozo.ozo.databinding.FragmentSettingBinding;
import com.ozo.ozo.model.User;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;
import com.ozo.ozo.utils.ShareHelper;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 4/10/17.
 */

public class FragmentSetting extends BaseFragment implements View.OnClickListener{
    private static final String TAG = "FragmentSetting";
    BaseActivity mActivity;
    FragmentSettingBinding mSettingBinding;
    OzoServices mServices;
    Subscription mSubscription;
    User mUser;
    ProgressDialog mProgressDialog;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mSettingBinding == null) {
            mSettingBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, container, false);
            initView();
        }
        return mSettingBinding.getRoot();
    }

    private void initView() {
        mSettingBinding.settingToolbar.imgBack.setVisibility(View.GONE);
        mSettingBinding.settingToolbar.txtTitleToolbar.setText(getResources().getString(R.string.text_title_setting));
        mSettingBinding.linearPostNews.setOnClickListener(this);
        mSettingBinding.linearNewsHided.setOnClickListener(this);
        mSettingBinding.linearNewsSaved.setOnClickListener(this);
        mSettingBinding.linearStaffManager.setOnClickListener(this);
        mSettingBinding.linearRechargeSetting.setOnClickListener(this);
        mSettingBinding.linearLogoutSetting.setOnClickListener(this);
        mSettingBinding.linearCardChooseSetting.setOnClickListener(this);
        mSettingBinding.linearHelp.setOnClickListener(this);

        mServices = BaseApplication.getInstance(mActivity).getApiServices();
        mUser = mActivity.getUserInfo();
        if (mUser != null) {
            mSettingBinding.txtUserName.setText( mUser.getFullName());
            mSettingBinding.txtUserPhone.setText(mUser.getPhone());
            mSettingBinding.txtCskh.setText(" " + mUser.getManagerName());
            mSettingBinding.txtSurplus.setText(" " + mUser.getAmount() + " vnđ");
            if (mUser.getDateEnd() != null && !mUser.getDateEnd().equalsIgnoreCase("")) {
                mSettingBinding.txtDateExpride.setText(" " + ShareHelper.parseDateTime(mUser.getDateEnd()));
            }

            if (mUser.getIsUser()) {// is customer
                mSettingBinding.linearCustomer.setVisibility(View.VISIBLE);
                mSettingBinding.linearStaff.setVisibility(View.GONE);
            } else {
                mSettingBinding.linearCustomer.setVisibility(View.GONE);
                mSettingBinding.linearStaff.setVisibility(View.VISIBLE);
            }
        }

    }

    private void pushSettingActivity(int id) {
        Intent mIntent = new Intent(mActivity, SettingActivity.class);
        mIntent.putExtra("id", id);
        startActivity(mIntent);
    }

    private void logout(int userId) {
        String sign = "userid=" + userId;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mServices.logout(userId, sign)
                .subscribeOn(Schedulers.io())
                .observeOn(BaseApplication.getInstance(mActivity).subscribeScheduler())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(JsonObject jsonObject) {
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    RealmResults<User> result = realm.where(User.class).findAll();
                                    result.clear();
                                    mActivity.saveIsLogin(false);
                                    mActivity.hideLoading(mProgressDialog);
                                    Intent intent = new Intent(mActivity, RegisterActivity.class);
                                    startActivity(intent);
                                    mActivity.finish();
                                }
                            });
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.linear_post_news:
                pushSettingActivity(Constant.POST_NEWS);
                break;
            case R.id.linear_news_hided:
                pushSettingActivity(Constant.NEWS_HIDE);
                break;
            case R.id.linear_news_saved:
                pushSettingActivity(Constant.NEWS_SAVED);
                break;
            case R.id.linear_staff_manager:
                pushSettingActivity(Constant.CUSTOMER_MANAGER);
                break;
            case R.id.linear_recharge_setting:
                pushSettingActivity(Constant.RECHARGE);
                break;
            case R.id.linear_card_choose_setting:
                pushSettingActivity(Constant.CARD_CHOOSE);
                break;
            case R.id.linear_logout_setting:
                if (mUser != null) {
                    mProgressDialog = mActivity.showLoading(mActivity);
                    logout(mUser.getId());
                }
                break;
            case R.id.linear_help:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mActivity.askPermission(Manifest.permission.CALL_PHONE, Constant.REQUEST_CALL)) {
                        mActivity.call(mActivity, getResources().getString(R.string.text_phone_help));
                    }
                } else {
                    mActivity.call(mActivity, getResources().getString(R.string.text_phone_help));
                }

                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(mActivity, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case Constant.REQUEST_CALL:
                    mActivity.call(mActivity, getResources().getString(R.string.text_phone_help));
                    break;
            }
        }
    }
}
