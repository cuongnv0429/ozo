package com.ozo.ozo.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.ozo.ozo.R;
import com.ozo.ozo.databinding.FragmentRechargeBinding;
import com.ozo.ozo.model.User;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 6/11/17.
 */

public class FragmentRecharge extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener{
    public static final String TAG = "FragmentRecharge";
    BaseActivity mActivity;
    FragmentRechargeBinding mFragmentRechargeBinding;
    BaseApplication mApplication;
    OzoServices mServices;
    Subscription mSubscription;
    ProgressDialog mProgressDialog;
    String telco = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mFragmentRechargeBinding == null) {
            mFragmentRechargeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_recharge, container, false);
            initView();
        }
        return mFragmentRechargeBinding.getRoot();
    }

    private void initView() {
        mFragmentRechargeBinding.toolbarRecharge.txtTitleToolbar.setText(getResources().getString(R.string.text_recharge));
        mFragmentRechargeBinding.toolbarRecharge.txtToolbarPost.setText(getResources().getString(R.string.text_done));

        mFragmentRechargeBinding.toolbarRecharge.txtToolbarCancel.setOnClickListener(this);
        mFragmentRechargeBinding.toolbarRecharge.txtToolbarPost.setOnClickListener(this);
        mFragmentRechargeBinding.spinnerNetworkProvider.setOnItemSelectedListener(this);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(mActivity, R.array.network_provider, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mFragmentRechargeBinding.spinnerNetworkProvider.setAdapter(adapter);

        mApplication = BaseApplication.getInstance(mActivity);
        mServices = mApplication.getApiServices();

    }

    private void recharge(int userId, String telco) {
        String pin = mFragmentRechargeBinding.edtPin.getText().toString().trim();
        String serial = mFragmentRechargeBinding.edtSerial.getText().toString().trim();
        mProgressDialog = mActivity.showLoading(mActivity);
        String sign = "userid=" + userId +  "&telco=" + telco.toLowerCase() + "&pin=" + pin + "&serial=" + serial;
        Log.e(TAG, sign);
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        Log.e(TAG, sign);
        mSubscription = mServices.recharge(userId, telco, pin, serial, sign, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        mActivity.hideLoading(mProgressDialog);
                        if (!jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                                if (mActivity.getIsLogin()) {
                                    mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            RealmResults<User> result = realm.where(User.class).findAll();
                                            result.clear();
                                            mActivity.saveIsLogin(false);
                                            mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                        }
                                    });
                                }
                            } else {
                                Toast.makeText(mActivity, jsonObject.get("message").getAsString().toString(), Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(mActivity, jsonObject.get("message").getAsString().toString(), Toast.LENGTH_SHORT).show();
                            mActivity.onBackPressed();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mActivity.hideLoading(mProgressDialog);
                    }
                });

    }

    private boolean validateNull() {
        if (mFragmentRechargeBinding.edtPin.getText().toString().equalsIgnoreCase("") ||
                mFragmentRechargeBinding.edtSerial.getText().toString().equalsIgnoreCase("")) {
            return true;
        }
        return false;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_toolbar_cancel:
                mActivity.onBackPressed();
                break;
            case R.id.txt_toolbar_post:
                mActivity.hideKeyboard();
                if (validateNull()) {
                    Toast.makeText(mActivity, getResources().getString(R.string.validate_empty_infor), Toast.LENGTH_SHORT).show();
                } else {
                    if (mActivity.getUserInfo() != null) {
                        recharge(mActivity.getUserInfo().getId(), telco);
                    }

                }
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                telco = "VTT";
                break;
            case 1:
                telco = "VNP";
                break;
            case 2:
                telco = "VMS";
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
