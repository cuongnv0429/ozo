package com.ozo.ozo.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ozo.ozo.R;
import com.ozo.ozo.activity.FilterActivity;
import com.ozo.ozo.activity.SettingActivity;
import com.ozo.ozo.adapter.CustomerManagerAdapter;
import com.ozo.ozo.customview.RecyclerTouchListener;
import com.ozo.ozo.databinding.FragmentCustomerManagerBinding;
import com.ozo.ozo.model.Account;
import com.ozo.ozo.model.BaseCustomer;
import com.ozo.ozo.model.Customer;
import com.ozo.ozo.model.Staff;
import com.ozo.ozo.model.User;
import com.ozo.ozo.model.callbacks.OnLoadMoreListener;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;
import com.ozo.ozo.utils.ShareHelper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 6/3/17.
 */

public class FragmentCustomerManager extends BaseFragment implements View.OnClickListener, TextWatcher{
    public  String TAG = "FragmentCustomerManager";
    BaseActivity mActivity;
    FragmentCustomerManagerBinding mFragmentCustomerManagerBinding;
    CustomerManagerAdapter mAdapter;
    List<Customer> mCustomers;
    BaseApplication mApplication;
    OzoServices mServices;
    Subscription mSubscription;
    String keySearch = "";
    private int pageIndex = 1;
    private int pageSize = 10;
    CountDownTimer mCountDownTimerSearchChange;
    private boolean isSearch = false;
    ProgressDialog progressDialog;
    private int managerId = 0;
    private int statusId = 0;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mFragmentCustomerManagerBinding == null) {
            mFragmentCustomerManagerBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_customer_manager, container, false);
            initView();
        }
        return mFragmentCustomerManagerBinding.getRoot();
    }

    private void initView() {
        mFragmentCustomerManagerBinding.toolbarCustomerManager.imgBack.setOnClickListener(this);
        mFragmentCustomerManagerBinding.toolbarCustomerManager.txtFilterCustomer.setOnClickListener(this);
        mFragmentCustomerManagerBinding.toolbarCustomerManager.toolbarSearchCustomer.imgExitSearch.setOnClickListener(this);
        mFragmentCustomerManagerBinding.toolbarCustomerManager.toolbarSearchCustomer.edtSearch.addTextChangedListener(this);

        mFragmentCustomerManagerBinding.txtEmptyInfor.setVisibility(View.GONE);

        mApplication = BaseApplication.getInstance(mActivity);
        mServices = mApplication.getApiServices();

        mCustomers = new ArrayList<>();
        mFragmentCustomerManagerBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mFragmentCustomerManagerBinding.recyclerView.setHasFixedSize(true);
        mFragmentCustomerManagerBinding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new CustomerManagerAdapter(mActivity, mCustomers, mFragmentCustomerManagerBinding.recyclerView);
        mFragmentCustomerManagerBinding.recyclerView.setAdapter(mAdapter);
        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                mCustomers.add(null);
                mAdapter.notifyItemInserted(mCustomers.size() - 1);
                pageIndex += 1;

                if (mActivity.getUserInfo() != null) {
                    getListCustomer(keySearch, managerId, statusId, pageIndex, pageSize, mActivity.getUserInfo().getId());
                }
            }
        });
        mFragmentCustomerManagerBinding.swipeRefreshCustomer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageIndex = 1;
                mAdapter.setLoaded();
                progressDialog = mActivity.showLoading(mActivity);
                if (mActivity.getUserInfo() != null) {
                    getListCustomer(keySearch, managerId, statusId, pageIndex, pageSize, mActivity.getUserInfo().getId());
                }
            }
        });

        mFragmentCustomerManagerBinding.recyclerView.addOnItemTouchListener(
                new RecyclerTouchListener(mActivity, mFragmentCustomerManagerBinding.recyclerView, new RecyclerTouchListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        ((SettingActivity) mActivity).pushFragment(FragmentCustomerDetail.newInstance(mCustomers.get(position).getId()));
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));
        pageIndex = 1;
        isSearch = false;
        Staff staff = new Gson().fromJson(ShareHelper.getByManager(mActivity, TAG), Staff.class);
        Account account = new Gson().fromJson(ShareHelper.getAccountType(mActivity, TAG), Account.class);
        try {
            managerId = Integer.parseInt(staff.getValue());
        } catch (Exception ex) {
            managerId = 0;
        }

        try {
            statusId = Integer.parseInt(account.getValue());
        } catch (Exception ex) {
            statusId = 0;
        }
        if (mActivity.getUserInfo() != null) {
            getListCustomer(keySearch, managerId, statusId, pageIndex, pageSize, mActivity.getUserInfo().getId());
        }


    }

    private void getListCustomer(String keySearch, int managerId, int statusId, final int pageIndex, int pageSize, int userId) {
        if (pageIndex == 1 && !mFragmentCustomerManagerBinding.swipeRefreshCustomer.isRefreshing()) {
            mFragmentCustomerManagerBinding.progressBar.setVisibility(View.VISIBLE);
        } else {
            mFragmentCustomerManagerBinding.progressBar.setVisibility(View.GONE);
        }
        String sign = "search=" + keySearch + "&managerid=" + managerId + "&statusid=" + statusId + "&pageindex=" + pageIndex + "&pagesize=" + pageSize;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.getListCustomer(keySearch, managerId, statusId, pageIndex, pageSize, sign, userId,
                mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        if (mFragmentCustomerManagerBinding.swipeRefreshCustomer.isRefreshing()) {
                            mActivity.hideLoading(progressDialog);
                            mFragmentCustomerManagerBinding.swipeRefreshCustomer.setRefreshing(false);
                            mCustomers.clear();
                            mAdapter.notifyDataSetChanged();
                        }
                        mFragmentCustomerManagerBinding.progressBar.setVisibility(View.GONE);
                        mFragmentCustomerManagerBinding.txtEmptyInfor.setVisibility(View.GONE);
                        if (mAdapter.isLoading()) {
                            mCustomers.remove(mCustomers.size() - 1);
                            mAdapter.notifyItemRemoved(mCustomers.size());
                        }
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            if (!jsonObject.getAsJsonObject("data").isJsonNull()) {
                                Type typeCollection = new TypeToken<BaseCustomer>(){}.getType();
                                BaseCustomer baseCustomer = new Gson().fromJson(jsonObject.getAsJsonObject("data"), typeCollection);
                                if (baseCustomer.getListCustomer().size() > 0 || isSearch) {
                                    if (isSearch) {
                                        isSearch = false;
                                        if (baseCustomer.getListCustomer().size() < 1) {
                                            if (pageIndex == 1) {
                                                mFragmentCustomerManagerBinding.txtEmptyInfor.setVisibility(View.VISIBLE);
                                            }

                                        }
                                    }
                                    mCustomers.addAll(mCustomers.size(), baseCustomer.getListCustomer());
                                    mAdapter.notifyDataSetChanged();
                                    mAdapter.setLoaded();
                                } else {
                                    if (mCustomers.size() < 1) {
                                        mFragmentCustomerManagerBinding.txtEmptyInfor.setVisibility(View.VISIBLE);

                                    }
                                }

                            }
                        } else if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                            if (mActivity.getIsLogin()) {
                                mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        RealmResults<User> result = realm.where(User.class).findAll();
                                        result.clear();
                                        mActivity.saveIsLogin(false);
                                        mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                    }
                                });
                            }
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mFragmentCustomerManagerBinding.progressBar.setVisibility(View.GONE);
                        if (mCustomers != null && mCustomers.size() < 1) {
                            mFragmentCustomerManagerBinding.txtEmptyInfor.setVisibility(View.VISIBLE);
                        }

                        if (mAdapter.isLoading()) {
                            mCustomers.remove(mCustomers.size() - 1);
                            mAdapter.notifyItemRemoved(mCustomers.size());
                        }

                        if (mFragmentCustomerManagerBinding.swipeRefreshCustomer.isRefreshing()) {
                            mActivity.hideLoading(progressDialog);
                            mFragmentCustomerManagerBinding.swipeRefreshCustomer.setRefreshing(false);
                        }
                    }
                });
    }

    private void search(final String keySearch) {
        if (mCountDownTimerSearchChange != null) {
            mCountDownTimerSearchChange.cancel();
        }
        mCountDownTimerSearchChange = new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                pageIndex = 1;
                mAdapter.setLoaded();
                isSearch = true;
                mCustomers.clear();
                mAdapter.notifyDataSetChanged();
                mFragmentCustomerManagerBinding.txtEmptyInfor.setVisibility(View.GONE);
                if (mActivity.getUserInfo() != null) {
                    getListCustomer(keySearch, managerId, statusId, pageIndex, pageSize, mActivity.getUserInfo().getId());
                }

            }
        };
        mCountDownTimerSearchChange.start();
    }

    private void initManagerList() {
        pageIndex = 1;
        mAdapter.setLoaded();
        isSearch = false;
        mCustomers.clear();
        mAdapter.notifyDataSetChanged();
        mFragmentCustomerManagerBinding.txtEmptyInfor.setVisibility(View.GONE);
        Staff staff = new Gson().fromJson(ShareHelper.getByManager(mActivity, TAG), Staff.class);
        Account account = new Gson().fromJson(ShareHelper.getAccountType(mActivity, TAG), Account.class);
        try {
            managerId = Integer.parseInt(staff.getValue());
        } catch (Exception ex) {
            managerId = 0;
        }

        try {
            statusId = Integer.parseInt(account.getValue());
        } catch (Exception ex) {
            statusId = 0;
        }
        if (mActivity.getUserInfo() != null) {
            getListCustomer(keySearch, managerId, statusId, pageIndex, pageSize, mActivity.getUserInfo().getId());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                mActivity.hideKeyboard();
                mActivity.onBackPressed();
                break;
            case R.id.txt_filter_customer:
//                ((SettingActivity) mActivity).pushFragment(new FragmentCustomerFilter());
                Intent intent = new Intent(mActivity, FilterActivity.class);
                intent.putExtra("tag", TAG);
                startActivityForResult(intent, Constant.REQUEST_FILTER_CUSTOMER_MANAGER);
                break;
            case R.id.img_exit_search:
                mFragmentCustomerManagerBinding.toolbarCustomerManager.toolbarSearchCustomer.edtSearch.setText("");
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (!s.toString().equalsIgnoreCase("") || (count == 0 && before != 0)) {
            if (count != 0) {
                mFragmentCustomerManagerBinding.toolbarCustomerManager.toolbarSearchCustomer.imgExitSearch.setVisibility(View.VISIBLE);
            } else {
                mFragmentCustomerManagerBinding.toolbarCustomerManager.toolbarSearchCustomer.imgExitSearch.setVisibility(View.GONE);
            }
            search(s.toString().trim());
        } else {
            mFragmentCustomerManagerBinding.toolbarCustomerManager.toolbarSearchCustomer.imgExitSearch.setVisibility(View.GONE);
        }

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.REQUEST_FILTER_CUSTOMER_MANAGER) {
            if (resultCode == Activity.RESULT_OK) {
                initManagerList();
            }
        }
    }
}
