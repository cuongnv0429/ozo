package com.ozo.ozo.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ozo.ozo.R;
import com.ozo.ozo.adapter.DistrictAdapter;
import com.ozo.ozo.databinding.FragmentDistrictBinding;
import com.ozo.ozo.model.District;
import com.ozo.ozo.model.Event;
import com.ozo.ozo.model.User;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;
import com.ozo.ozo.utils.ShareHelper;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 5/17/17.
 */

public class FragmentDistrict extends BaseFragment implements View.OnClickListener, DistrictAdapter.IOnDistrict{
    private static final String TAG = "FragmentDistrict";
    BaseActivity mActivity;
    FragmentDistrictBinding mFragmentDistrictBinding;
    DistrictAdapter mDistrictAdapter;
    OzoServices mServices;
    List<District> mDistrics;
    Subscription mSubscription;
    BaseApplication mApplication;
    DistrictAdapter.IOnDistrict mIOnDistrict = this;
    private String tag = "";

    public static FragmentDistrict newInstance(String tag) {
        Bundle args = new Bundle();
        args.putString("tag", tag);
        FragmentDistrict fragment = new FragmentDistrict();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tag = getArguments().getString("tag");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mFragmentDistrictBinding == null) {
            mFragmentDistrictBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_district, container, false);
            initView();
        }
        return mFragmentDistrictBinding.getRoot();
    }

    private void initView() {
        mFragmentDistrictBinding.toolbarFilterDetail.imgBackFilterDetailToolbar.setOnClickListener(this);
        mFragmentDistrictBinding.toolbarFilterDetail.txtFilterDetailToolbar.setText(getResources().getString(R.string.text_district));
        mApplication = BaseApplication.getInstance(mActivity);
        mServices = mApplication.getApiServices();
        mDistrics = new ArrayList<>();
        mDistrictAdapter = new DistrictAdapter(mActivity, mDistrics, mIOnDistrict, tag);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mFragmentDistrictBinding.recyclerDistrict.setLayoutManager(mLayoutManager);
        mFragmentDistrictBinding.recyclerDistrict.setItemAnimator(new DefaultItemAnimator());
        mFragmentDistrictBinding.recyclerDistrict.setAdapter(mDistrictAdapter);
        if (mActivity.getUserInfo() != null) {
            District oldProvince = new Gson().fromJson(ShareHelper.getProvince(mActivity, tag), District.class);
            getListDistrict(mActivity.getUserInfo().getId(),oldProvince.getId());
        }

    }

    private void getListDistrict(int userId, int provinceId) {
        mFragmentDistrictBinding.progressDistrict.setVisibility(View.VISIBLE);
        String sign = "userid=" + userId + "&proid=" + provinceId;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign);
        mSubscription = mServices.getListDistrict(sign, userId, provinceId, mActivity.getInfoLogin())
                .observeOn(mApplication.subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        Log.e(TAG, jsonObject.toString());
                        mSubscription.unsubscribe();
                        mFragmentDistrictBinding.progressDistrict.setVisibility(View.GONE);
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            if (!jsonObject.get("data").isJsonNull()) {
                                Type collectionType = new TypeToken<List<District>>(){}.getType();
                                mDistrics = (List<District>) new Gson().fromJson(jsonObject.getAsJsonArray("data"), collectionType);
                                mDistrictAdapter = new DistrictAdapter(mActivity, mDistrics, mIOnDistrict, tag);
                                mFragmentDistrictBinding.recyclerDistrict.setAdapter(mDistrictAdapter);
                            } else {
                                mFragmentDistrictBinding.progressDistrict.setVisibility(View.GONE);
                            }
                        } else if (mActivity.getIsLogin()) {
                            mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    RealmResults<User> result = realm.where(User.class).findAll();
                                    result.clear();
                                    mActivity.saveIsLogin(false);
                                    mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                }
                            });
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mFragmentDistrictBinding.progressDistrict.setVisibility(View.GONE);
                        throwable.printStackTrace();
                    }
                });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_filter_detail_toolbar:
                mActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onChooseDistrict(String name) {
        Event event = new Event(Constant.DISTRICT_TYPE, name);
        RxBus.getInstance().post(event);
        mActivity.onBackPressed();
    }
}
