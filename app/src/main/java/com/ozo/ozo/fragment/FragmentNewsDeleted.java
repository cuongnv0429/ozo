package com.ozo.ozo.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ozo.ozo.R;
import com.ozo.ozo.adapter.NewsDeletedAdapter;
import com.ozo.ozo.databinding.FragmentNewsDeletedBinding;
import com.ozo.ozo.model.News;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ITV01 on 4/13/17.
 */

public class FragmentNewsDeleted extends BaseFragment implements View.OnClickListener{
    BaseActivity mActivity;
    FragmentNewsDeletedBinding mNewsDeletedBinding;
    List<News> mNewsList;
    NewsDeletedAdapter mAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mNewsDeletedBinding == null) {
            mNewsDeletedBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_news_deleted, container, false);
            initView();
        }
        return mNewsDeletedBinding.getRoot();
    }

    private void initView() {
        mNewsDeletedBinding.toolbarNewsDeleted.imgBack.setOnClickListener(this);
        mNewsDeletedBinding.toolbarNewsDeleted.txtTitleToolbar.setText(getResources().getString(R.string.text_news_deleted));
        mNewsList = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            News mNews = new News();
            mNews.setmName("Nguyen Van Cuong");
            mNews.setmContent("Noi dung");
            mNews.setmDate("14/04/2017");
            mNewsList.add(mNews);
        }
        mAdapter = new NewsDeletedAdapter(mActivity, mNewsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mNewsDeletedBinding.recyclerNewsDeleted.setLayoutManager(mLayoutManager);
        mNewsDeletedBinding.recyclerNewsDeleted.setItemAnimator(new DefaultItemAnimator());
        mNewsDeletedBinding.recyclerNewsDeleted.setAdapter(mAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                mActivity.onBackPressed();
                break;
        }
    }
}
