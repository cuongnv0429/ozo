package com.ozo.ozo.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.google.gson.Gson;
import com.ozo.ozo.R;
import com.ozo.ozo.activity.FilterActivity;
import com.ozo.ozo.databinding.FragmetnNewsFilterBinding;
import com.ozo.ozo.model.ByDate;
import com.ozo.ozo.model.ByPrice;
import com.ozo.ozo.model.Category;
import com.ozo.ozo.model.District;
import com.ozo.ozo.model.Event;
import com.ozo.ozo.model.NewsSite;
import com.ozo.ozo.model.NewsStatus;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;
import com.ozo.ozo.utils.ShareHelper;

import java.util.Calendar;

import rx.functions.Action1;

/**
 * Created by ITV01 on 4/13/17.
 */

public class FragmentNewsFilter extends BaseFragment implements View.OnClickListener{
    public static final String TAG = "FragmentNewsFilter";
    BaseActivity mActivity;
    FragmetnNewsFilterBinding mNewsFilterBinding;
    DatePickerDialog mFromdatePickerDialog;
    DatePickerDialog mTodatePickerDialog;
    private int mYear, mMonth, mDay;
    private String tag;//dung bo loc nao

    public static FragmentNewsFilter newInstance(String tag) {
        Bundle args = new Bundle();
        args.putString("tag", tag);
        FragmentNewsFilter fragment = new FragmentNewsFilter();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tag = getArguments().getString("tag");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mNewsFilterBinding == null) {
            mNewsFilterBinding = DataBindingUtil.inflate(inflater, R.layout.fragmetn_news_filter, container, false);
            initView();
        }
        return mNewsFilterBinding.getRoot();
    }

    private void initView() {
        mNewsFilterBinding.toolbarNewsFilter.txtToolbarCancel.setOnClickListener(this);
        mNewsFilterBinding.toolbarNewsFilter.txtToolbarPost.setOnClickListener(this);
        mNewsFilterBinding.linearFilterCategory.setOnClickListener(this);
        mNewsFilterBinding.linearFilterProvince.setOnClickListener(this);
        mNewsFilterBinding.txtFilterDistrict.setOnClickListener(this);
        mNewsFilterBinding.txtFilterNewType.setOnClickListener(this);
        mNewsFilterBinding.lineaFilterNewSource.setOnClickListener(this);
        mNewsFilterBinding.linearFilterByDate.setOnClickListener(this);
        mNewsFilterBinding.linearFilterByPrice.setOnClickListener(this);
        mNewsFilterBinding.linearFilterFromDate.setOnClickListener(this);
        mNewsFilterBinding.linearFilterToDate.setOnClickListener(this);
        mNewsFilterBinding.fabReset.setOnClickListener(this);
        mNewsFilterBinding.toolbarNewsFilter.txtToolbarPost.setText(getResources().getString(R.string.text_done));
        mNewsFilterBinding.toolbarNewsFilter.txtTitleToolbar.setText(getResources().getString(R.string.text_news_filter));
        mNewsFilterBinding.fabReset.setBackgroundColor(getResources().getColor(R.color.colorMain));
        Calendar mCalendar = Calendar.getInstance();
        mYear = mCalendar.get(Calendar.YEAR);
        mMonth = mCalendar.get(Calendar.MONTH);
        mDay = mCalendar.get(Calendar.DAY_OF_MONTH);
        mFromdatePickerDialog = new DatePickerDialog(mActivity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                ShareHelper.saveFromDate(dayOfMonth + "-" + (month + 1) + "-" + year, mActivity, tag);
                mNewsFilterBinding.txtFilterFromDate.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
            }
        }, mYear, mMonth, mDay);

        mTodatePickerDialog = new DatePickerDialog(mActivity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                ShareHelper.saveToDate(dayOfMonth + "-" + (month + 1) + "-" + year, mActivity, tag);
                mNewsFilterBinding.txtFilterToDate.setText(dayOfMonth + "-" + (month + 1) + "-" + year);
            }
        }, mYear, mMonth, mDay);

        mNewsFilterBinding.txtFilterFromDate.setText(ShareHelper.getFromDate(mActivity, tag));
        mNewsFilterBinding.txtFilterToDate.setText(ShareHelper.getToDate(mActivity, tag));

        District oldDistrict = new Gson().fromJson(ShareHelper.getDistrict(mActivity, tag), District.class);
        mNewsFilterBinding.txtFilterDistrict.setText(oldDistrict.getName());

        District oldProvince = new Gson().fromJson(ShareHelper.getProvince(mActivity, tag), District.class);
        mNewsFilterBinding.txtFilterProvince.setText(oldProvince.getName());

        Category oldCategory = new Gson().fromJson(ShareHelper.getCategory(mActivity, tag), Category.class);
        mNewsFilterBinding.txtFilterCategory.setText(oldCategory.getName());

        NewsStatus oldNewsStatus = new Gson().fromJson(ShareHelper.getNewsStatus(mActivity, tag), NewsStatus.class);
        mNewsFilterBinding.txtFilterNewType.setText(oldNewsStatus.getName());

        NewsSite oldNewsSite = new Gson().fromJson(ShareHelper.getNewsSite(mActivity, tag), NewsSite.class);
        mNewsFilterBinding.txtFilterNewSource.setText(oldNewsSite.getName());

        ByDate oldByDate = new Gson().fromJson(ShareHelper.getByDate(mActivity, tag), ByDate.class);
        mNewsFilterBinding.txtFilterByDate.setText(oldByDate.getName());

        ByPrice oldByPrice = new Gson().fromJson(ShareHelper.getByPrice(mActivity, tag), ByPrice.class);
        mNewsFilterBinding.txtFilterByPrice.setText(oldByPrice.getName());

        RxBus.getInstance().getEvents().subscribe(new Action1<Object>() {
            @Override
            public void call(Object o) {
                if (o instanceof Event) {
                    switch (((Event) o).getType()) {
                        case Constant.CATEGORY_TYPE:
                            mNewsFilterBinding.txtFilterCategory.setText(((Event) o).getContent().toString());
                            break;
                        case Constant.PROVINCE_TYPE:
                            mNewsFilterBinding.txtFilterProvince.setText(((Event) o).getContent().toString());
                            District district = new District(0, "Tất cả");
                            ShareHelper.saveDistrict(new Gson().toJson(district), mActivity, tag);
                            mNewsFilterBinding.txtFilterDistrict.setText("Tất cả");
                            break;
                        case Constant.DISTRICT_TYPE:
                            mNewsFilterBinding.txtFilterDistrict.setText(((Event) o).getContent().toString());
                            break;
                        case Constant.NEWS_TYPE_TYPE:
                            mNewsFilterBinding.txtFilterNewType.setText(((Event) o).getContent().toString());
                            break;
                        case Constant.BY_DATE_TYPE:
                            mNewsFilterBinding.txtFilterByDate.setText(((Event) o).getContent().toString());
                            break;
                        case Constant.BY_PRICE_TYPE:
                            mNewsFilterBinding.txtFilterByPrice.setText(((Event) o).getContent().toString());
                            break;
                        case Constant.NEWS_SOURCE_TYPE:
                            mNewsFilterBinding.txtFilterNewSource.setText(((Event) o).getContent().toString());
                            break;
                    }
                }
            }
        });
    }

    private void sendResult() {
        Intent intent = mActivity.getIntent();
        mActivity.setResult(Activity.RESULT_OK, intent);
        mActivity.finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_toolbar_cancel:
                mActivity.onBackPressed();
                break;
            case R.id.txt_toolbar_post:
                sendResult();
                break;
            case R.id.linear_filter_category:
                ((FilterActivity) mActivity).pushFragment(FragmentCategory.newInstance(tag));
                break;
            case R.id.txt_filter_district:
                ((FilterActivity) mActivity).pushFragment(FragmentDistrict.newInstance(tag));
                break;
            case R.id.linear_filter_province:
                ((FilterActivity) mActivity).pushFragment(FragmentProvince.newInstance(tag));
                break;
            case R.id.txt_filter_new_type:
                ((FilterActivity) mActivity).pushFragment(FragmentNewsType.newInstance(tag));
                break;
            case R.id.linear_filter_by_date:
                ((FilterActivity) mActivity).pushFragment(FragmentByDate.newInstance(tag));
                break;
            case R.id.linear_filter_by_price:
                ((FilterActivity) mActivity).pushFragment(FragmentByPrice.newInstance(tag));
                break;
            case R.id.linea_filter_new_source:
                ((FilterActivity) mActivity).pushFragment(FragmentNewsSource.newInstance(tag));
                break;
            case R.id.linear_filter_from_date:
                if (!ShareHelper.getFromDate(mActivity, tag).equalsIgnoreCase("")) {
                    mDay = Integer.parseInt(ShareHelper.getFromDate(mActivity, tag).split("-")[0]);
                    mMonth = Integer.parseInt(ShareHelper.getFromDate(mActivity, tag).split("-")[1]);
                    mYear = Integer.parseInt(ShareHelper.getFromDate(mActivity, tag).split("-")[2]);
                }
                mFromdatePickerDialog.show();
                break;
            case R.id.linear_filter_to_date:
                if (!ShareHelper.getToDate(mActivity, tag).equalsIgnoreCase("")) {
                    mDay = Integer.parseInt(ShareHelper.getToDate(mActivity, tag).split("-")[0]);
                    mMonth = Integer.parseInt(ShareHelper.getToDate(mActivity, tag).split("-")[1]);
                    mYear = Integer.parseInt(ShareHelper.getToDate(mActivity, tag).split("-")[2]);
                }
                mTodatePickerDialog.show();
                break;
            case R.id.fab_reset:
                mActivity.resetFilter(mActivity, tag);
                initView();
                break;
        }
    }
}
