package com.ozo.ozo.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ozo.ozo.R;
import com.ozo.ozo.adapter.NotificationAdapter;
import com.ozo.ozo.databinding.FragmentNotificationBinding;
import com.ozo.ozo.model.Notification;
import com.ozo.ozo.model.User;
import com.ozo.ozo.model.callbacks.OnLoadMoreListener;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 4/10/17.
 */

public class FragmentNotification extends BaseFragment{
    private static final String TAG = "FragmentNotification";
    BaseActivity mActivity;
    FragmentNotificationBinding mNotificaitonBinding;
    private List<Notification> notificationList;
    private NotificationAdapter mAdapter;
    private OzoServices mServices;
    private Subscription mSubscription;
    private User mUser;
    private int pageIndex = 1;
    private ProgressDialog progressDialog;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mNotificaitonBinding == null) {
            mNotificaitonBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false);
            initView();
        }
        return mNotificaitonBinding.getRoot();
    }

    private void initView() {
        mNotificaitonBinding.settingToolbar.imgBack.setVisibility(View.GONE);
        mNotificaitonBinding.settingToolbar.txtTitleToolbar.setText(getResources().getString(R.string.text_title_notification));
        mServices = BaseApplication.getInstance(mActivity).getApiServices();
        mNotificaitonBinding.txtEmptyNotification.setVisibility(View.GONE);
        mUser = mActivity.getUserInfo();
        notificationList = new ArrayList<>();
        mNotificaitonBinding.recyclerNotifications.setLayoutManager(new LinearLayoutManager(mActivity));
        mNotificaitonBinding.recyclerNotifications.setHasFixedSize(true);
        mNotificaitonBinding.recyclerNotifications.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new NotificationAdapter(mActivity, notificationList,mNotificaitonBinding.recyclerNotifications);
        mNotificaitonBinding.recyclerNotifications.setAdapter(mAdapter);
        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                notificationList.add(null);
                mAdapter.notifyItemInserted(notificationList.size() - 1);
                pageIndex++;
                getListNotification(mUser.getId(), pageIndex, mUser.getIsUser());
            }
        });
        mNotificaitonBinding.swiperefreshNotification.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageIndex = 1;
                mAdapter.setLoaded();
                progressDialog = mActivity.showLoading(mActivity);
                getListNotification(mUser.getId(), pageIndex, mUser.getIsUser());
            }
        });
        if (mUser != null) {
            mNotificaitonBinding.progressBar.setVisibility(View.VISIBLE);
            pageIndex = 1;
            getListNotification(mUser.getId(), pageIndex, mUser.getIsUser());
        }
    }

    private void getListNotification(int userId, final int pageIndex, boolean isUser) {
        if (pageIndex == 1 && !mNotificaitonBinding.swiperefreshNotification.isRefreshing()) {
            mNotificaitonBinding.progressBar.setVisibility(View.VISIBLE);
        } else {
            mNotificaitonBinding.progressBar.setVisibility(View.GONE);
        }
        String sign = "userid=" + userId + "&page=" + pageIndex + "&isuser=" + isUser;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.getNoticeList(userId, pageIndex, isUser, sign, mActivity.getInfoLogin())
                .observeOn(BaseApplication.getInstance(mActivity).subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        if (mNotificaitonBinding.swiperefreshNotification.isRefreshing()) {
                            mActivity.hideLoading(progressDialog);
                            mNotificaitonBinding.swiperefreshNotification.setRefreshing(false);
                            notificationList.clear();
                            mAdapter.notifyDataSetChanged();
                        }
                        mNotificaitonBinding.progressBar.setVisibility(View.GONE);
                        mNotificaitonBinding.txtEmptyNotification.setVisibility(View.GONE);
                        if (mAdapter.isLoading()) {
                            notificationList.remove(notificationList.size() - 1);
                            mAdapter.notifyItemRemoved(notificationList.size() - 1);
                        }
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            if (!jsonObject.getAsJsonObject("data").isJsonNull()) {
                                Type typeCollection = new TypeToken<List<Notification>>(){}.getType();
                                List<Notification> data = new Gson().fromJson(jsonObject.getAsJsonObject("data").getAsJsonArray("NoticeList"), typeCollection);
                                if (data.size() > 0) {
                                    int position = notificationList.size();
                                    notificationList.addAll(notificationList.size(), data);
                                    mAdapter.notifyItemInserted(position);
                                    mAdapter.setLoaded();
                                } else {
                                    if (notificationList.size() < 1) {
                                        mNotificaitonBinding.txtEmptyNotification.setVisibility(View.VISIBLE);
                                    }
                                }

                            }
                        } else if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("1")) {
                            if (mActivity.getIsLogin()) {
                                mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        RealmResults<User> result = realm.where(User.class).findAll();
                                        result.clear();
                                        mActivity.saveIsLogin(false);
                                        mActivity.block(mActivity, jsonObject.get("message").getAsString());
                                    }
                                });
                            }
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mNotificaitonBinding.progressBar.setVisibility(View.GONE);
                        if (mNotificaitonBinding.swiperefreshNotification.isRefreshing()) {
                            mActivity.hideLoading(progressDialog);
                            mNotificaitonBinding.swiperefreshNotification.setRefreshing(false);
                        }
                        if (mAdapter.isLoading()) {
                            notificationList.remove(notificationList.size() - 1);
                            mAdapter.notifyItemRemoved(notificationList.size() - 1);
                        }
                        if (notificationList != null && notificationList.size() < 1) {
                            mNotificaitonBinding.txtEmptyNotification.setVisibility(View.VISIBLE);
                        }
                    }
                });
    }
}
