package com.ozo.ozo.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.ozo.ozo.R;
import com.ozo.ozo.activity.MainActivity;
import com.ozo.ozo.databinding.FragmentRegisterBinding;
import com.ozo.ozo.model.User;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseApplication;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

import io.realm.Realm;
import io.realm.RealmResults;
import rx.Subscription;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by ITV01 on 4/18/17.
 */

public class FragmentRegister extends BaseFragment implements View.OnClickListener{
    private static final String TAG = "FragmentRegister";
    BaseActivity mActivity;
    FragmentRegisterBinding mRegisterBinding;
    OzoServices mServices;
    Subscription mSubscription;
    ProgressDialog mProgressDialog;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRegisterBinding == null) {
            mRegisterBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);
//            mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
            initView();
        }
        return mRegisterBinding.getRoot();
    }

    private void initView() {
        mRegisterBinding.toolbarRegister.imgDelete.setOnClickListener(this);
        mRegisterBinding.btnAccountCreate.setOnClickListener(this);
        mRegisterBinding.txtHotlineRegister.setOnClickListener(this);

        mServices = BaseApplication.getInstance(mActivity).getApiServices();

    }

    private boolean validate(String phone, String password, String passwordConfirm, String fullname) {
        if (phone.equalsIgnoreCase("") || password.equalsIgnoreCase("") || passwordConfirm.equalsIgnoreCase("") ||
                fullname.equalsIgnoreCase("")) {
            Toast.makeText(mActivity, getResources().getString(R.string.validate_empty_infor), Toast.LENGTH_SHORT).show();
            return true;
        }
        if (mActivity.validatePhone(phone)) {
            Toast.makeText(mActivity, getResources().getString(R.string.validate_phone), Toast.LENGTH_SHORT).show();
            return true;
        }

        if (!password.equalsIgnoreCase(passwordConfirm)) {
            Toast.makeText(mActivity, getResources().getString(R.string.validate_password), Toast.LENGTH_SHORT).show();
            return true;
        }

        return false;
    }

    private void registerCustomer(String username, String password, String fullname) {
        String sign = "username=" + username + "&password=" + password + "&fullname=" + fullname;
        sign = mActivity.hmacSha(Constant.KEY_SIGN, sign.toLowerCase());
        mSubscription = mServices.registerCustomer(username, password, fullname, sign, mActivity.getInfoLogin())
                .observeOn(BaseApplication.getInstance(mActivity).subscribeScheduler())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<JsonObject>() {
                    @Override
                    public void call(final JsonObject jsonObject) {
                        mSubscription.unsubscribe();
                        Log.e(TAG, jsonObject.toString());
                        if (jsonObject.get("errorcode").getAsString().equalsIgnoreCase("0")) {
                            if (!jsonObject.get("data").isJsonNull()) {
                                mActivity.getmRealm().executeTransaction(new Realm.Transaction() {
                                    @Override
                                    public void execute(Realm realm) {
                                        RealmResults<User> result = realm.where(User.class).findAll();
                                        result.clear();
                                        realm.createObjectFromJson(User.class, jsonObject.getAsJsonObject("data").toString());
                                        if (mProgressDialog != null) {
                                            mActivity.hideLoading(mProgressDialog);
                                        }
                                        mActivity.saveIsLogin(true);
                                        Intent mIntent = new Intent(mActivity, MainActivity.class);
                                        startActivity(mIntent);
                                        mActivity.finish();
                                    }
                                });

                            } else {
                                mActivity.hideLoading(mProgressDialog);
                                mActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(mActivity, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        } else {
                            mActivity.hideLoading(mProgressDialog);
                            mActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(mActivity, jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        mActivity.hideLoading(mProgressDialog);
                        throwable.printStackTrace();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_delete:
                mActivity.onBackPressed();
                break;
            case R.id.btn_account_create:
                mActivity.hideKeyboard();
                if (!validate(mRegisterBinding.edtPhoneNumberRegister.getText().toString().trim(),
                        mRegisterBinding.edtPasswordRegister.getText().toString().trim(),
                        mRegisterBinding.edtPasswordConfirmRegister.getText().toString().trim(),
                        mRegisterBinding.edtFullnameRegister.getText().toString().trim())) {
                    mProgressDialog = mActivity.showLoading(mActivity);
                    registerCustomer(mRegisterBinding.edtPhoneNumberRegister.getText().toString().trim(),
                            mRegisterBinding.edtPasswordRegister.getText().toString().trim(),
                            mRegisterBinding.edtFullnameRegister.getText().toString().trim());
                }
                break;
            case R.id.txt_hotline_register:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (mActivity.askPermission(Manifest.permission.CALL_PHONE, Constant.REQUEST_CALL)) {
                        mActivity.call(mActivity, getResources().getString(R.string.text_phone_help));
                    }
                } else {
                    mActivity.call(mActivity, getResources().getString(R.string.text_phone_help));
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (ActivityCompat.checkSelfPermission(mActivity, permissions[0]) == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case Constant.REQUEST_CALL:
                    mActivity.call(mActivity, getResources().getString(R.string.text_phone_help));
                    break;
            }
        }
    }
}
