package com.ozo.ozo.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ozo.ozo.R;
import com.ozo.ozo.adapter.ByPriceAdapter;
import com.ozo.ozo.databinding.FragmentByPriceBinding;
import com.ozo.ozo.model.ByPrice;
import com.ozo.ozo.model.Event;
import com.ozo.ozo.network.RxBus;
import com.ozo.ozo.utils.BaseActivity;
import com.ozo.ozo.utils.BaseFragment;
import com.ozo.ozo.utils.Constant;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ITV01 on 5/18/17.
 */

public class FragmentByPrice extends BaseFragment implements View.OnClickListener, ByPriceAdapter.IOnByPrice{
    private static final String TAG = "FragmentByPrice";
    BaseActivity mActivity;
    FragmentByPriceBinding mFragmentByPriceBinding;
    ByPriceAdapter mByPriceAdapter;
    List<ByPrice> mByPrices;
    ByPriceAdapter.IOnByPrice mIOnByPrice = this;
    private String tag = "";

    public static FragmentByPrice newInstance(String tag) {

        Bundle args = new Bundle();
        args.putString("tag", tag);
        FragmentByPrice fragment = new FragmentByPrice();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (BaseActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tag = getArguments().getString("tag");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mFragmentByPriceBinding == null) {
            mFragmentByPriceBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_by_price, container, false);
            initView();
        }
        return mFragmentByPriceBinding.getRoot();
    }

    private void initView() {

        mFragmentByPriceBinding.toolbarFilterDetail.imgBackFilterDetailToolbar.setOnClickListener(this);
        mFragmentByPriceBinding.toolbarFilterDetail.txtFilterDetailToolbar.setText(getResources().getString(R.string.text_filter_by_price));
        mByPrices = new ArrayList<>();
        String[] by_price_array = getResources().getStringArray(R.array.by_price);
        for (int i = 0; i < by_price_array.length; i++ ) {
            mByPrices.add(new ByPrice(((i + 1) + ""), by_price_array[i]));
        }
        mByPriceAdapter = new ByPriceAdapter(mActivity, mByPrices, mIOnByPrice, tag);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mFragmentByPriceBinding.recyclerByPrice.setLayoutManager(mLayoutManager);
        mFragmentByPriceBinding.recyclerByPrice.setItemAnimator(new DefaultItemAnimator());
        mFragmentByPriceBinding.recyclerByPrice.setAdapter(mByPriceAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_filter_detail_toolbar:
                mActivity.onBackPressed();
                break;
        }
    }

    @Override
    public void onChoose(String name) {
        Event event = new Event(Constant.BY_PRICE_TYPE, name);
        RxBus.getInstance().post(event);
        mActivity.onBackPressed();
    }
}
