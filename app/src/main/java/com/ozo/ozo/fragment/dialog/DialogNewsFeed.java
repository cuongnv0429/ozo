package com.ozo.ozo.fragment.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.ozo.ozo.R;
import com.ozo.ozo.databinding.DialogNewsFeedBinding;
import com.ozo.ozo.utils.Constant;

/**
 * Created by ITV01 on 5/28/17.
 */

public class DialogNewsFeed extends android.support.v4.app.DialogFragment implements View.OnClickListener{
    DialogNewsFeedBinding mDialogNewsFeedBinding;
    public static final int REQUEST_CODE = 1001;
    public static final String KEY = "CHOOSE_NEWSPAPER";
    private boolean isStaff = false;
    private boolean isCC = false;
    private int position = -1;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (mDialogNewsFeedBinding == null) {
            mDialogNewsFeedBinding = DataBindingUtil.inflate(inflater, R.layout.dialog_news_feed, container, false);
            initView();
        }
        return mDialogNewsFeedBinding.getRoot();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private void initView() {
        mDialogNewsFeedBinding.txtAgencyNewspaper.setOnClickListener(this);
        mDialogNewsFeedBinding.txtCancle.setOnClickListener(this);
        mDialogNewsFeedBinding.txtOwnerNewspaper.setOnClickListener(this);
        mDialogNewsFeedBinding.txtDeleteStaff.setOnClickListener(this);
        if (getArguments() != null) {
            isStaff = getArguments().getBoolean("isStaff");
            isCC = getArguments().getBoolean("isCC");
            position = getArguments().getInt("position");
        }

        if (!isStaff) {
            mDialogNewsFeedBinding.linerStaff.setVisibility(View.GONE);
            mDialogNewsFeedBinding.linerCustomer.setVisibility(View.VISIBLE);
            if (isCC) {
                mDialogNewsFeedBinding.txtOwnerNewspaper.setText(getResources().getString(R.string.text_remove_for_use));
            } else {
                mDialogNewsFeedBinding.txtOwnerNewspaper.setText(getResources().getString(R.string.text_for_user));
            }
        } else {
            mDialogNewsFeedBinding.linerStaff.setVisibility(View.VISIBLE);
            mDialogNewsFeedBinding.linerCustomer.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_owner_newspaper:
                if (isCC) {
                    sendResult(Constant.FOR_USER_EDIT, position);
                    break;
                } else {
                    sendResult(Constant.NEWS_FOR_USER, position);
                    break;
                }

            case R.id.txt_agency_newspaper:
                sendResult(Constant.AGENCY_REPORT, position);
                break;
            case R.id.txt_delete_staff:
                sendResult(Constant.DELETE, position);
                break;
            case R.id.txt_cancle:
                sendResult(Constant.CANCEL, position);
                break;
        }
    }

    private void sendResult(int value, int position) {
        Intent intent = new Intent();
        intent.putExtra(KEY, value);
        intent.putExtra("position", position);
        getTargetFragment().onActivityResult(getTargetRequestCode(), REQUEST_CODE, intent);
        this.dismiss();
    }
}
