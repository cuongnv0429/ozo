package com.ozo.ozo.network;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

/**
 * Created by ITV01 on 5/18/17.
 */

public class RxBus {
    public static RxBus instance;
    private Subject<Object, Object> mBusSubject = new SerializedSubject<>(PublishSubject.create());
    private RxBus() {

    }
    public static RxBus getInstance() {
        if (instance == null) {
            instance = new RxBus();
        }
        return instance;
    }

    public void post(Object o) {
        if (mBusSubject == null) {
            mBusSubject = new SerializedSubject<>(PublishSubject.create());
        }
        mBusSubject.onNext(o);
    }

    public Observable<Object> getEvents() {
        if (mBusSubject == null) {
            mBusSubject = new SerializedSubject<>(PublishSubject.create());
        }
        return mBusSubject;
    }
}
