package com.ozo.ozo.network;

import com.google.gson.JsonObject;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by ITV01 on 5/9/17.
 */

public interface OzoServices {

    @FormUrlEncoded
    @POST("login")
    Observable<JsonObject> login(@Field("username") String username, @Field("password") String password,@Field("infologin") String infologin,
                                 @Field("sign") String sign);

    @FormUrlEncoded
    @POST("CreateCustomer")
    Observable<JsonObject> registerCustomer(@Field("username") String username, @Field("password") String password,@Field("fullname") String fullname,
                                            @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("getlistdistrictv2")
    Observable<JsonObject> getListDistrict(@Field("sign") String sign, @Field("userid") int userid, @Field("proId") int provinceId, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("GetlistStatus")
    Observable<JsonObject> getlistStatus(@Field("sign") String sign, @Field("userid") int userid, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("getlistsite")
    Observable<JsonObject> getlistSite(@Field("sign") String sign, @Field("userid") int userid, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("getlistcategory")
    Observable<JsonObject> getlistCategory(@Field("sign") String sign, @Field("userid") int userid, @Field("infologin") String inforLogin);


    @FormUrlEncoded
    @POST("GetListNewsInHome")
    Observable<JsonObject> getListNewsInHome(@Field("UserId") int userId, @Field("CateId") int cateId, @Field("provinceId") int provinceId, @Field("DistricId") int districtId,
    @Field("StatusId") int statusId, @Field("SiteId") int siteId, @Field("BackDate") int backDate, @Field("From") String from, @Field("To") String to,
    @Field("MinPrice") double minPrice, @Field("MaxPrice") double maxPrice, @Field("pageIndex") int pageIndex, @Field("pageSize") int pageSize,
     @Field("IsRepeat") boolean isRepeat, @Field("key") String key, @Field("NameOrder") String nameOrder, @Field("descending") boolean decending,
                                             @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("GetListCustomer")
    Observable<JsonObject> getListCustomer(@Field("search") String search, @Field("managerId") int managerId,
                                           @Field("statusId") int statusId, @Field("pageIndex") int pageIndex,
                                           @Field("pageSize") int pageSize, @Field("sign") String sign,
                                           @Field("UserId") int userId, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("GetNewsDetail")
    Observable<JsonObject> getNewsDetail(@Field("Id") int id, @Field("UserId") int userId, @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("CreateNews")
    Observable<JsonObject> createNews(@Field("Title") String title, @Field("CateId") int cateId, @Field("DistrictId") int districtId,
                                      @Field("Phone") String phone, @Field("Price") double price, @Field("Content") String content,
                                      @Field("UserId") int userId, @Field("IsUser") boolean isUser, @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("CustomerDetail")
    Observable<JsonObject> customerDetail(@Field("Id") int id, @Field("sign") String sign, @Field("UserId") int userId,
                                          @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("Recharge")
    Observable<JsonObject> recharge(@Field("userId") int userId, @Field("telco") String telco, @Field("pin") String pin,
                                    @Field("serial") String serial, @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("UserSaveNews")
    Observable<JsonObject> userSaveNews(@Field("listNewsId") int id [], @Field("userId") int userId,
                                        @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("UserHideNews")
    Observable<JsonObject> userHideNews(@Field("listNewsId") int id [], @Field("userId") int userId,
                                        @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("NewsForUser")
    Observable<JsonObject> newsForUser(@Field("listNewsId") int id [], @Field("userId") int userId,
                                       @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("DeleteNews")
    Observable<JsonObject> newsDelete(@Field("listNewsId") int id [], @Field("userId") int userId,
                                      @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("NewsSpam")
    Observable<JsonObject> newsSpam(@Field("listNewsId") int id [], @Field("userId") int userId,
                                    @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("ReportNews")
    Observable<JsonObject> agencyNews(@Field("listNewsId") int id [], @Field("userId") int userId,
                                      @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("RemoveNewsforUser")
    Observable<JsonObject> removeNewsForUser(@Field("listNewsId") int id [], @Field("userId") int userId,
                                             @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("UserRemoveNewsSave")
    Observable<JsonObject> removeNewsSave(@Field("listNewsId") int id [], @Field("userId") int userId,
                                          @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("UserRemoveNewsHide")
    Observable<JsonObject> removeNewsHide(@Field("listNewsId") int id [], @Field("userId") int userId,
                                          @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("Logout")
    Observable<JsonObject> logout(@Field("userId") int userId, @Field("sign") String sign);

    @FormUrlEncoded
    @POST("GetPaymentStatus")
    Observable<JsonObject> getPaymentStatus(@Field("userId") int userId, @Field("page") int page,
                                          @Field("isUser") boolean isUser, @Field("sign") String sign);

    @FormUrlEncoded
    @POST("GetManagerList")
    Observable<JsonObject> getManagerList(@Field("userId") int userId, @Field("page") int page,
                                          @Field("isUser") boolean isUser, @Field("sign") String sign);

    @FormUrlEncoded
    @POST("GetListNewStatus")
    Observable<JsonObject> getListNewStatus(@Field("UserId") int userId, @Field("CateId") int cateId, @Field("DistricId") int districtId,
                                             @Field("StatusId") int statusId, @Field("SiteId") int siteId, @Field("BackDate") int backDate, @Field("From") String from, @Field("To") String to,
                                             @Field("MinPrice") double minPrice, @Field("MaxPrice") double maxPrice, @Field("pageIndex") int pageIndex, @Field("pageSize") int pageSize,
                                             @Field("newsStatus") int newsStatus, @Field("IsRepeat") boolean isRepeat, @Field("key") String key, @Field("NameOrder") String nameOrder, @Field("descending") boolean decending,
                                             @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("NoticeList")
    Observable<JsonObject> getNoticeList(@Field("userId") int userId, @Field("page") int page,
                                          @Field("isUser") boolean isUser, @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("GetPayment")
    Observable<JsonObject> getPayment(@Field("sign") String sign, @Field("userId") int userId);

    @FormUrlEncoded
    @POST("GetPaymentMethod")
    Observable<JsonObject> getPaymentMethod(@Field("sign") String sign, @Field("userid") int userId,
                                            @Field("infologin") String infologin);

    @FormUrlEncoded
    @POST("InsertPayment")
    Observable<JsonObject> insertPayment(@Field("cusid") int customerId, @Field("paymentMethodId") int paymentMethodId,
                                         @Field("Note") String note, @Field("Amount") long amount,  @Field("sign") String sign,
                                         @Field("userid") int userId, @Field("infologin") String infologin);

    @FormUrlEncoded
    @POST("RegisterPackage")
    Observable<JsonObject> registerPackage(@Field("userId") int userId, @Field("paymentId") int paymentId, @Field("sign") String sign,
                                            @Field("infologin") String infologin);


    @FormUrlEncoded
    @POST("NewsWarning")
    Observable<JsonObject> newsWarning(@Field("UserId") int userId, @Field("NewsID") int newsId, @Field("reason") String reason,
                                            @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("CancelNewsWarning")
    Observable<JsonObject> cancleNewsWarning(@Field("UserId") int userId, @Field("NewsID") int newsId,
                                       @Field("sign") String sign, @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("GetListHistory")
    Observable<JsonObject> getListExchangeHistory(@Field("cusId") int cusId, @Field("Page") int page,
                                          @Field("sign") String sign, @Field("userid") int userId,
                                          @Field("infologin") String inforLogin);

    @FormUrlEncoded
    @POST("getlistprovince")
    Observable<JsonObject> getListProvince(@Field("sign") String sign, @Field("userid") int userid, @Field("infologin") String inforLogin);

}
