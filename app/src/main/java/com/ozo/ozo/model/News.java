package com.ozo.ozo.model;

/**
 * Created by ITV01 on 4/13/17.
 */

public class News {
    private String mAvarta;
    private String mName;
    private String mContent;
    private String mDate;


    public String getmAvarta() {
        return mAvarta;
    }

    public void setmAvarta(String mAvarta) {
        this.mAvarta = mAvarta;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmContent() {
        return mContent;
    }

    public void setmContent(String mContent) {
        this.mContent = mContent;
    }

    public String getmDate() {
        return mDate;
    }

    public void setmDate(String mDate) {
        this.mDate = mDate;
    }
}
