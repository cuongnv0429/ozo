package com.ozo.ozo.model;

import io.realm.RealmObject;

/**
 * Created by ITV01 on 5/9/17.
 */

public class User extends RealmObject{


    public User() {
        this.Id = -1;
        this.Username = "";
        this.Phone = "";
        this.FullName = "";
        this.IsPayment = false;
        this.IsUser = false;
        this.ManagerName = "";
        this.Amount = "";
        this.DateEnd = "";
    }
    private int Id;
    private String Username;
    private String Phone;
    private String FullName;
    private boolean IsPayment;
    private boolean IsUser;
    private String ManagerName;
    private String Amount;
    private String DateEnd;


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public boolean getIsPayment() {
        return IsPayment;
    }

    public void setIsPayment(boolean payment) {
        IsPayment = payment;
    }

    public boolean getIsUser() {
        return IsUser;
    }

    public void setIsUser(boolean user) {
        IsUser = user;
    }

    public String getManagerName() {
        return ManagerName;
    }

    public void setManagerName(String managerName) {
        ManagerName = managerName;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getDateEnd() {
        return DateEnd;
    }

    public void setDateEnd(String dateEnd) {
        DateEnd = dateEnd;
    }
}
