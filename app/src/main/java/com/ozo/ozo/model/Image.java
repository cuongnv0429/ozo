package com.ozo.ozo.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ITV01 on 6/17/17.
 */

public class Image {
    @SerializedName("Id")
    private int id;

    @SerializedName("NewsId")
    private int newsId;

    @SerializedName("ImageUrl")
    private String imageUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNewsId() {
        return newsId;
    }

    public void setNewsId(int newsId) {
        this.newsId = newsId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
