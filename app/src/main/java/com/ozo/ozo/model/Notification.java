package com.ozo.ozo.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tug-on(_SM_) on 7/5/2017.
 */

public class Notification {
    @SerializedName("UserId")
    private int userId;

    @SerializedName("Id")
    private int id;

    @SerializedName("DateSend")
    private String dateSend;

    @SerializedName("UserName")
    private String userName;

    @SerializedName("Title")
    private String title;

    @SerializedName("Type")
    private int type;

    @SerializedName("Description")
    private String description;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateSend() {
        return dateSend;
    }

    public void setDateSend(String dateSend) {
        this.dateSend = dateSend;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
