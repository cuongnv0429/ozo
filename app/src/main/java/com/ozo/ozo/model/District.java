package com.ozo.ozo.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ITV01 on 5/17/17.
 */

public class District {
    @SerializedName("Id")
    private int id;

    @SerializedName("Name")
    private String name;

    public District() {

    }

    public District(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
