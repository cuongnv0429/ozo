package com.ozo.ozo.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ITV01 on 5/18/17.
 */

public class ByPrice {
    @SerializedName("Id")
    private String id;

    @SerializedName("Name")
    private String name;

    public ByPrice() {

    }

    public ByPrice(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
