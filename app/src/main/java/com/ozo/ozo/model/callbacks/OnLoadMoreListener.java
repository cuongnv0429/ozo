package com.ozo.ozo.model.callbacks;

/**
 * Created by ITV01 on 6/3/17.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
