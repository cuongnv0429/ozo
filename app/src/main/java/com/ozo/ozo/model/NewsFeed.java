package com.ozo.ozo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ITV01 on 4/19/17.
 */

public class NewsFeed {
    public NewsFeed() {
    }

    @SerializedName("Id")
    private int id;

    @SerializedName("CategoryId")
    private int categoryId;

    @SerializedName("Title")
    private String title;

    @SerializedName("Contents")
    private String contents;

    @SerializedName("Summary")
    private String sumary;

    @SerializedName("Link")
    private String link;

    @SerializedName("SiteId")
    private int siteId;

    @SerializedName("DistrictId")
    private int districtdId;

    @SerializedName("ProvinceId")
    private String provinceId;

    @SerializedName("Phone")
    private String phone;

    @SerializedName("PriceText")
    private String priceText;

    @SerializedName("CreatedBy")
    private String createBy;

    @SerializedName("CreatedOn")
    private String createOn;

    @SerializedName("Price")
    private long price;

    @SerializedName("StatusId")
    private int statusId;

    @SerializedName("IsOwner")
    private boolean isOwner;

    @SerializedName("Iscc")
    private boolean isCC;

    @SerializedName("IsReason")
    private boolean isReason;

    @SerializedName("DistictName")
    private String districtName;

    @SerializedName("StatusName")
    private String statusName;

    @SerializedName("IsAdmin")
    private boolean isAdmin;

    @SerializedName("CateName")
    private String cateName;

    @SerializedName("SiteName")
    private String siteName;

    @SerializedName("PersonCheck")
    private String personCheck;

    @SerializedName("PersionalReport")
    private String persionalReport;

    @SerializedName("ListImage")
    private List<Image> listImage;

    @SerializedName("SameNews")
    private List<NewsFeed> sameNews;

    private boolean isShow = false;



    public boolean isShow() {
        return isShow;
    }

    public void setShow(boolean show) {
        isShow = show;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getSumary() {
        return sumary;
    }

    public void setSumary(String sumary) {
        this.sumary = sumary;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getSiteId() {
        return siteId;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    public int getDistrictdId() {
        return districtdId;
    }

    public void setDistrictdId(int districtdId) {
        this.districtdId = districtdId;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPriceText() {
        return priceText;
    }

    public void setPriceText(String priceText) {
        this.priceText = priceText;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateOn() {
        return createOn;
    }

    public void setCreateOn(String createOn) {
        this.createOn = createOn;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public boolean isOwner() {
        return isOwner;
    }

    public void setOwner(boolean owner) {
        isOwner = owner;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public String getCateName() {
        return cateName;
    }

    public void setCateName(String cateName) {
        this.cateName = cateName;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getPersonCheck() {
        return personCheck;
    }

    public void setPersonCheck(String personCheck) {
        this.personCheck = personCheck;
    }

    public String getPersionalReport() {
        return persionalReport;
    }

    public void setPersionalReport(String persionalReport) {
        this.persionalReport = persionalReport;
    }

    public List<Image> getListImage() {
        return listImage;
    }

    public void setListImage(List<Image> listImage) {
        this.listImage = listImage;
    }

    public List<NewsFeed> getSameNews() {
        return sameNews;
    }

    public void setSameNews(List<NewsFeed> sameNews) {
        this.sameNews = sameNews;
    }


    public boolean isCC() {
        return isCC;
    }

    public void setCC(boolean CC) {
        isCC = CC;
    }

    public boolean isReason() {
        return isReason;
    }

    public void setReason(boolean reason) {
        isReason = reason;
    }
}
