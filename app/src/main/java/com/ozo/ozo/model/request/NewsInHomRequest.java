package com.ozo.ozo.model.request;

/**
 * Created by ITV01 on 5/23/17.
 */

public class NewsInHomRequest {

    public NewsInHomRequest() {

    }

    public NewsInHomRequest(int userId, int cateId, int districtId, int statusId, int siteId,
                            int backDate, String from, String to, double minPrice, double maxPrice,
                            int pageIndex, int pageSize, boolean isRepeat, String key, String nameOrder,
                            boolean descending, String sign) {
        this.userId = userId;
        this.cateId = cateId;
        this.districtId = districtId;
        this.statusId = statusId;
        this.siteId = siteId;
        this.backDate = backDate;
        this.from = from;
        this.to = to;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.isRepeat = isRepeat;
        this.key = key;
        this.nameOrder = nameOrder;
        this.descending = descending;
        this.sign = sign;
    }

    private int userId;
    private int cateId;
    private int provinceId;
    private int districtId;
    private int statusId;
    private int siteId;
    private int backDate;
    private String from;
    private String to;
    private double minPrice;
    private double maxPrice;
    private int pageIndex;
    private int pageSize;
    private boolean isRepeat;
    private String key;
    private String nameOrder;
    private boolean descending;
    private String sign;

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCateId() {
        return cateId;
    }

    public void setCateId(int cateId) {
        this.cateId = cateId;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public int getSiteId() {
        return siteId;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    public int getBackDate() {
        return backDate;
    }

    public void setBackDate(int backDate) {
        this.backDate = backDate;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public boolean isRepeat() {
        return isRepeat;
    }

    public void setRepeat(boolean repeat) {
        isRepeat = repeat;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNameOrder() {
        return nameOrder;
    }

    public void setNameOrder(String nameOrder) {
        this.nameOrder = nameOrder;
    }

    public boolean isDescending() {
        return descending;
    }

    public void setDescending(boolean descending) {
        this.descending = descending;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
