package com.ozo.ozo.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tug-on(_SM_) on 7/6/2017.
 */

public class Staff {
    @SerializedName("Disabled")
    private boolean disabled;

    @SerializedName("Group")
    private String group;

    @SerializedName("Selected")
    private boolean selected;

    @SerializedName("Text")
    private String text;

    @SerializedName("Value")
    private String value;

    public Staff() {}

    public Staff(boolean disabled, String group, boolean selected, String text, String value) {
        this.disabled = disabled;
        this.group = group;
        this.selected = selected;
        this.text = text;
        this.value = value;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
