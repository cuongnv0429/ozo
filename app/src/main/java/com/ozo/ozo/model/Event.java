package com.ozo.ozo.model;

/**
 * Created by ITV01 on 5/18/17.
 */

public class Event {
    private String type;
    private Object content;
    public Event() {

    }

    public Event(String type, Object content) {
        this.type = type;
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }
}
