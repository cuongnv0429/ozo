package com.ozo.ozo.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ITV01 on 6/3/17.
 */

public class Customer implements Serializable{

    @SerializedName("Id")
    private int id;

    @SerializedName("FullName")
    private String fullName;

    @SerializedName("UserName")
    private String userName;

    @SerializedName("Phone")
    private String phone;

    @SerializedName("Email")
    private String email;

    @SerializedName("IsMember")
    private boolean isMember;

    @SerializedName("ManagerBy")
    private String manageBy;

    @SerializedName("RoleName")
    private String roleName;

    @SerializedName("IsDelete")
    private boolean isDelete;

    @SerializedName("IsOnline")
    private boolean isOnline;

    @SerializedName("EndTimePayment")
    private String endTimePayment;

    @SerializedName("ManagerId")
    private int managerId;

    @SerializedName("TimeEnd")
    private String timeEnd;

    @SerializedName("Amount")
    private String amount;

    @SerializedName("LastLogin")
    private String lastLogin;

    @SerializedName("CashPayment")
    private String cashPayment;

    @SerializedName("CardPayment")
    private String cardPayment;

    @SerializedName("Status")
    private String status;

    @SerializedName("Role")
    private String role;

    @SerializedName("Notes")
    private String notes;

    @SerializedName("CreateDate")
    private String createDate;

    @SerializedName("TimeEndStr")
    private String timeEndStr;

    public Customer() {
        this.timeEndStr = "Chưa đăng ký gói cước";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isMember() {
        return isMember;
    }

    public void setMember(boolean member) {
        isMember = member;
    }

    public String getManageBy() {
        return manageBy;
    }

    public void setManageBy(String manageBy) {
        this.manageBy = manageBy;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public boolean isDelete() {
        return isDelete;
    }

    public void setDelete(boolean delete) {
        isDelete = delete;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public String getEndTimePayment() {
        return endTimePayment;
    }

    public void setEndTimePayment(String endTimePayment) {
        this.endTimePayment = endTimePayment;
    }

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }


    public String getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(String timeEnd) {
        this.timeEnd = timeEnd;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getCashPayment() {
        return cashPayment;
    }

    public void setCashPayment(String cashPayment) {
        this.cashPayment = cashPayment;
    }

    public String getCardPayment() {
        return cardPayment;
    }

    public void setCardPayment(String cardPayment) {
        this.cardPayment = cardPayment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getTimeEndStr() {
        return timeEndStr;
    }

    public void setTimeEndStr(String timeEndStr) {
        this.timeEndStr = timeEndStr;
    }
}
