package com.ozo.ozo.model;

/**
 * Created by user on 8/4/17.
 */

public class DialogNewsReport {
    private int position;
    private Object content;

    public DialogNewsReport() {}

    public DialogNewsReport(int position, Object content) {
        this.position = position;
        this.content = content;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }
}
