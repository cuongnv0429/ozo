package com.ozo.ozo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ITV01 on 5/27/17.
 */

public class BaseNewsFeed {
    @SerializedName("pageIndex")
    private int pageIndex;

    @SerializedName("pageSize")
    private int pageSize;

    @SerializedName("Totalpage")
    private int totalPage;

    @SerializedName("Total")
    private int total;

    @SerializedName("ListNew")
    private List<NewsFeed> newsFeedList;

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<NewsFeed> getNewsFeedList() {
        return newsFeedList;
    }

    public void setNewsFeedList(List<NewsFeed> newsFeedList) {
        this.newsFeedList = newsFeedList;
    }
}
