package com.ozo.ozo.model;

/**
 * Created by ITV01 on 4/13/17.
 */

public class NewsFilter {
    private String title;
    private String value;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
