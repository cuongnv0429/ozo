package com.ozo.ozo.utils;

/**
 * Created by ITV01 on 4/13/17.
 */

public class Constant {
    public static final int POST_NEWS = 14;
    public static final int NEWS_DELETED = 15;
    public static final int NEWS_SAVED = 16;
    public static final int CUSTOMER_MANAGER = 17;
    public static final int RECHARGE = 18;
    public static final int CARD_CHOOSE = 24;
    public static final String KEY_SIGN = "api_ozo_2017_for_app";
    public static final String IS_LOGIN = "IS_LOGIN";
    public static final String DISTRICT_TYPE = "DISTRICT_TYPE";
    public static final String PROVINCE_TYPE = "PROVINCE_TYPE";
    public static final String NEWS_TYPE_TYPE = "NEWS_TYPE_TYPE";
    public static final String BY_DATE_TYPE = "BY_DATE_TYPE";
    public static final String BY_PRICE_TYPE = "BY_PRICE_TYPE";
    public static final String NEWS_SOURCE_TYPE = "NEWS_SOURCE_TYPE";
    public static final String CATEGORY_TYPE = "CATEGORY_TYPE";
    public static final String FROM_DATE = "FROM_DATE";
    public static final String TO_DATE = "TO_DATE";
    public static final String BY_MANAGER = "BY_MANAGER";
    public static final String ACCOUNT_TYPE = "ACCOUNT_TYPE";
    public static final String PAYMENT_METHOD = "PAYMENT_METHOD";
    public static final String ID_CATEGORY_TYPE = "ID_CATEGORY_TYPE";

    public static final int SEE_MORE = 250;
    public static final int READ_MORE = 450;

    public static final String REGEX_PHONE = "^\\+?\\d{1,3}?[- .]?\\(?(?:\\d{2,3})\\)?[- .]?\\d\\d\\d[- .]?\\d\\d\\d\\d$";

    public static final int NEWS_HIDE = 1;
    public static final int NEWS_SAVE = 2;
    public static final int NEWS_REJECT = 3;
    public static final int CONTACT = 4;
    public static final int NEWS_FOR_USER = 5;
    public static final int REMOVE_NEWS_HIDE = 21;
    public static final int REMOVE_NEWS_SAVE = 22;

//    public static final int FOR_USER_REPORT = 0;
    public static final int AGENCY_REPORT = 6;
    public static final int REPORT = 7;
    public static final int CANCEL_REPORT = 26;
    public static final int FOR_USER_EDIT = 8;
    public static final int DELETE = 9;
    public static final int CANCEL = -1;

    public static final int REQUEST_CALL = 1111;
    public static final int REQUEST_SMS = 1112;
    public static final int DETAIL_NEWSFEED = 11;
    public static final int RESULT_CODE_DETAIL_NEWSFEED = 12;

    public static final int REQUEST_FILTER_NEWS_FEED = 13;
    public static final int REQUEST_FILTER_NEWS_HIDED = 19;
    public static final int REQUEST_FILTER_NEWS_SAVED = 25;
    public static final int REQUEST_FILTER_CUSTOMER_MANAGER = 23;

    public static final int STATUS_SAVED = 1;
    public static final int STATUS_HIDED = 3;

    public static final int REQUEST_SAVED_TO_DETAIL = 101;
    public static final int REQUEST_HIDED_TO_DETAIL = 102;
}
