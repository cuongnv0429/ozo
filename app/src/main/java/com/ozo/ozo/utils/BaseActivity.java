package com.ozo.ozo.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ozo.ozo.R;
import com.ozo.ozo.activity.RegisterActivity;
import com.ozo.ozo.customview.ContactListener;
import com.ozo.ozo.customview.DialogConfirmListerner;
import com.ozo.ozo.customview.DialogConfirmNewsListerner;
import com.ozo.ozo.model.ByDate;
import com.ozo.ozo.model.User;

import java.util.Formatter;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ITV01 on 2/16/17.
 */

public class BaseActivity extends AppCompatActivity {

    private Realm mRealm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRealm = Realm.getDefaultInstance();
    }

    public Realm getmRealm() {
        if (mRealm == null) {
            mRealm = Realm.getDefaultInstance();
        }
        return mRealm;
    }

    public String convertLongTimeToString(int timeMs) {
        if (timeMs <= 0 || timeMs >= 24 * 60 * 60 * 1000) {
            return "00:00";
        }
        int totalSeconds = timeMs / 1000;
        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;
        StringBuilder mFormatBuilder = new StringBuilder();
        Formatter mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        try {
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ProgressDialog showLoading(Context context) {
        ProgressDialog progressDialog = ProgressDialog.show(context, null, null, true);
        progressDialog.setContentView(R.layout.progress_loading);
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }

        progressDialog.show();
        return progressDialog;
    }

    public void hideLoading(ProgressDialog progressDialog) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void block(Context mContex, String content) {
        Intent intent = new Intent(mContex, RegisterActivity.class);
        intent.putExtra("block", content);
        startActivity(intent);
        finish();
    }

    public void showDialog(final Context mContext, String content) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.dialog_block_app);
        Button dialogButton = (Button) dialog.findViewById(R.id.btn_ok_dialog);
        TextView txtContent = (TextView) dialog.findViewById(R.id.txt_content_dialog);
        txtContent.setText(content);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public String hmacSha(String key, String value) {
        try {
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(value.getBytes("UTF-8"));

            byte[] hexArray = {
                    (byte) '0', (byte) '1', (byte) '2', (byte) '3',
                    (byte) '4', (byte) '5', (byte) '6', (byte) '7',
                    (byte) '8', (byte) '9', (byte) 'a', (byte) 'b',
                    (byte) 'c', (byte) 'd', (byte) 'e', (byte) 'f'
            };
            byte[] hexChars = new byte[rawHmac.length * 2];
            for (int j = 0; j < rawHmac.length; j++) {
                int v = rawHmac[j] & 0xFF;
                hexChars[j * 2] = hexArray[v >>> 4];
                hexChars[j * 2 + 1] = hexArray[v & 0x0F];
            }
            return new String(hexChars);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public User getUserInfo() {
        User mUser;
        if (mRealm == null) {
            mRealm = Realm.getDefaultInstance();
        }
        mUser = mRealm.where(User.class).findFirst();
        if (mUser == null) {
            mUser = new User();
        }
        return mUser;
    }

    public void deleteUser() {
        if (mRealm == null) {
            mRealm = Realm.getDefaultInstance();
        }
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<User> result = realm.where(User.class).findAll();
                result.clear();
            }
        });
    }


    public void saveUserInfo(BaseActivity activity, final User mUser) {
        if (mRealm == null) {
            mRealm = Realm.getInstance(activity);
        }
        deleteUser();
        try {
            mRealm.beginTransaction();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    User mUserRealm = realm.createObject(User.class);
                    mUserRealm.setId(mUser.getId());
                    mUserRealm.setFullName(mUser.getFullName());
                    mUserRealm.setIsPayment(mUser.getIsPayment());
                    mUserRealm.setPhone(mUser.getPhone());
                    mUserRealm.setUsername(mUser.getUsername());
                    mUserRealm.setIsUser(mUser.getIsUser());
                }
            });
        } catch (Exception ex) {
            mRealm.cancelTransaction();
        }

    }

    public void saveIsLogin(Boolean isLogin) {
        SharedPreferences mySharedPreferences = getSharedPreferences("ozo", MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor = mySharedPreferences.edit();
        sharedpreferenceeditor.putBoolean(Constant.IS_LOGIN, isLogin);
        sharedpreferenceeditor.commit();
    }

    public Boolean getIsLogin() {
        SharedPreferences mySharedPreferences = getSharedPreferences("ozo", MODE_PRIVATE);
        return mySharedPreferences.getBoolean(Constant.IS_LOGIN, false);
    }

    public boolean validatePhone(String phone) {
        Pattern mPattern = Pattern.compile(Constant.REGEX_PHONE);
        Matcher mMatcher = mPattern.matcher(phone);
        return mMatcher.find() ? false : true;
    }

    public boolean askPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{permission}, requestCode);
            }
            return false;
        } else {
            return true;
        }
    }

    public void call(Context mContext, String phone) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + phone));
        mContext.startActivity(callIntent);
    }

    public void sendMessage(String phone) {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.setData(Uri.parse("sms:" + phone));
        sendIntent.putExtra("sms_body", "");
        startActivity(sendIntent);
    }

    public void contact(Context mContext, final ContactListener contactListener) {
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.dialog_contact);
        LinearLayout linearCall = (LinearLayout) dialog.findViewById(R.id.linear_call);
        LinearLayout linearMessage = (LinearLayout) dialog.findViewById(R.id.linear_message);
        linearCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                contactListener.contact("call");
            }
        });
        linearMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                contactListener.contact("message");
            }
        });
        dialog.show();
    }

    public String getInfoLogin() {
       return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public void dialogConfirm(Context mContext, String content, final DialogConfirmListerner confirmListerner) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirm);
        dialog.setCanceledOnTouchOutside(false);
        Button btnOk = (Button) dialog.findViewById(R.id.btn_ok_dialog_confirm);
        Button btnCancle = (Button) dialog.findViewById(R.id.btn_cancle_dialog_confirm);
        TextView txtContent = (TextView) dialog.findViewById(R.id.txt_content_dialog_confirm);
        TextView txtTitle = (TextView) dialog.findViewById(R.id.txt_title_dialog_confirm);
        txtTitle.setText("Ozo");
        txtContent.setText(content);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmListerner.dialogOk();
                dialog.dismiss();
            }
        });

        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmListerner.dialogCancle();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void dialogConfirmNews(Context mContext, String content,
                                  final DialogConfirmNewsListerner confirmListerner, final int type,
                                  final int position) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_confirm);
        dialog.setCanceledOnTouchOutside(false);
        Button btnOk = (Button) dialog.findViewById(R.id.btn_ok_dialog_confirm);
        Button btnCancle = (Button) dialog.findViewById(R.id.btn_cancle_dialog_confirm);
        TextView txtContent = (TextView) dialog.findViewById(R.id.txt_content_dialog_confirm);
        TextView txtTitle = (TextView) dialog.findViewById(R.id.txt_title_dialog_confirm);
        txtTitle.setText("Ozo");
        txtContent.setText(content);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmListerner.dialogOk(type, position);
                dialog.dismiss();
            }
        });

        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                confirmListerner.dialogCancle(type, position);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public int defineByDate(ByDate date) {
        int mByDate;
        switch (date.getId()) {
            case "1":
                mByDate = -1;
                break;
            case "2":
                mByDate = 1;
                break;
            case "3":
                mByDate = 0;
                break;
            case "4":
                mByDate = 2;
                break;
            case "5":
                mByDate = 3;
                break;
            case "6":
                mByDate = 5;
                break;
            case "7":
                mByDate = 7;
                break;
            case "8":
                mByDate = 15;
                break;
            case "9":
                mByDate = 30;
                break;
            default:
                mByDate = -1;
                break;
        }
        return mByDate;
    }

    public void resetFilter(Context mContext, String tag) {
        SharedPreferences mySharedPreferences = getSharedPreferences(tag, MODE_PRIVATE);
        SharedPreferences.Editor editor = mySharedPreferences.edit();
        editor.clear();
        editor.commit();
    }


}
