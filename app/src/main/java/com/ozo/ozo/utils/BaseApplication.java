package com.ozo.ozo.utils;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.ozo.ozo.network.OzoServices;
import com.ozo.ozo.network.ServiceFactory;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by ITV01 on 2/16/17.
 */

public class BaseApplication extends Application{
    private OzoServices ozoServices;
    private Scheduler scheduler;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(this)
                .name(Realm.DEFAULT_REALM_NAME)
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    public static BaseApplication getInstance(Context context) {
        return (BaseApplication) context.getApplicationContext();
    }


    public Scheduler subscribeScheduler() {
        if (scheduler == null) {
            scheduler = AndroidSchedulers.mainThread();
        }
        return scheduler;
    }

    public OzoServices getApiServices() {
        if (ozoServices == null) {
            ozoServices = ServiceFactory.createServices();
        }
        return ozoServices;
    }
}

