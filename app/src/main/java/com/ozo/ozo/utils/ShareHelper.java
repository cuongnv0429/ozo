package com.ozo.ozo.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.ozo.ozo.R;
import com.ozo.ozo.model.Account;
import com.ozo.ozo.model.ByDate;
import com.ozo.ozo.model.ByPrice;
import com.ozo.ozo.model.Category;
import com.ozo.ozo.model.District;
import com.ozo.ozo.model.NewsSite;
import com.ozo.ozo.model.NewsStatus;
import com.ozo.ozo.model.Payment;
import com.ozo.ozo.model.Staff;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by ITV01 on 5/18/17.
 */

public class ShareHelper {
    public static void saveDistrict(String content, Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor=mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.DISTRICT_TYPE, content);
        sharedpreferenceeditor.commit();
    }

    public static String getDistrict(Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        District district = new District(0, "Tất cả");
        return mySharedPreferences.getString(Constant.DISTRICT_TYPE, new Gson().toJson(district));
    }

    public static void saveProvince(String content, Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor=mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.PROVINCE_TYPE, content);
        sharedpreferenceeditor.commit();
    }

    public static String getProvince(Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        District district = new District(0, "Tất cả");
        return mySharedPreferences.getString(Constant.PROVINCE_TYPE, new Gson().toJson(district));
    }

    public static void saveNewsStatus(String content, Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor=mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.NEWS_TYPE_TYPE, content);
        sharedpreferenceeditor.commit();
    }

    public static String getNewsStatus(Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        NewsStatus newsStatus = new NewsStatus(0, "Tất cả");
        return mySharedPreferences.getString(Constant.NEWS_TYPE_TYPE, new Gson().toJson(newsStatus));
    }

    public static void saveNewsSite(String content, Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor=mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.NEWS_SOURCE_TYPE, content);
        sharedpreferenceeditor.commit();
    }

    public static String getNewsSite(Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        NewsSite newsSite = new NewsSite(0, "Tất cả");
        return mySharedPreferences.getString(Constant.NEWS_SOURCE_TYPE, new Gson().toJson(newsSite));
    }

    public static void saveByDate(String content, Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor = mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.BY_DATE_TYPE, content);
        sharedpreferenceeditor.commit();
    }

    public static String getByDate(Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        ByDate byDate = new ByDate("", "Tất cả");
        return mySharedPreferences.getString(Constant.BY_DATE_TYPE, new Gson().toJson(byDate));
    }

    public static void saveByPrice(String content, Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor = mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.BY_PRICE_TYPE, content);
        sharedpreferenceeditor.commit();
    }

    public static String getByPrice(Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        ByPrice byPrice = new ByPrice("", "Tất cả");
        return mySharedPreferences.getString(Constant.BY_PRICE_TYPE, new Gson().toJson(byPrice));
    }

    public static void saveCategory(String content, Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor = mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.CATEGORY_TYPE, content);
        sharedpreferenceeditor.commit();
    }

    public static String getCategory(Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        Category category = new Category(0, "Tất cả", -1);
        return mySharedPreferences.getString(Constant.CATEGORY_TYPE, new Gson().toJson(category));
    }

    public static void saveFromDate(String content, Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor = mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.FROM_DATE, content);
        sharedpreferenceeditor.commit();
    }

    public static String getFromDate(Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        return mySharedPreferences.getString(Constant.FROM_DATE, "");
    }

    public static void saveToDate(String content, Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor = mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.TO_DATE, content);
        sharedpreferenceeditor.commit();
    }

    public static String getToDate(Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        return mySharedPreferences.getString(Constant.TO_DATE, "");
    }

    public static void saveByManager(String content, Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor = mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.BY_MANAGER, content);
        sharedpreferenceeditor.commit();
    }

    public static String getByManager(Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        return mySharedPreferences.getString(Constant.BY_MANAGER, new Gson().toJson(new Staff(false, null, false, "Tất cả", "0")));
    }

    public static void saveAccountType(String content, Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor = mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.ACCOUNT_TYPE, content);
        sharedpreferenceeditor.commit();
    }

    public static String getAccountType(Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        return mySharedPreferences.getString(Constant.ACCOUNT_TYPE, new Gson().toJson(new Account(false, null, false, "Tất cả", "0")));
    }

    public static void savePaymentMethod(String content, Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        SharedPreferences.Editor sharedpreferenceeditor = mySharedPreferences.edit();
        sharedpreferenceeditor.putString(Constant.PAYMENT_METHOD, content);
        sharedpreferenceeditor.commit();
    }

    public static String getPaymentMethod(Context mContext, String tag) {
        SharedPreferences mySharedPreferences = mContext.getSharedPreferences(tag, MODE_PRIVATE);
        return mySharedPreferences.getString(Constant.PAYMENT_METHOD, new Gson().toJson(new Payment(false, null, false,
                mContext.getResources().getString(R.string.text_payment_method_choose), "0")));
    }

    public static String parseDateTime(String time) {
        String date = time.replace("/Date(", "");
        date = date.replace(")/", "");
        try {
            Timestamp timestamp = new Timestamp(Long.parseLong(date));
            Date mCreateDate = new Date(timestamp.getTime());
            DateFormat df = new SimpleDateFormat("dd/MM/yyy");
            date = df.format(mCreateDate);
            return date;
        } catch (NumberFormatException ex) {
            return time;
        }
    }
}
