package com.ozo.ozo.customview;

/**
 * Created by tug-on(_SM_) on 7/4/2017.
 */

public interface ContactListener {
    void contact(String action);
}
