package com.ozo.ozo.customview;

/**
 * Created by ITV01 on 6/27/17.
 */

public interface ClickItemNewsFeed {
    public void onClickItemNewsFeed(int item, int position);
}
