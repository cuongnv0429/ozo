package com.ozo.ozo.customview;

import android.view.View;

/**
 * Created by ITV01 on 6/17/17.
 */

public interface ClickListener {
    public void onClick(View view, int position);
    public void onLongClick(View view,int position);
}
