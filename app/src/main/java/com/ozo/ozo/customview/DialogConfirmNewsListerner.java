package com.ozo.ozo.customview;

/**
 * Created by user on 7/31/17.
 */

public interface DialogConfirmNewsListerner {
    void dialogOk(int type, int position);
    void dialogCancle(int type, int position);
}
